// https://wwww.bredofficial.ca/api/
// http://dev.bredofficial.ca/api/

export const environment = {
  production: true,
  API: {
    URL: 'https://www.bredofficial.ca/api/',
    KEY: ''
  },
  GA:'UA-134674041-2' // GOOGLE ANALYSIST GA CODE

};
