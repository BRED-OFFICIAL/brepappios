// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// http://dev.bredofficial.ca/api/
// http://localhost/bredofficial/api/

export const environment = {
  production: false,
  API: {
    URL: 'http://localhost/bredofficial/api/',
    KEY: 'jlsjdfljljl4234j24j2l4j2l4jljljlsdfjlsdfjlsf'
  },
  GA:'UA-134674041-1' // GOOGLE ANALYSIST GA CODE
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
