import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SignupService {
  url = environment.API.URL;
  apiKey = environment.API.KEY;

 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  signup(data) {
   
       
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let user = new HttpParams;
    user = user.set('email', data.email);
    user = user.set('password', data.password);
    user = user.set('username', data.username);
    user = user.set('firstname', data.firstname);
    user = user.set('lastname', data.lastname);
    user = user.set('phone', data.phone);
    user = user.set('shoesize', data.shoesize);
    user = user.set('newsletter', data.newsletter);
    user = user.set('socialactivation', data.socialactivation);
    user = user.set('id', data.userid);
    const userparam = user.toString();
    //console.log(userparam);
    // return this.http.post(`http://dev.bredofficial.ca/api/mainapis/user`, userparam, { headers: headerdata } )
    // .subscribe(res =>{
    //     console.log(res);
    // })

    return this.http.post(this.url+`mainapis/user_register`, userparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  resendmail(userid){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let user = new HttpParams;
    user = user.set('userid', userid);
    
    const userparam = user.toString();

    return this.http.post(this.url+`mainapis/resendConfirmationEmail`, userparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }
 
 
}