import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BrowseService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  getproducts(fromlimit,fetchproducts,gender,category,size,sortby,search) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let products = new HttpParams;
    products = products.set('fromlimit', fromlimit);
    products = products.set('fetchproducts', fetchproducts);
    products = products.set('gender', gender);
    products = products.set('category', category);
    products = products.set('size', size);
    products = products.set('sortby', sortby);
    products = products.set('search', search);

    const productsparam = products.toString();
 

    return this.http.post(this.url+`productlisting/productlist`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  getproductCount(gender,category,size,sortby) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let products = new HttpParams;
    products = products.set('gender', gender);
    products = products.set('category', category);
    products = products.set('size', size);
    products = products.set('sortby', sortby);

    const productsparam = products.toString();
 

    return this.http.post(this.url+`productlisting/productcount`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

 
 
 
}