import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; 

  constructor(private http: HttpClient) { }

  buynow(
    productId: any,
    buyerId: any,
    productSize: any,
    lowestask: any,
    shippingfee: any,
    processfee: any,
    grandprice: any,
    askid: any,
    discountid: any,
    discountcode: any,
    discountvalue: any,
    offerid = null) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('product_id', productId)
                      .set('buyer_id', buyerId)
                      .set('product_size', productSize)
                      .set('lowestask', lowestask)
                      .set('shippingfee', shippingfee)
                      .set('processfee', processfee)
                      .set('grandprice', grandprice)
                      .set('askid', askid)
                      .set('discountid', discountid)
                      .set('discountcode', discountcode)
                      .set('discountvalue', discountvalue)
                      .set('offerid', offerid);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/createOrder`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  sellnow(
    productId: any,
    sellerId: any,
    productSize: any,
    price: any,
    shippingfee: any,
    processfee: any,
    transactionfee: any,
    totalprice: any,
    bidid: any,
    discountid: any,
    discountcode: any,
    discountvalue: any,
    offerid = null
    ) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('product_id', productId)
                      .set('seller_id', sellerId)
                      .set('product_size', productSize)
                      .set('price', price)
                      .set('shippingfee', shippingfee)
                      .set('processfee', processfee)
                      .set('transactionfee', transactionfee)
                      .set('totalprice', totalprice)
                      .set('bidid', bidid)
                      .set('discountid', discountid)
                      .set('discountcode', discountcode)
                      .set('discountvalue', discountvalue)
                      .set('offerid', offerid);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/createOrderBySeller`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  listing(
    productId: any,
    sellerId: any,
    productSize: any,
    productsizevalue: any,
    price: any,
    shippingfee: any,
    processfee: any,
    transactionfee: any,
    totalprice: any,
    discountid: any,
    discountcode: any,
    discountvalue: any) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('product_id', productId)
                      .set('seller_id', sellerId)
                      .set('product_size', productSize)
                      .set('product_size_value', productsizevalue)
                      .set('price', price)
                      .set('shippingfee', shippingfee)
                      .set('processfee', processfee)
                      .set('transactionfee', transactionfee)
                      .set('totalprice', totalprice)
                      .set('discountid', discountid)
                      .set('discountcode', discountcode)
                      .set('discountvalue', discountvalue);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/createSellListing`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  confirm(
    productId: any,
    buyerId: any,
    productSize: any,
    price: any,
    shippingfee: any,
    processfee: any,
    total: any,
    discountid: any,
    discountcode: any,
    discountvalue: any) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('product_id', productId)
                      .set('buyer_id', buyerId)
                      .set('product_size', productSize)
                      .set('price', price)
                      .set('shippingfee', shippingfee)
                      .set('processfee', processfee)
                      .set('total', total)
                      .set('discountid', discountid)
                      .set('discountcode', discountcode)
                      .set('discountvalue', discountvalue);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/confirmBid`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  detail(
    orderId: any,
    buyerId: any,
    sellerId: any,
    ) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('order_id', orderId)
                      .set('buyer_id', (buyerId)?buyerId:'')
                      .set('seller_id', (sellerId)?sellerId:'');
    const productsparam = params.toString();
    return this.http.post(this.url + `order/orderDetail`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  orderShippingLabel(orderId: any, userId: any) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    params = params.set('order_id', orderId)
                      .set('user_id', userId);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/shippingLabel`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }
  updateAddress(userId: any, cardInformation: any, billingInformation: any) {

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;

    // params = params.set('order_id', orderId)
    //                   .set('user_id', userId);
    const productsparam = params.toString();
    return this.http.post(this.url + `order/shippingLabel`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }



}
