import { Injectable } from '@angular/core';
import {HttpClient,HttpParams,HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  url = environment.API.URL;
  apiKey = environment.API.KEY;

  constructor(private http: HttpClient) { }

  getToken(){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let params = new HttpParams;
    
    const productsparam = params.toString();
    return this.http.post(this.url + `payment/getToken`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }
}
