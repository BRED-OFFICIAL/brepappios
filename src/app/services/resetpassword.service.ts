import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class ResetpasswordService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  resetpassword( username: any ) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let user = new HttpParams;
    user = user.set('email', username);
    const userparam = user.toString();
    //console.log(userparam);
    // return this.http.post(`http://dev.bredofficial.ca/api/mainapis/user`, userparam, { headers: headerdata } )
    // .subscribe(res =>{
    //     console.log(res);
    // })

    return this.http.post(this.url+`mainapis/resetPassword`, userparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }
 
 
}