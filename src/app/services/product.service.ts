import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!
  productSource$ = new BehaviorSubject<any>({});
  product$ = this.productSource$.asObservable();

  productSizeSource$ = new BehaviorSubject<any>([]);
  productSize$ = this.productSizeSource$.asObservable();
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  getproductpagepagedata(id) {
    

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let product = new HttpParams;
    product = product.set('product_id', id);
    const productsparam = product.toString();
 

    return this.http.post(this.url+`product/getproduct`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  loadProduct(id) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    let product = new HttpParams;
    product = product.set('product_id', id);
    const productsparam = product.toString();
    this.http.post(this.url+`product/getproduct`, productsparam, { headers: headerdata } ).subscribe((res)=>{
      this.productSource$.next(res);
    });
  }

  fetch(){
    return this.product$;
  }
  update(data: any){
    this.productSource$.next(data);
  }

  loadProductSize(){
    this.product$.subscribe((res: any)=>{
      const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
      // tslint:disable-next-line: new-parens
      let product = new HttpParams;
      product = product.set('product_id', res.product.id);
      const productsparam = product.toString();


      this.http.post(this.url+`product/getproductsizes`, productsparam, { headers: headerdata } ).subscribe((res: any)=>{
          this.productSizeSource$.next(res);
      });


    });
  }

  fetchProductSize(){
    return this.productSize$;
  }

  getproductsize(id) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let product = new HttpParams;
    product = product.set('product_id', id);
    const productsparam = product.toString();
 

    return this.http.post(this.url+`product/getproductsizes`, productsparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }

  getbuyerallinformation(id) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url+`product/getbuyerallinformation`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  getsellerallinformation(id) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url+`product/getsellerallinformation`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  getuserallinformation(id) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url+`product/getUserAllInformation`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  getbuyerfees(id: any) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url+`product/getbuyerfees`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  getAllSizes() {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    // buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url+`product/getAllSizes`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  getAllCategories() {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let buyer = new HttpParams;
    // buyer = buyer.set('user_id', id);
    const buyerparam = buyer.toString();
 

    return this.http.post(this.url + `product/getAllCategories`, buyerparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));
  }

  // buynow(product_id,buyer_id,product_size,lowestask,shippingfee,processfee,grandprice,askid,discountid,discountcode,discountvalue) {
  //   // console.log('servics page');

  //   const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

  //   // tslint:disable-next-line: new-parens
  //   let params = new HttpParams;

  //     params = params.set('product_id', product_id)
  //             .set('buyer_id', buyer_id)
  //             .set('product_size', product_size)
  //             .set('lowestask', lowestask)
  //             .set('shippingfee', shippingfee)
  //             .set('processfee', processfee)
  //             .set('grandprice', grandprice)
  //             .set('askid', askid)
  //             .set('discountid', discountid)
  //             .set('discountcode', discountcode)
  //             .set('discountvalue', discountvalue);
  //   const productsparam = params.toString();
  //   return this.http.post(this.url+`order/createOrder`, productsparam, { headers: headerdata } )
  //   .pipe(map( resp => {
  //     return resp;
  //   }));

  // }

 
 
 
}