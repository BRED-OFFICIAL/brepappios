import { Injectable } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor(
    private ga: GoogleAnalytics
  ) { }

  setTrackScreen(screen = ''){
    this.ga.startTrackerWithId(environment.GA)
    .then(() => {
      console.log('Google analytics is ready now');
        this.ga.trackView(screen);
        console.log('Google analytics Tracking: ', screen);
      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
    })
    .catch(e => console.log('Error starting GoogleAnalytics', e));
  }
}
