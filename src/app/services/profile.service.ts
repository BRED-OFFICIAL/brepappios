import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!

  private sellingSubject =  new BehaviorSubject({});
  sellingdata = this.sellingSubject.asObservable();

  private buyingSubject =  new BehaviorSubject({});
  buyingdata = this.buyingSubject.asObservable();



  constructor(private http: HttpClient) {
   
   }

  buying( id: any, searchpurchase: any, searchoffer: any) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    usersearch = usersearch.set('searchpurchase', searchpurchase);
    usersearch = usersearch.set('searchoffer', searchoffer);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/buying', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  loadbuying( id: any, searchpurchase: any, searchoffer: any) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    usersearch = usersearch.set('searchpurchase', searchpurchase);
    usersearch = usersearch.set('searchoffer', searchoffer);
    const userparam = usersearch.toString();
    this.http.post(this.url + 'profile/buying', userparam, { headers: headerdata } ).subscribe((users)=>{
      this.buyingSubject.next(users);
    })
  }

  selling( id: any, searchsolditems: any, searchlistings: any) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    

    

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    usersearch = usersearch.set('searchsolditems', searchsolditems);
    usersearch = usersearch.set('searchlistings', searchlistings);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/selling', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  loadselling(id: any, searchsolditems: any, searchlistings: any){
    console.log('loadselling event');
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    usersearch = usersearch.set('searchsolditems', searchsolditems);
    usersearch = usersearch.set('searchlistings', searchlistings);
    const userparam = usersearch.toString();
    this.http.post(this.url + 'profile/selling', userparam, { headers: headerdata } ).subscribe((users)=>{
      this.sellingSubject.next(users)
    });
   

  }

  deleteOffer(id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/deleteOffers', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }

  deleteListing(id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/deleteListing', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }

  updateAddress(data: any){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('billingname', data.billingname);
    usersearch = usersearch.set('billingaddres1', data.billingaddres1);

    usersearch = usersearch.set('billingaddres2', data.billingaddres2);
    usersearch = usersearch.set('billingcountry', data.billingcountry);
    usersearch = usersearch.set('billingregion', data.billingregion);
    usersearch = usersearch.set('billingcity', data.billingcity);
    usersearch = usersearch.set('billingpostalcode', data.billingpostalcode);
    usersearch = usersearch.set('billingphone', data.billingphone);
    usersearch = usersearch.set('shiptobilling', data.shiptobilling);
    usersearch = usersearch.set('payment', JSON.stringify(data.payment));
    usersearch = usersearch.set('payment_identifer', data.payment_identifer);
    usersearch = usersearch.set('billing_id', data.billing_id);
    usersearch = usersearch.set('shipping_id', data.shipping_id);
    usersearch = usersearch.set('shipLabel', data.shipLabel);
    usersearch = usersearch.set('ship_name', data.ship_name);
    usersearch = usersearch.set('ship_address1', data.ship_address1);
    usersearch = usersearch.set('ship_address2', data.ship_address2);
    usersearch = usersearch.set('ship_country', data.ship_country);
    usersearch = usersearch.set('ship_region', data.ship_region);
    usersearch = usersearch.set('ship_city', data.ship_city);
    usersearch = usersearch.set('ship_postalcode', data.ship_postalcode);
    usersearch = usersearch.set('ship_phone', data.ship_phone);
    usersearch = usersearch.set('form_action', data.form_action);
    usersearch = usersearch.set('userid', data.userid);

    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/updateAddress', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));


  }

  updatePayout(paypalemail: string, id: string){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    
    let usersearch = new HttpParams;
    usersearch = usersearch.set('paypalemail', paypalemail);
    usersearch = usersearch.set('userid', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/updatePayout', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }


  changePassword(data: any, id: string, email: string){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    usersearch = usersearch.set('email', email);
    usersearch = usersearch.set('oldpassword', data.oldpassword);
    usersearch = usersearch.set('newpassword', data.newpassword);
    usersearch = usersearch.set('confirmpassword', data.confirmpassword);
    
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'profile/changePassword', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }


}
