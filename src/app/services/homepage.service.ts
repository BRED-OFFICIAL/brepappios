import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class HomepageService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  gethomepagedata() {
    
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
   

    // tslint:disable-next-line: new-parens
    let user = new HttpParams;
    const userparam = user.toString();
 

    return this.http.post(this.url+`home/homepage_content`, userparam, { headers: headerdata })
    .pipe(map( resp => {
      return resp;
    }));

  }

  getsearcheddata(key) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('data', key);
    const userparam = usersearch.toString();
 

    return this.http.post(this.url+`mainapis/getsearchdata`, userparam, { headers: headerdata } )
    .pipe(map( resp => {
      return resp;
    }));

  }
 
 
}