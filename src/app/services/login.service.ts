import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = environment.API.URL;
  apiKey = environment.API.KEY; // <-- Enter your own key here!
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }

  getLogin( username: any, password: any ) {
    // console.log('servics page');

    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');

    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('email', username);
    usersearch = usersearch.set('password', password);
    usersearch = usersearch.set('action', 'login');
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/user', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  isusernameunqiue(username: any, id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('username', username);
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/isusernameunqiue', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  isemailunqiue(email: any, id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('email', email);
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/isemailunqiue', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  updateprofile(data: any, id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('username', data.username);
    usersearch = usersearch.set('email', data.email);
    usersearch = usersearch.set('firstname', data.firstname);
    usersearch = usersearch.set('lastname', data.lastname);
    usersearch = usersearch.set('phone', data.phone);
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/updateprofile', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));
  }

  getprofile(id: any) {
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', id);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/getprofile', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }

  facebooklogin(data: any){
    const headerdata = new HttpHeaders().set('content-type', 'application/x-www-form-urlencoded');
    // tslint:disable-next-line: new-parens
    let usersearch = new HttpParams;
    usersearch = usersearch.set('id', data.id);
    usersearch = usersearch.set('name', data.name);
    usersearch = usersearch.set('email', data.email);
    usersearch = usersearch.set('picture', data.picture.data.url);
    const userparam = usersearch.toString();
    return this.http.post(this.url + 'mainapis/facebookLogin', userparam, { headers: headerdata } )
    .pipe(map( users => {
      return users;
    }));

  }


}