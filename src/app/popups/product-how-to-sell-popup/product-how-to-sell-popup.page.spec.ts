import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductHowToSellPopupPage } from './product-how-to-sell-popup.page';

describe('ProductHowToSellPopupPage', () => {
  let component: ProductHowToSellPopupPage;
  let fixture: ComponentFixture<ProductHowToSellPopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductHowToSellPopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductHowToSellPopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
