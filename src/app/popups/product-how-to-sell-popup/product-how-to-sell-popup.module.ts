import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductHowToSellPopupPage } from './product-how-to-sell-popup.page';

const routes: Routes = [
  {
    path: '',
    component: ProductHowToSellPopupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductHowToSellPopupPage]
})
export class ProductHowToSellPopupPageModule {}
