import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams,LoadingController} from '@ionic/angular';
import {BuyoffersTabPage} from '../../pages/buyoffers-tab/buyoffers-tab.page';
import {ProductService} from '../../services/product.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-buying-offer-detail-modal',
  templateUrl: './buying-offer-detail-modal.page.html',
  styleUrls: ['./buying-offer-detail-modal.page.scss'],
})
export class BuyingOfferDetailModalPage implements OnInit {
  offer: any;
  parentscope: any;
  product: any;
  productsizes: any;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.offer = this.navParams.data.d;
    this.parentscope = this.navParams.data.obj;
    console.log(this.offer);
  }
  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }
  close() {
    this.closeModal();
  }

  parentdelete(item: any){
    this.closeModal();
    this.parentscope.delete(item);
  }

  parentloaddata(){
    this.parentscope.loaddata();
  }

  editOffer(item: any){


    this.closeModal();
    this.presentLoader()
    this.productService.getproductpagepagedata(item.product_id)
      .subscribe( (productpagedata: any) => {
        this.product = productpagedata['product'];
        this.productsizes = productpagedata['size'];
        this.opnebuyOfferModal(item);
        this.loadingController.dismiss();
      });

  }

  async opnebuyOfferModal(offer: any){
    const modal = await this.modalController.create({
      component: BuyoffersTabPage,
       cssClass: 'buyoffers-tab',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.product.id,
        product: this.product,
        productSize: offer.attribute_value,
        productsizes: this.productsizes,
        id: offer.id,
        amount: offer.highest_bid_price,
        highestBid: offer.highest_offer,
        lowestAsk: offer.lowest_listing
      }
    });
    
    modal.onDidDismiss().then ((dataReturned) => {
      this.parentloaddata();
    });
    return await modal.present();
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loadertoshow = await this.loadingController.create({
      message: ''
    });
    await loadertoshow.present();

    const { role, data } = await loadertoshow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  redirecttoproductdetial(){
    this.closeModal();
    this.router.navigate(['/productdetail', this.offer.product_id]);

  }

}
