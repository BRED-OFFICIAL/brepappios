import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuyingOfferDetailModalPage } from './buying-offer-detail-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BuyingOfferDetailModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuyingOfferDetailModalPage]
})
export class BuyingOfferDetailModalPageModule {}
