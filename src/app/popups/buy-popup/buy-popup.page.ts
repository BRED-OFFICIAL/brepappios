import {Component,OnInit} from '@angular/core';
import {ModalController,NavController,NavParams} from '@ionic/angular';
import { BuypopupTabsPage } from '../../pages/buypopup-tabs/buypopup-tabs.page'
// import { halfSlideUpAnimation } from '../../animations/halfSlideUpAnimation';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
    selector: 'buy-popup',
    templateUrl: './buy-popup.page.html',
    styleUrls: ['./buy-popup.css'],
  })

export class BuyPopupPage implements OnInit{
    
    productid: number;
    product: any;

    constructor(
        private modalController: ModalController,
        private navParams: NavParams,
        private navCtrl: NavController,
        private gaAnalyticsService: GoogleAnalyticsService

    ){}

    goback(){
      this.navCtrl.pop();
    }

    ngOnInit(){
       // console.table(this.navParams);
        this.productid = this.navParams.data.productid;
        this.product = this.navParams.data.product;
        console.log(this.product);
        
    }

    async openbuynowpopup(){

      this.closeModal();
      const modal = await this.modalController.create({
        component: BuypopupTabsPage,
        cssClass: 'buypopup-tabs',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.productid,
          product: this.product
        }
      });

      return await modal.present();

    }

    checkedmessagepopupagain(e:any){
      //console.log('calling checkedmessage' + e.detail.checked);
      localStorage.setItem('buyinstructionmessagepopup', e.detail.checked);
    }

    async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Buy OnBoarding Screen');
  }
}