import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuyPopupPage } from './buy-popup.page';

const routes: Routes = [
  {
    path: '',
    component: BuyPopupPage
  }
  
];

@NgModule({
  imports: [  
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuyPopupPage]
})
export class BuyPopupPageModule {}
