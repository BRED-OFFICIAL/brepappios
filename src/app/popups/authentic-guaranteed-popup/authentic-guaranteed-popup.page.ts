import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ThemeableBrowser, ThemeableBrowserObject, ThemeableBrowserOptions } from '@ionic-native/themeable-browser/ngx';
import {ThemeableBrowserOptionConfig} from '../../../config/ThemeableBrowserOptions';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'app-authentic-guaranteed-popup',
  templateUrl: './authentic-guaranteed-popup.page.html',
  styleUrls: ['./authentic-guaranteed-popup.css'],
})
export class AuthenticGuaranteedPopupPage implements OnInit {

  modalTitle:string;
  modelId:number;
 
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private themeableBrowser: ThemeableBrowser,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }
 
  ngOnInit() {
    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
  }
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  openbrowser(url:string) {
    const options: ThemeableBrowserOptions = ThemeableBrowserOptionConfig;
    const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
    
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Authenticity Guaranteed Screen');
  }

}
