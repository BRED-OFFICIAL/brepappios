import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthenticGuaranteedPopupPage } from './authentic-guaranteed-popup.page';

const routes: Routes = [
  {
    path: '',
    component: AuthenticGuaranteedPopupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AuthenticGuaranteedPopupPage]
})
export class AuthenticGuaranteedPopupPageModule {}
