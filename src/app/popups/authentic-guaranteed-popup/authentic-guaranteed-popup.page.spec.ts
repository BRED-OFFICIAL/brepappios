import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticGuaranteedPopupPage } from './authentic-guaranteed-popup.page';

describe('AuthenticGuaranteedPopupPage', () => {
  let component: AuthenticGuaranteedPopupPage;
  let fixture: ComponentFixture<AuthenticGuaranteedPopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticGuaranteedPopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticGuaranteedPopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
