import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchfilterBrandPage } from './searchfilter-brand.page';
import { SlickCarouselModule } from 'ngx-slick-carousel';

const routes: Routes = [
  {
    path: '',
    component: SearchfilterBrandPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SlickCarouselModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchfilterBrandPage]
})
export class SearchfilterBrandPageModule {}
