import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchfilterBrandPage } from './searchfilter-brand.page';

describe('SearchfilterBrandPage', () => {
  let component: SearchfilterBrandPage;
  let fixture: ComponentFixture<SearchfilterBrandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchfilterBrandPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchfilterBrandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
