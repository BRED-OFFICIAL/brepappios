import { Component, OnInit, ViewChild } from '@angular/core';
import {ProductService} from '../../services/product.service';
import {ModalController, LoadingController, IonSlides, NavParams} from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
@Component({
  selector: 'app-searchfilter-brand',
  templateUrl: './searchfilter-brand.page.html',
  styleUrls: ['./searchfilter-brand.page.scss'],
})
export class SearchfilterBrandPage implements OnInit {
  @ViewChild('mySlides',{read: IonSlides}) slides: IonSlides;

  slideConfigBrands = {
    'slidesToShow': 2.4,
    'slidesToScroll': 1,
    'nextArrow':'<div class=\'nav-btn next-slide\'></div>',
    'prevArrow':'<div class=\'nav-btn prev-slide\'></div>',
    'dots':false,
    'infinite': false
  };

  categories: any;
  selectmaincategory: any;
  selectmaincategoryobj: any;
  selectcategory: any;
  selectcategoryobj: any;
  selectchildcategory: any;
  adidasobj: any;
  nikeobj: any;
  airjordenobj: any;

  categoriesSubject: any;

  selectedcategory: any;

  slideOpts = {
    slidesPerView: 2.4,
    speed: 800,
    slidesPerColumn: 1,
    slidesPerGroup:2,
    freeMode: true,
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true,
  }
  };


  public readonly serviceTypes = [
    'Swedish',
    'Deep Tissue',
    'Prenatal',
    'Sports',
  ];

  public readonly durations = [60, 90, 120];

  public serviceTypeIsOpen: { [key: string]: boolean } = {
    Swedish: false,
    'Deep Tissue': false,
    Prenatal: false,
    Sports: false,
  };

  constructor(
    private productService: ProductService,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.presentLoader();

    this.categoriesSubject = new BehaviorSubject([]);


    this.productService.getAllCategories().subscribe((response: any) => {
      if (response.status) {
        this.categories = response.data;
        this.categoriesSubject.next(this.categories);
        for(const category of this.categories ) {
          if(category.slug == 'adidas'){
            this.adidasobj = category;
          }

          if(category.slug == 'airjordan'){
            this.airjordenobj = category;
          }
          
          if(category.slug == 'nike'){
            this.nikeobj = category;
          }

        }
        this.loadingController.dismiss();
      }
    });
  }

  closepopup(){
    this.modalController.dismiss();

  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    let loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  setparentCategory(category: any){
    console.log('setparentCategory');
    if(this.selectmaincategory == category.slug){
      this.selectmaincategory = '';
      this.selectcategory = '';
      this.selectchildcategory = '';
      this.selectcategoryobj = {};
    } else{
      this.selectmaincategory = category.slug;
      this.selectcategoryobj = category;
    }
  }

  setCategory(category: any, level = 0){
    console.log('setCategory');
    if( category.subcategories !== undefined && category.subcategories.length > 0){
      if(this.selectcategory == category.slug){
        this.selectcategory = '';
        this.selectcategoryobj = {};
      } else{
        this.selectcategory = category.slug;
        this.selectcategoryobj = category;
      }
    }else{
      if(level==1){
        this.selectcategory = category.slug;
        this.selectchildcategory = '';
      }else{
        this.selectchildcategory = category.slug;
      }
      this.selectcategoryobj = category;
      console.log('Selected slug',this.selectchildcategory);
    }
    
    console.log(category);
  }

  reset(){
    this.selectcategory = '';
    this.selectchildcategory = '';
    this.selectcategoryobj = {};

    this.selectmaincategory = '';

  }

  setadidasCategory(){
    this.selectmaincategory = this.adidasobj.slug;
    this.selectcategoryobj = this.adidasobj;
  }

  setairjordenCategory(){
    this.selectmaincategory = this.airjordenobj.slug;
    this.selectcategoryobj = this.airjordenobj;
  }

  setnikeCategory(){
    this.selectmaincategory = this.nikeobj.slug;
    this.selectcategoryobj = this.nikeobj;
  }

  // setSubCategory(category: any){
  //   console.log('setSubCategory');
  //   this.selectcategoryobj = category;
  // }

  viewResult(){
    console.log('view result',this.selectcategoryobj);
    this.closeModal({
      Category: this.selectcategoryobj
    });

  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  ionViewWillEnter(){
    this.selectcategoryobj = this.navParams.data.selectcategory;
    if(this.selectcategoryobj != undefined && this.selectcategoryobj.slug != undefined ){
     
      if(this.selectcategoryobj.slug == 'adidas' || this.selectcategoryobj.slug == 'airjordan' || this.selectcategoryobj.slug == 'nike'){
        this.selectmaincategory = this.selectcategoryobj.slug;
        this.selectcategoryobj = this.selectcategory;
      }else{
        

        this.categoriesSubject.subscribe((data)=>{
          let isfound = false;
          for(const category of data ) {
            this.selectmaincategory = category.slug;
            if(category.slug == this.selectcategoryobj.slug){
              this.selectcategoryobj = category;
              this.selectcategory = category.slug;
              isfound = true;
              break;
            }


            if(category.categories.length > 0){
              for(const subcategory of category.categories ){
                this.selectcategory = subcategory.slug;
                if(subcategory.slug == this.selectcategoryobj.slug){
                  this.selectcategoryobj = subcategory;
                  isfound = true;
                  break;
                }

                if(subcategory.subcategories.length > 0){
                  for(const subsubcategory of subcategory.subcategories ){
                    if(subsubcategory.slug == this.selectcategoryobj.slug){
                      this.selectchildcategory = subsubcategory.slug;
                      this.selectcategoryobj = subsubcategory;
                      isfound = true;
                      break;
                    }
                  }
                }
                if(isfound){
                  break;
                }
              }
              if(isfound){
                break;
              }
            }

            if(isfound){
              break;
            }


          }



        });

        
        



      }
    }
    console.log(this.selectcategory);




    this.slides.update();
  }
  
}
