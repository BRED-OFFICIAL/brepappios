import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../services/product.service';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-searchfilter-size',
  templateUrl: './searchfilter-size.page.html',
  styleUrls: ['./searchfilter-size.page.scss'],
})
export class SearchfilterSizePage implements OnInit {

  sizes: any;
  selectedsize: any;
  gender: any;
  gendertext: any;

  constructor(
    private productService: ProductService,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.presentLoader();
   
    this.productService.getAllSizes().subscribe((response: any) => {
      if (response.status) {
        this.sizes = response.data;
        this.loadingController.dismiss();
      }
    });
  }

  closepopup(){
    this.modalController.dismiss();

  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    let loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  setSize(productSize: any) {
    this.selectedsize = productSize;
    this.updateSize();
  }
  updateSize() {
    console.log('After update',this.selectedsize);
    this.closeModal({
      Size: this.selectedsize
    });

  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }
  
  back(){
    this.closeModal();
  }
  reset(){
    this.selectedsize = {};
    this.updateSize();
  }
  ionViewWillEnter (){
    this.selectedsize = this.navParams.data.selectedsize;
    this.gender = this.navParams.data.gender;
  }

}
