import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchfilterSizePage } from './searchfilter-size.page';

const routes: Routes = [
  {
    path: '',
    component: SearchfilterSizePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchfilterSizePage]
})
export class SearchfilterSizePageModule {}
