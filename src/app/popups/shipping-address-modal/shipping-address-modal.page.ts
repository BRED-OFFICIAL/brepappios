import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Router} from '@angular/router';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';


@Component({
  selector: 'app-shipping-address-modal',
  templateUrl: './shipping-address-modal.page.html',
  styleUrls: ['./shipping-address-modal.page.scss'],
})
export class ShippingAddressModalPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
  }

async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  redirectToPaymentSettingPage(){
    this.closeModal();
    this.router.navigate(['/payment-settings']);

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Shipping Address Popup');
  }

}
