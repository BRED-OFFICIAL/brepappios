import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams,LoadingController, ToastController} from '@ionic/angular';
import {SellListPriceModalPage} from '../../pages/sell/sell-list-price-modal/sell-list-price-modal.page';
import {ProductService} from '../../services/product.service';
import { ProfileService} from '../../services/profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selling-listing-detail-modal',
  templateUrl: './selling-listing-detail-modal.page.html',
  styleUrls: ['./selling-listing-detail-modal.page.scss'],
})
export class SellingListingDetailModalPage implements OnInit {

  offer: any;
  parentscope: any;
  product: any;
  productsizes: any;
  highestBid: any;
  lowestask: any;


  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private productService: ProductService,
    public toastController: ToastController,
    private router: Router,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    this.offer = this.navParams.data.d;
    this.parentscope = this.navParams.data.obj;
    console.log(this.offer);
  }
  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }
  close() {
    this.profileService.loadselling(localStorage.getItem('userID'), '', '');
    console.log('closed event');
    this.closeModal();
  }

  parentdelete(item: any){
    this.closeModal();
    this.parentscope.delete(item);
  }

  parentloaddata(){
    this.parentscope.loaddata();
  }

  editListing(item: any){


    this.closeModal();
    this.presentLoader()
    this.productService.getproductpagepagedata(item.product_id)
      .subscribe( (productpagedata: any) => {
        console.log(productpagedata);
        this.product = productpagedata['product'];
        this.productsizes = productpagedata['size'];
        this.highestBid = productpagedata['hightest_bid'];
        this.lowestask = productpagedata['lowest_ask'];
        // this.opnebuyOfferModal(item);
        this.openSellingListingModal(item);
        this.loadingController.dismiss();
      });

  }

  async openSellingListingModal(offer: any){
    const modal = await this.modalController.create({
      component: SellListPriceModalPage,
       cssClass: 'sell-list-price-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.product.id,
        product: this.product,
        productSize: offer.attribute_value,
        productsizes: this.productsizes,
        id: offer.id,
        amount: offer.lowest_ask_price,
        highestBid: this.highestBid,
        lowestAsk: this.lowestask
      }
    });
    
    modal.onDidDismiss().then ((dataReturned) => {
      this.parentloaddata();
    });
    return await modal.present();
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loadertoshow = await this.loadingController.create({
      message: ''
    });
    await loadertoshow.present();

    const { role, data } = await loadertoshow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  redirecttoproductdetial(){
    this.closeModal();
    this.router.navigate(['/productdetail', this.offer.product_id]);

  }

}
