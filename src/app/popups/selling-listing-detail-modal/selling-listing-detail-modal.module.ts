import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellingListingDetailModalPage } from './selling-listing-detail-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SellingListingDetailModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellingListingDetailModalPage]
})
export class SellingListingDetailModalPageModule {}
