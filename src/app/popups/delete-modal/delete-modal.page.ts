import { Component, OnInit } from '@angular/core';
import {NavParams,ModalController} from '@ionic/angular';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.page.html',
  styleUrls: ['./delete-modal.page.scss'],
})
export class DeleteModalPage implements OnInit {

  popupTitle: string;
  popupText: string;
  popupButtonText: string;

  constructor(
    private navParams: NavParams,
    private modalController: ModalController,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.popupTitle =  this.navParams.data.title;
    this.popupText = this.navParams.data.text;
    this.popupButtonText = this.navParams.data.actionbutton;
  }

  close() {
    this.closeModal();
  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  delete(){

    this.closeModal({
      delete: true
    });

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen(this.popupTitle + ' Popup');
  }


}
