import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductHowToBuyPopupPage } from './product-how-to-buy-popup.page';

const routes: Routes = [
  {
    path: '',
    component: ProductHowToBuyPopupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductHowToBuyPopupPage]
})
export class ProductHowToBuyPopupPageModule {}
