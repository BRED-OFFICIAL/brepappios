import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductHowToBuyPopupPage } from './product-how-to-buy-popup.page';

describe('ProductHowToBuyPopupPage', () => {
  let component: ProductHowToBuyPopupPage;
  let fixture: ComponentFixture<ProductHowToBuyPopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductHowToBuyPopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductHowToBuyPopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
