import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import { SignupService } from '../services/signup.service';
import {LoginService} from '../services/login.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, NavController, Platform } from '@ionic/angular';
import {UsernameValidator} from '../validators/username';
import {EmailValidator} from '../validators/email';
import {Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { ThemeableBrowser, ThemeableBrowserObject, ThemeableBrowserOptions } from '@ionic-native/themeable-browser/ngx';
import {ThemeableBrowserOptionConfig} from '../../config/ThemeableBrowserOptions';
import { GoogleAnalyticsService } from '../services/google-analytics.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  data = {
    primary: true,
    secondary: false,
    email: '',
    password: ''
  };

  modal1 = {
    email: '',
    password: ''
  };
  modal2 = {
    email: '',
    password: ''
  };

  submitAttempt1: boolean;
  submitAttempt2: boolean;
  signupstep1: FormGroup;
  signupstep2: FormGroup;
  userID: number;
  socialActivation: number;
  termsofserviceselected: boolean;
  UserData: any;
  subscription: any;




  constructor(
    private signupserv: SignupService,
    private serv: LoginService,
    private router: Router,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public formBuilder: FormBuilder,
    public usernameValidator: UsernameValidator,
    public emailValidator: EmailValidator,
    private route: ActivatedRoute,
    private fb: Facebook,
    private themeableBrowser: ThemeableBrowser,
    private platform: Platform,
    private navCtrl: NavController,
    private gaAnalyticsService: GoogleAnalyticsService
    ) {
      this.submitAttempt1 = false;

      this.signupstep1 = formBuilder.group({
        emailID: ['', Validators.compose([Validators.required, Validators.email]), emailValidator.checkEmail.bind(emailValidator)],
        password: ['', Validators.required],
      });

      this.signupstep2 = formBuilder.group({
        username: ['', Validators.required, usernameValidator.checkusername.bind(usernameValidator)],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        phone: ['', Validators.required],
        shoesize:[''],
        newsletterchecked: [''],
        termsofservicechecked: ['']
      });

      this.onChanges();
     }
  

    onChanges(): void {
      this.signupstep2.valueChanges.subscribe(val => {
        
        this.termsofserviceselected = val.termsofservicechecked;
        console.log(this.termsofserviceselected);
      });
    }

    


  ngOnInit() {

    this.signupstep1.setValue({
      emailID: '',
      password: '',
    }, {onlySelf: true});

    this.signupstep2.setValue({
      username: '',
      firstname: '',
      lastname: '',
      phone: '',
      shoesize: '',
      newsletterchecked: false,
      termsofservicechecked: false
    }, {onlySelf: true});

    this.socialActivation = 0;


    this.route.queryParams.subscribe((params)=> {

      if(params.step == 'step2') {
        this.modal1.email = params.userEmail;
        
        if(params.password !== undefined){
          this.modal1.password = params.password;
        }else{
          this.modal1.password = '';
        }

        this.userID = params.userID;
        localStorage.setItem('NewuserID', params.userID);

        this.signupstep2.setValue({
          username: params.userName,
          firstname: params.firstName,
          lastname: params.lastName,
          phone: '',
          shoesize: '',
          newsletterchecked: false,
          termsofservicechecked: false
        }, {onlySelf: true});


        this.data.secondary = true;
        this.data.primary = false;
        this.socialActivation = 1;
      }


    });

   


  }
  

  continue(){
    this.submitAttempt1 = true;
    if (this.signupstep1.valid) {
      this.modal1.email = this.signupstep1.controls.emailID.value;
      this.modal1.password = this.signupstep1.controls.password.value;
      this.data.secondary = true;
      this.data.primary = false;
      this.gaAnalyticsService.setTrackScreen('Sign up OnBoarding Screen');
    } else {
      console.log(this.signupstep1.controls);

     }
    

  }

  showPrimary(){
    this.data.secondary = false;
    this.data.primary = true;
    this.gaAnalyticsService.setTrackScreen('Sign up Screen');


  }

 async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  signup() {
    this.submitAttempt2 = true;
    
    if (this.signupstep2.valid) {
      this.presentLoader();
    //  this.modal1.email = this.signupstep1.controls.emailID.value;
     // this.modal1.password = this.signupstep1.controls.password.value;
      this.data.secondary = true;
      this.data.primary = false;
      const data: any = {
        username: this.signupstep2.controls.username.value,
        firstname: this.signupstep2.controls.firstname.value,
        lastname: this.signupstep2.controls.lastname.value,
        email:  this.modal1.email ,
        phone: this.signupstep2.controls.phone.value,
        password: this.modal1.password,
        shoesize: this.signupstep2.controls.shoesize.value,
        newsletter: this.signupstep2.controls.newsletterchecked.value,
        socialactivation: this.socialActivation,
        userid: (this.userID !== undefined)?this.userID:''
      };

      this.signupserv.signup(data)
        .subscribe( (userdata: any) => {
            this.UserData = userdata;
            this.loadingController.dismiss();
            if (this.UserData.resp == true) {
              localStorage.setItem('email_to_verify', data.email);
              localStorage.setItem('userid_to_verify', userdata.data.id);
              this.router.navigate(['/accountverify']);
            } else {
              this.presentToast(this.UserData.message);
            }
      },(error)=> {
        this.presentToast('Couldn\'t connect to server');
        this.loadingController.dismiss();
      });


    } else {
      console.log(this.signupstep1.controls);

     }
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  fbLogin(){
    this.presentLoader();
    this.fb.getLoginStatus().then((res) => {
      if (res.status === 'connected') {
        console.log(res);
        this.fb.api('me?fields=id,name,email,gender,locale,picture',[]).then((userData) => {
          // console.log(userData);
          this.call_facebook_login(userData);


        });
      
          // Already logged in to FB so pass credentials to provider (in my case firebase)
          // let provider = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
          // firebase.auth().signInWithCredential(provider).then((authToken) => {
          //    this.authToken = authToken;
          // });
      } else {
          // Not already logged in to FB so sign in
          this.fb.login(['public_profile','email']).then((userData1) => {
            console.log(userData1);
            this.fb.api('me?fields=id,name,email,gender,locale,picture',[]).then((userData)=> {
              console.log(userData);
              this.call_facebook_login(userData);
            });
              // FB Log in success
          }).catch((error) => {
            console.log('Error logging into Facebook', error);
              // FB Log in error
          });
      }
  });
  //   this.fb.login(['public_profile', 'user_friends', 'email'])
  // .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
  // .catch(e => console.log('Error logging into Facebook', e));


    this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);

 }

 call_facebook_login(data: any){
  this.loadingController.dismiss();
  this.serv.facebooklogin(data).subscribe((response: any) => {
    console.log(response);
    if (response.status) {
      localStorage.setItem('userID', response.userID);
      localStorage.setItem('userEmail', response.userEmail);
      this.router.navigate(['/tabs/home']);
    }else{
      if(response.code == 'step2'){
        let navigationExtras: NavigationExtras = {
          queryParams: {
            userID: response.userID,
            userEmail: response.userEmail,
            firstName: response.userData.first_name,
            lastName:  response.userData.last_name,
            userName: response.userData.user_name,
            step: 'step2'
          }
        };
        this.router.navigate(['/signup'],navigationExtras);
      } else if (response.code == 'confirm_email'){
        localStorage.setItem('userid_to_verify', response.userID);
        this.router.navigate(['/accountverify']);
      }

      
    }


  });

 }

openbrowser(url:string) {
  const options: ThemeableBrowserOptions = ThemeableBrowserOptionConfig;
  const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
}

ionViewDidEnter(){

  this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
  console.log('subscribeWithPriority');
    if (this.router.isActive('/signup', true) && this.router.url === '/signup') {
      if(this.data.secondary){
        this.data.secondary = false;
        this.data.primary = true;
      }else{
        this.navCtrl.back();
      }
      
    }


});
}

ionViewWillLeave(){
 
  this.subscription.unsubscribe();
}

ionViewWillEnter(){
  this.gaAnalyticsService.setTrackScreen('Sign up Screen');
}


}
