import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuynowTabPage } from './buynow-tab.page';

describe('BuynowTabPage', () => {
  let component: BuynowTabPage;
  let fixture: ComponentFixture<BuynowTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuynowTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuynowTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
