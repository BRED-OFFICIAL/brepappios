import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyreviewpurchaseModalPage } from './buyreviewpurchase-modal.page';

describe('BuyreviewpurchaseModalPage', () => {
  let component: BuyreviewpurchaseModalPage;
  let fixture: ComponentFixture<BuyreviewpurchaseModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyreviewpurchaseModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyreviewpurchaseModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
