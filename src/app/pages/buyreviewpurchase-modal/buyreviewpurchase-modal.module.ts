import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuyreviewpurchaseModalPage } from './buyreviewpurchase-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BuyreviewpurchaseModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuyreviewpurchaseModalPage]
})
export class BuyreviewpurchaseModalPageModule {}
