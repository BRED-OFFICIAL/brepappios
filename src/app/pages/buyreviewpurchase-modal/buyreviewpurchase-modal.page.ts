import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams,LoadingController, Platform, ToastController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { OrderService } from '../../services/order.service';
import { BuypopupTabsPage } from '../../pages/buypopup-tabs/buypopup-tabs.page'
import { BuysuccessModalPage } from '../../pages/buysuccess-modal/buysuccess-modal.page';
import {PaymentEditModalPage} from '../../pages/payment-edit-modal/payment-edit-modal.page';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
@Component({
  selector: 'app-buyreviewpurchase-modal',
  templateUrl: './buyreviewpurchase-modal.page.html',
  styleUrls: ['./buyreviewpurchase-modal.page.scss'],
})
export class BuyreviewpurchaseModalPage implements OnInit {
  productid: number;
  buyid: any;
  askid: number;
  lowestAskVal: number;
  discountPrice: number;
  prodSize: number;
  prodSizeValue: number;
  discountCoupn: number;
  discountCoupnId: number;
  discountCoupnType: number;
  bidorbuy: any;
  credit: number;
  tokenId: any;
  product: any;
  loadertoshow: any;
  buyerpaymentinformation: any;
  buyershippinginformation: any;
  buyerbillinginformation: any;
  subtotal: number;
  shippingamount: number;
  paymentprocamount: number;  //Payment Proc: 3.5%
  authenticationfee: number;
  transfee: number;

  price: number; // Product price + shipping chargs + buyer fee
  totalamount: number;
  buyerfee: number;
  loadervisiblecheck: any;
  loaderToShow: any;

  offerid: number;

  fingerPrintOptions: FingerprintOptions;


 

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private productpageserv: ProductService,
    private orderService: OrderService,
    private loadingController: LoadingController,
    private fingerPrint: FingerprintAIO,
    private platform: Platform,
    private toastController: ToastController
) { 

  this.fingerPrintOptions = {
    title:'Confirm Purchase',
    // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
     //clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
     disableBackup:true,  //Only for Android(optional)
    // localizedFallbackTitle: 'Use Pin', //Only for iOS
   //  localizedReason: 'Please authenticate' //Only for iOS
 };


}

  ngOnInit() {
    this.showLoader();
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.lowestAskVal = Number(this.navParams.data.lowestPrice);
    this.totalamount = this.navParams.data.ordertotal;
    this.shippingamount = Number(this.navParams.data.buyershippingfee);
    this.prodSize = Number(this.navParams.data.size);
    this.prodSizeValue = Number(this.navParams.data.sizeValue);
    this.paymentprocamount = 0;
    this.authenticationfee = 0;
    this.totalamount = 0;

    if (this.navParams.data.offerid !== undefined){
      this.offerid = this.navParams.data.offerid;
    }
    
    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      this.buyid = localStorage.getItem('userID');
    }

    if (this.buyid !== '') {

      this.subtotal = 0.00;
      this.shippingamount = 0.00;
      this.paymentprocamount = 0.00;
      this.authenticationfee = 0.00;
      this.totalamount = 0.00;
     






      this.productpageserv.getbuyerallinformation(this.buyid)
        .subscribe( (buyeralldata: any) => {
          this.buyerpaymentinformation = buyeralldata['payment'];
          this.buyershippinginformation = buyeralldata['shipping'];
          this.buyerbillinginformation = buyeralldata['billing'];
          this.buyerfee = buyeralldata['buyerfee'];
          this.transfee = buyeralldata['trans_fee'];
          this.shippingamount = parseInt(buyeralldata['buyerfee']);
          console.log(this.buyershippinginformation);

          this.price = +this.lowestAskVal + +this.shippingamount;
          this.paymentprocamount = (this.price * 0.035);
         // this.totalamount = parseInt(parseInt(this.lowestAskVal) +  parseInt(this.price) + parseInt(this.paymentprocamount));
          this.totalamount = +this.lowestAskVal + +this.totalamount;
          this.totalamount = +this.shippingamount + +this.totalamount;
          this.totalamount = +this.paymentprocamount.toFixed(2) + +this.totalamount;
          this.loadingController.dismiss();
          this.loadervisiblecheck = setInterval( () => {
            try {
              this.loadingController.dismiss();
            } catch (err) {
              clearTimeout(this.loadervisiblecheck);
            } finally {
              clearTimeout(this.loadervisiblecheck);
            }
  
  
          }, 1000);




        });


    }
  }

  openbuynowpopup(){
    console.log('openbuynowpopup');
    this.closeModal({
      openmodal: 'BuypopupTabsPage',
      data: {
        productid: this.productid,
        product: this.product
      }
    });
  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }


  ionViewDidLoad() {
    console.log("I'm alive!");
  }
  ionViewWillLeave() {
    console.log("Looks like I'm about to leave :(");
  }

  async purchase() {
    //this.showLoader();
    // console.log('Product ID' + this.productid );
    // console.log('Product Size' + this.prodSize );
    // console.log('User ID' + this.buyid );
    // console.log('lowestask_val' + this.lowestAskVal );
    // console.log('ask_id' + this.askid );

    try{
      await this.platform.ready();
      const available = await this.fingerPrint.isAvailable();
      
      if(available == "biometric"){
        this.fingerPrint.show(this.fingerPrintOptions)
        .then((result: any) => {
          console.log(result)
          if(result == 'biometric_success'){
            this.showLoader();
            this.orderService.buynow(
              this.productid,
              this.buyid,
              this.prodSize,
              this.lowestAskVal,
              this.shippingamount,
              this.paymentprocamount.toFixed(2),
              this.totalamount.toFixed(2),
              this.askid,
              null,
              null,
              null,
              this.offerid)
            .subscribe( (response: any) => {
              if(response.status){
                this.loadsuccessordermodal(response.order);
              } else {
                if(response.errorCode=='decline'){
                  this.presentToast('Payment has been declined. Error: '+ response.payment.message);
                }else{
                  this.presentToast('Something went wrong.');
                }
              }
              this.loadervisiblecheck = setTimeout( () => {
                  this.loadingController.dismiss();
                }, 1000);
            });
          }
        })
        .catch((error: any) => {
          console.log(error);
          this.presentToast('Finger touch lock doesn\'t match.');

        });
      }
    } catch(e) {
        console.log('Fingure tyr errr:', e);
        // this.presentToast('Finger touch lock is not exists.');
        this.showLoader();
      this.orderService.buynow(
        this.productid,
        this.buyid,
        this.prodSize,
        this.lowestAskVal,
        this.shippingamount,
        this.paymentprocamount.toFixed(2),
        this.totalamount.toFixed(2),
        this.askid,
        null,
        null,
        null,
        this.offerid)
      .subscribe( (response: any) => {
        if(response.status){
          this.loadsuccessordermodal(response.order);
        } else {
          if(response.errorCode=='decline'){
            this.presentToast('Payment has been declined. Error: '+ response.payment.message);
          }else{
            this.presentToast('Something went wrong.');
          }
        }
        this.loadervisiblecheck = setTimeout( () => {
            this.loadingController.dismiss();
          }, 1000);
      });
    }
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        clearTimeout(this.loadervisiblecheck);
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  async loadsuccessordermodal(order){

    this.closeModal();
    const modal = await this.modalController.create({
      component: BuysuccessModalPage,
      cssClass: 'buysuccess-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        order: order,
        productsize: this.prodSize
      }
    });

    // modal.onDidDismiss().then(data => {
    //     console.log(data);
    //     if(data.data.closed) {
    //       setTimeout(() => {
    //         this.closeModal();
    //         this.loadingController.dismiss();

    //       }, 500);
    //     }

    //     //console.log('dismissed', data);
    // });

    return await modal.present();

  }


  async paymentedit() {
   // this.closeModal();
    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.buyerpaymentinformation,
        billing: this.buyerbillinginformation,
        shipping: this.buyershippinginformation,
        paymentIdentifer: 'buyer_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      this.ngOnInit();
    });
    return await modal.present();
  }

  async openself() {
    const modal = await this.modalController.create({
      component: BuyreviewpurchaseModalPage,
       cssClass: 'buyreviewpurchase-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        lowestPrice: this.lowestAskVal,
        ordertotal: this.totalamount,
        buyershippingfee: this.shippingamount
      }
    });

    modal.onDidDismiss().then((response: any) => {
      // console.log(response.data.modal);
      if (response !== null) {
        if (response.data.openmodal === 'BuypopupTabsPage'){
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          //this.buypopuptab(response.data.data);
        }
      }
    });



    return await modal.present();


  }

  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

}
