import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuypopupTabsPage } from './buypopup-tabs.page';

describe('BuypopupTabsPage', () => {
  let component: BuypopupTabsPage;
  let fixture: ComponentFixture<BuypopupTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuypopupTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuypopupTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
