import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams, LoadingController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { BuyreviewpurchaseModalPage } from '../../pages/buyreviewpurchase-modal/buyreviewpurchase-modal.page';
import { BuyoffersTabPage } from '../buyoffers-tab/buyoffers-tab.page';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

import { promise } from 'protractor';
@Component({
  selector: 'app-buypopup-tabs',
  templateUrl: './buypopup-tabs.page.html',
  styleUrls: ['./buypopup-tabs.page.scss'],
})
export class BuypopupTabsPage implements OnInit {

  productid: number;
  data = {showmore:false,showless:true,product_id:0};
  productsizes = [];
  buynowvisible = true;
  offersvisible = false;
  sizechartvisible = false;
  modalvisible = false;
  product: any;
  loaderToShow: any;
  isHaveLowestAsk: boolean;

  constructor(
      private modalController: ModalController,
      private navParams: NavParams,
      private navCtrl: NavController,
      private route: ActivatedRoute,
      private router: Router,
      private productpageserv: ProductService,
      private loadingController: LoadingController,
      private gaAnalyticsService: GoogleAnalyticsService

  ){ }
  ngOnInit() {
    this.presentLoader();
    
     this.productid = this.navParams.data.productid;
   // this.product = this.navParams.data.product;

    this.productpageserv.fetch().subscribe((product)=>{
      this.product = product.product;
      this.productid = this.product.id;
    });
    

    // this.productpageserv.fetchProductSize().subscribe((productpagedata: any)=>{
    //   console.log('New product size load', productpagedata);
    //   this.productsizes = productpagedata.size;
    //   this.loadingController.dismiss();
    //   this.modalvisible = true;
    // });
    this.isHaveLowestAsk = false;
    this.loadProductSize().then(()=>{
     setTimeout(()=>{
       this.loadingController.dismiss();
      },2000);
    });
    

    if (this.navParams.data.defaulttab !== undefined) {
      if (this.navParams.data.defaulttab === 'offers') {
        this.offersvisible = true;
        this.buynowvisible = false;
        this.sizechartvisible = false;

      }


    }


    // this.route.queryParams.subscribe(params => {
    //   this.productpageserv.fetchProductSize().subscribe((productpagedata: any)=>{
    //     this.productsizes = productpagedata.size;
    //     this.loadingController.dismiss();
    //     this.modalvisible = true;
    //   });
      // if (this.productid) {
      //   this.data.product_id = this.productid;
      //   this.productpageserv.getproductsize(this.data.product_id)
      // .subscribe( (productpagedata) => {
      //   this.productsizes = productpagedata['size'];
      //   console.log(this.productsizes);
      //   this.loadingController.dismiss();
      //   this.modalvisible = true;
      // });

      // }
    //});





  }

  async closeModal() {
    const onClosedData = 'Wrapped Up!';
    console.log('test test');
    await this.modalController.dismiss(onClosedData);
  }

  async loadProductSize(){
    await this.productpageserv.getproductsize(this.productid)
      .subscribe( (productpagedata) => {
        this.productsizes = productpagedata['size'];
        for(const value of this.productsizes){
          if(value.lowest_ask){
            this.isHaveLowestAsk = true;
            break;
          }
        }
        this.modalvisible = true;
    });
  }

  showbuynowsection(){
    this.buynowvisible=true;
    this.offersvisible = false;
    this.sizechartvisible = false;
    this.gaAnalyticsService.setTrackScreen('Buy Popup Screen: Buy Now');
  }
  showofferssection(){
    this.buynowvisible=false;
    this.offersvisible = true;
    this.sizechartvisible = false;
    this.gaAnalyticsService.setTrackScreen('Buy Popup Screen: Place Offer');
  }
  showsizechart(){
    this.buynowvisible =false;
    this.offersvisible = false;
    this.sizechartvisible = true;
    this.gaAnalyticsService.setTrackScreen('Buy Popup Screen: Size Chart');

  }
  async reviewpurchasemodal(lowestPrice,size, askid, sizevalue ){
    this.closeModal();
    console.log(localStorage.getItem('userID'));

    if(localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      
      const modal = await this.modalController.create({
        component: BuyreviewpurchaseModalPage,
        // cssClass: 'buypopup-tabs',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.productid,
          product: this.product,
          lowestPrice: lowestPrice,
          size: size,
          askid: askid,
          sizeValue: sizevalue
        }
      });

      modal.onDidDismiss().then((response) => {
        if (response !== null) {
          if (response.data.openmodal == 'BuypopupTabsPage'){
            //response.data.data.defaulttab = 'offers';
            this.buypopuptab(response.data.data);
          }
        }
      });

      return await modal.present();
    } else {
      this.showLoader();
      let navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          lowestPrice: lowestPrice,
          size: size,
          askid: askid,
          modal: 'BuyreviewpurchaseModalPage',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
      


    }
  }

  async buypopuptab(data: any) {
    const modal = await this.modalController.create({
      component: BuypopupTabsPage,
      // cssClass: 'buypopup-tabs',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: data
    });

    return await modal.present();



  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  imageloaded(){
    this.loadingController.dismiss();
  }


  async buyoffersmodal( sizeid: any,highestbid = {}, lowestask = {}, sizevalue: number ){
    this.closeModal();
    console.log(localStorage.getItem('userID'));

    if(localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      
      const modal = await this.modalController.create({
        component: BuyoffersTabPage,
        cssClass: 'buyoffers-tab',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.productid,
          productSize: sizeid,
          product: this.product,
          productsizes: this.productsizes,
          highestBid: highestbid,
          lowestAsk: lowestask,
          sizeValue: sizevalue
          }
      });
      modal.onDidDismiss().then((dataReturned) => {

        if (dataReturned.data.openmodel) {
          this.loadmymodal(this.productid, 'offers');
        }

        

        
      });

      return await modal.present();
    } else {
      this.showLoader();
      let navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          productSize: sizeid,
          modal: 'BuyoffersTabPage',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
      


    }
  }

  async loadmymodal(id: number, tab: string) {
    const modal = await this.modalController.create({
      component: BuypopupTabsPage,
      cssClass: 'buypopup-tabs',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: id,
        product: this.product,
        defaulttab: tab
      }
    });
    return await modal.present();
  }

  doRefresh($event){
    this.productpageserv.loadProduct(this.productid);
    this.loadProductSize().then(()=>{
      setTimeout(() => {
        $event.target.complete();
      }, 2000);
    });

  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Buy Popup Screen');
  }

}
