import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuypopupTabsPage } from './buypopup-tabs.page';
import { BuyreviewpurchaseModalPageModule } from '../../pages/buyreviewpurchase-modal/buyreviewpurchase-modal.module';
import { BuysuccessModalPageModule } from '../../pages/buysuccess-modal/buysuccess-modal.module';
import {BuyoffersTabPageModule} from '../buyoffers-tab/buyoffers-tab.module';
import {BuyoffersreviewModalPageModule} from '../buyoffersreview-modal/buyoffersreview-modal.module';
import {BuyoffersuccessModalPageModule} from '../buyoffersuccess-modal/buyoffersuccess-modal.module';
const routes: Routes = [
  {
    path: 'buypoptabs',
    component: BuypopupTabsPage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuyreviewpurchaseModalPageModule,
    BuysuccessModalPageModule,
    BuyoffersTabPageModule,
    BuyoffersreviewModalPageModule,
    BuyoffersuccessModalPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuypopupTabsPage]
})
export class BuypopupTabsPageModule {}
