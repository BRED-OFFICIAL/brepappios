import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {ModalController,NavController, NavParams, LoadingController, AlertController, Platform, ToastController} from '@ionic/angular';
import {ProductService} from '../../../services/product.service';
import {OrderService} from '../../../services/order.service';
import {SellSuccessModalPage} from '../sell-success-modal/sell-success-modal.page';
import {PaymentEditModalPage} from '../../payment-edit-modal/payment-edit-modal.page';
import {PayoutEditModalPage} from '../../payout-edit-modal/payout-edit-modal.page';
import { NumberFormatStyle } from '@angular/common';
import { PayoutEditModalPageModule } from '../../payout-edit-modal/payout-edit-modal.module';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-sell-review-modal',
  templateUrl: './sell-review-modal.page.html',
  styleUrls: ['./sell-review-modal.page.scss'],
})
export class SellReviewModalPage implements OnInit {
  productid: number;
  sellerid: number;
  bidId: number;
  prodSizeId: number;
  size: number;
  discountCoupon: number;
  discountCouponId: number;
  discountCouponType: number;
  
  credit: number;
  tokenId: any;
  product: any;
  sellerpaymentinformation: any;
  sellershippinginformation: any;
  sellerbillinginformation: any;
  subtotal: number;
  shippingamount: number;
  paymentprocamount: number;
  authenticationfee: number;
  transfee: number;
  transfeeamount: number;
  price: number;
  totalamount: number;
  sellerfee: number;
  loadervisiblecheck: any;
  termsaccepted: boolean;
  productsizeselected: any;
  loaderToShow: any;
  payoutinfo: any;

  isauthenticchecked: boolean;
  isshippedchecked: boolean;
  offerid: number;
  
  
  fingerPrintOptions: FingerprintOptions;

  constructor(
    private modalController: ModalController,
    private productService: ProductService,
    private orderService: OrderService,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private alertController: AlertController,
    private toastController: ToastController,
    private fingerPrint: FingerprintAIO,
    private platform: Platform,
    private gaAnalyticsService: GoogleAnalyticsService
  ) {
    this.fingerPrintOptions = {
      title:'Confirm Sale',
      // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      //clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      disableBackup:true,  //Only for Android(optional)
      // localizedFallbackTitle: 'Use Pin', //Only for iOS
      //  localizedReason: 'Please authenticate' //Only for iOS
    };


   }

  ngOnInit() {
    console.log(this.navParams.data);
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.price = Number(this.navParams.data.bidPrice);
    this.shippingamount = Number(this.navParams.data.buyershippingfee);
    this.size = Number(this.navParams.data.size);
    this.bidId = Number(this.navParams.data.highestbidid);
    this.paymentprocamount = 0;
    this.authenticationfee = 0;
    this.totalamount = 0;
    this.isauthenticchecked = false;
    this.isshippedchecked = false;


    if (this.navParams.data.offerid !== undefined){
      this.offerid = this.navParams.data.offerid;
    }

    


    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') != null ) {
      this.sellerid = Number(localStorage.getItem('userID'));
    }

    if (this.sellerid) {
      this.subtotal = 0.00;
      this.shippingamount = 0.00;
      this.paymentprocamount = 0.00;
      this.authenticationfee = 0.00;
      this.totalamount = 0.00;
    }
    this.presentLoader();
    this.productService.getsellerallinformation(this.sellerid)
    .subscribe( (buyeralldata: any) => {
      console.log('api called');
      this.sellerpaymentinformation = buyeralldata['payment'];
      this.sellershippinginformation = buyeralldata['shipping'];
      this.sellerbillinginformation = buyeralldata['billing'];
      this.sellerfee = buyeralldata['sellerfee'];
      this.transfee = Number(buyeralldata['trans_fee']);
      this.transfeeamount =  Number(this.price * (this.transfee / 100));
      this.shippingamount = Number(buyeralldata['sellerfee']);
      this.payoutinfo = buyeralldata['payoutinfo'];
      this.transfeeamount = Number(this.transfeeamount.toFixed(2));
      this.subtotal = this.price - this.shippingamount;
      this.paymentprocamount = (this.price * 0.035);
      this.totalamount = +this.subtotal + +this.totalamount;
      this.totalamount = +this.totalamount - +this.paymentprocamount.toFixed(2);
      this.totalamount = +this.totalamount - this.transfeeamount;
      this.loadingController.dismiss();
      
      // this.loadervisiblecheck = setTimeout( () => {
      //   //this.loadingController.dismiss();
      // }, 3000);
    });
  }
 

  async presentLoader() {
    const loading = await this.loadingController.create({
      message: ''
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }


  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  async sell() {
    try{
      await this.platform.ready();
      const available = await this.fingerPrint.isAvailable();
      this.fingerPrint.show(this.fingerPrintOptions)
      .then((result: any) => {
        if(result == 'biometric_success'){
          this.processSale();
        }
      })
      .catch((error: any) => {
        console.log(error);
        this.presentToast('Finger touch lock doesn\'t match.');
      });
    } catch(e) {
      this.processSale();
    }
  }

  processSale(){
    if (this.isauthenticchecked && this.isshippedchecked) {
      this.presentLoader();
      this.orderService.sellnow(
        this.productid,
        localStorage.getItem('userID'),
        this.size,
        this.price,
        this.shippingamount,
        this.paymentprocamount.toFixed(2),
        this.transfeeamount.toFixed(2),
        this.totalamount.toFixed(2),
        this.bidId,
        null,
        null,
        null,
        this.offerid)
      .subscribe( (response: any) => {
        this.loadervisiblecheck = setTimeout( () => {
          this.loadingController.dismiss();
        }, 1000);
        if (response.status) {
            this.loadsuccessordermodal(response.order);
        } else {
          this.alert('Something went wrong. Try again');
        }
      });
    }
  }

  opensellpricepopup() {
    this.closeModal({
      openmodal: 'SellPriceModalPage',
      data: {
        productid: this.productid,
        product: this.product,
        price: this.price
      }
    });


  }

  authenticCheckboxTrigger(e: any) {
    this.isauthenticchecked = e.detail.checked;
  }

  shipCheckboxTrigger(e: any) {
    this.isshippedchecked = e.detail.checked;
  }
  Checkboxfocus(){

  }

  async alert(messageText: any) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: messageText,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();



  }

  async loadsuccessordermodal(orderDetail: any){

    this.closeModal();
    const modal = await this.modalController.create({
      component: SellSuccessModalPage,
      cssClass: 'sell-success-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        order: orderDetail,
        productsize: this.size
      }
    });
    return await modal.present();
  }

  async paymentedit() {

    this.closeModal();
    console.log(this.sellerbillinginformation);
    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.sellerpaymentinformation,
        billing: this.sellerbillinginformation,
        shipping: this.sellershippinginformation,
        paymentIdentifer: 'seller_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      console.log(response);
          

      if (response !== null) {
        if (response.data.openModal === 'SellListReview'){
          console.log(response.data);
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          this.openself();
        }
      } 

    });
    return await modal.present();
  }

  async openself() {
    const modal = await this.modalController.create({
      component: SellReviewModalPage,
       cssClass: 'sell-review-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        bidPrice: this.price,
        highestbidid: this.bidId,
        size: this.size,
        buyershippingfee: this.shippingamount

      }
    });

    modal.onDidDismiss().then((response: any) => {
      // console.log(response.data.modal);
      if (response !== null) {
        if (response.data.openmodal === 'SellPriceModalPage'){
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          //this.sellPriceTab(response.data.data);
        }
      }
    });



    return await modal.present();


  }

  async payoutedit() {

    this.closeModal();
    console.log(this.sellerbillinginformation);
    const modal = await this.modalController.create({
      component: PayoutEditModalPage,
      cssClass: 'payout-edit-modal',
      componentProps: {
        payoutInfo: this.payoutinfo,
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      console.log(response);
          

      if (response !== null) {
        if (response.data.openModal === 'SellListReview'){
          console.log(response.data);
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          this.openself();
        }
      } 

    });
    return await modal.present();
  }

  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sale Review Screen');
  }
}
