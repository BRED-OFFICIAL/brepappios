import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellReviewModalPage } from './sell-review-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SellReviewModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellReviewModalPage]
})
export class SellReviewModalPageModule {}
