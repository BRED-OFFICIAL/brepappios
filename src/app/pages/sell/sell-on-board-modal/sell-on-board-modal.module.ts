import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellOnBoardModalPage } from './sell-on-board-modal.page';
import {SellPriceModalPageModule} from '../sell-price-modal/sell-price-modal.module';

const routes: Routes = [
  {
    path: '',
    component: SellOnBoardModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SellPriceModalPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellOnBoardModalPage]
})
export class SellOnBoardModalPageModule {}
