import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';
import { SellPriceModalPage } from '../sell-price-modal/sell-price-modal.page';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-sell-on-board-modal',
  templateUrl: './sell-on-board-modal.page.html',
  styleUrls: ['./sell-on-board-modal.page.scss'],
})
export class SellOnBoardModalPage implements OnInit {

  product: any;
  productid: any;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private gaAnalyticsService: GoogleAnalyticsService 
    ) { }

  ngOnInit() {
    this.product = this.navParams.data.product;
    this.productid = this.navParams.data.productid;
  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  async loadsellpage() {
    this.closeModal();
    const modal = await this.modalController.create({
      component: SellPriceModalPage,
       cssClass: 'sell-price-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
      }
    });
    return await modal.present();
  }

  checkedmessagepopupagain(e: any) {
    localStorage.setItem('sellinstructionmessagepopup', e.detail.checked);
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sell OnBoarding Screen');
  }

}
