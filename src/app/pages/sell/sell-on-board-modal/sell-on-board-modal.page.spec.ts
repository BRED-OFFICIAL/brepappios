import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellOnBoardModalPage } from './sell-on-board-modal.page';

describe('SellOnBoardModalPage', () => {
  let component: SellOnBoardModalPage;
  let fixture: ComponentFixture<SellOnBoardModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellOnBoardModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellOnBoardModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
