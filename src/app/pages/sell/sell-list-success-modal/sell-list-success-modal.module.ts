import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellListSuccessModalPage } from './sell-list-success-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SellListSuccessModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellListSuccessModalPage]
})
export class SellListSuccessModalPageModule {}
