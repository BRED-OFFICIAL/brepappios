import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-sell-list-success-modal',
  templateUrl: './sell-list-success-modal.page.html',
  styleUrls: ['./sell-list-success-modal.page.scss'],
})
export class SellListSuccessModalPage implements OnInit {
  productid: number;
  listing: any;
  amount: number;
  product: any;
  productsize: any;
  loaderToShow: any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private loadingController: LoadingController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.listing = this.navParams.data.listing;
    this.productsize = this.navParams.data.productsize;
    this.amount = this.navParams.data.price;
    console.log(this.navParams.data);
  }
  async closeModal() {
    const data = {closed: true};
    data.closed = true;
    await this.modalController.dismiss(data);
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  redirectSellOrderDetail(){
    this.closeModal();
    this.router.navigate(['/selling'],{queryParams:{tab:'listing',id: this.listing.confirm_ask}});
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sell List Success Screen');
  }

}
