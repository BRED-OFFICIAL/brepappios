import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';
import {Router, NavigationExtras} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import {SellReviewModalPage} from '../sell-review-modal/sell-review-modal.page';
import {SellListPriceModalPage} from '../sell-list-price-modal/sell-list-price-modal.page';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-sell-price-modal',
  templateUrl: './sell-price-modal.page.html',
  styleUrls: ['./sell-price-modal.page.scss'],
})
export class SellPriceModalPage implements OnInit {
  product: any;
  productid: any;
  productsizes: any;
  loaderToShow: any;
  sellnowvisible: boolean;
  listpricevisible: boolean;
  sizechartvisible: boolean;
  isHaveHighestOffer: boolean;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private productService: ProductService,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService 
  ) { }

  ngOnInit() {
    this.product = this.navParams.data.product;
    this.productid = this.navParams.data.productid;

    this.sellnowvisible = true;
    this.listpricevisible = false;
    this.sizechartvisible = false;

    if ( this.navParams.data.defaulttab !== undefined){
      if ( this.navParams.data.defaulttab == 'listprice'){
        this.sellnowvisible = false;
        this.listpricevisible = true;
        this.sizechartvisible = false;

      }

    }
    this.presentLoader();
    this.productService.getproductsize(this.productid)
      .subscribe( (productpagedata) => {
        this.productsizes = productpagedata['size'];
        for(const value of this.productsizes){
          if(value.lowest_ask){
            this.isHaveHighestOffer = true;
            break;
          }
        }
        // console.log(this.productsizes);
        this.loadingController.dismiss();
        // this.modalvisible = true;
      });


  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  showsellnowsection(){
    this.sellnowvisible = true;
    this.listpricevisible = false;
    this.sizechartvisible = false;
    this.gaAnalyticsService.setTrackScreen('Sell Screen: Sell Now');
  }
  showlistpricesection(){
    this.sellnowvisible = false;
    this.listpricevisible = true;
    this.sizechartvisible = false;
    this.gaAnalyticsService.setTrackScreen('Sell Screen: List Price');
  }
  showsizechart(){
    this.sellnowvisible = false;
    this.listpricevisible = false;
    this.sizechartvisible = true;
    this.gaAnalyticsService.setTrackScreen('Sell Screen: Size Chart');

  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }


  async reviewsellmodal(price: string, productSizeId: number, highestBidId: number, sizeValue: number ) {
    this.closeModal();
    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') !== null ) {
      const modal = await this.modalController.create({
        component: SellReviewModalPage,
        cssClass: 'sell-review-modal',
        componentProps: {
          productid: this.productid,
          product: this.product,
          bidPrice: price,
          productsizeid: productSizeId,
          highestbidid: highestBidId,
          size: sizeValue
        }
      });

      modal.onDidDismiss().then((response) => {
        if (response !== null) {
          if (response.data.openmodal == 'SellPriceModalPage'){
            //response.data.data.defaulttab = 'offers';
            this.loadmymodal(response.data.data);
          }
        }
      });

      return await modal.present();
    } else {
      //this.showLoader();
      let navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          bidPrice: price,
          productsizeid: productSizeId,
          highestbidid: highestBidId,
          size: sizeValue,
          modal: 'sellPriceModal',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
    }

  }

  async sellListPricemodal(id: number, highestbid = {}, lowestask = {}) {
    this.closeModal();
    

    if(localStorage.getItem('userID') !== '' && localStorage.getItem('userID') != null ) {
      const modal = await this.modalController.create({
        component: SellListPriceModalPage,
        cssClass: 'sell-list-price-modal',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.productid,
          productSize: id,
          product: this.product,
          productsizes: this.productsizes,
          highestBid: highestbid,
          lowestAsk: lowestask
          }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data.openmodal == 'SellPriceModalPage'){
          this.loadmymodal(dataReturned.data.data.productid, 'listprice');
        }
      });
      return await modal.present();
    } else {
     // this.showLoader();
      const navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          productSize: id,
          modal: 'SellListPrice',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
    }
  }

  async loadmymodal(data: any, tab = '') {
    const modal = await this.modalController.create({
      component: SellPriceModalPage,
      cssClass: 'sell-price-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        defaulttab: tab
      }
    });
    return await modal.present();

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sell Screen');
  }

}
