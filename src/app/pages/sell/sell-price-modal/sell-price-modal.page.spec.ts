import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellPriceModalPage } from './sell-price-modal.page';

describe('SellPriceModalPage', () => {
  let component: SellPriceModalPage;
  let fixture: ComponentFixture<SellPriceModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellPriceModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellPriceModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
