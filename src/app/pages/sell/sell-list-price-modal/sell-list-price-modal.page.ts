import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams, LoadingController} from '@ionic/angular';
import { ActivatedRoute,NavigationExtras, Router } from '@angular/router';
import { SellListSuccessModalPage} from '../sell-list-success-modal/sell-list-success-modal.page';
import {SellListReviewModalPage} from '../sell-list-review-modal/sell-list-review-modal.page';
import {SellReviewModalPage} from '../sell-review-modal/sell-review-modal.page';
import {ProductService} from '../../../services/product.service';
import {OrderService} from '../../../services/order.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-sell-list-price-modal',
  templateUrl: './sell-list-price-modal.page.html',
  styleUrls: ['./sell-list-price-modal.page.scss'],
})
export class SellListPriceModalPage implements OnInit {

  product: any;
  productid: number;
  productsizes: any;
  productsizeselected: any;
  highestBid: any;
  lowestask: any;
  productsizeid: number;
  loaderToShow: any;
  askamount: string;
  askamountnumber: number;
  processFee: number;
  transfee: number;
  sellerfee: number;
  ordertotal: number;
  askmessage: string;
  loadervisiblecheck: any;
  sellerid: number;

  id: number;
  previousamount: number;

  constructor(
    private navParams: NavParams,
    private route: ActivatedRoute,
    private modalController: ModalController,
    // private productservice: ProductService,
    private loadingController: LoadingController,
    private productService: ProductService,
    private orderService: OrderService,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService 
  ) { }

  ngOnInit() {
    this.presentLoader();
    this.product = this.navParams.data.product;
    this.productid = this.navParams.data.productid;
    this.productsizeid = this.navParams.data.productSize;
    this.highestBid = this.navParams.data.highestBid;
    this.lowestask = this.navParams.data.lowestAsk;
    this.productsizes = this.navParams.data.productsizes;
    if (this.navParams.data.productsizeselcted != undefined){
      this.productsizeselected = this.navParams.data.productsizeselcted;
    }
    


    this.askamount = '0';
    this.askamountnumber = 0;
    this.ordertotal = 0;

    if (this.navParams.data.id !== undefined){
      this.id = this.navParams.data.id;
    }
    if (this.navParams.data.amount !== undefined){
      this.previousamount = this.askamount = this.askamountnumber = this.navParams.data.amount;
    }

    
    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') != null ) {
      this.sellerid = Number(localStorage.getItem('userID'));
    }

    this.productService.getsellerallinformation(this.sellerid)
    .subscribe( (buyeralldata: any) => {
      this.sellerfee = buyeralldata['sellerfee'];
      this.transfee = Number(buyeralldata['trans_fee']);
      this.loadervisiblecheck = setInterval( () => {
        try {
          this.loadingController.dismiss();
        } catch (err) {
          clearTimeout(this.loadervisiblecheck);
        } finally {
          clearTimeout(this.loadervisiblecheck);
        }
      }, 1000);
    });

    if (!this.highestBid || !this.productsizeselected) {
      this.productService.getproductsize(this.productid)
      .subscribe( (productpagedata) => {
        this.productsizes = productpagedata['size'];
        console.log(this.productsizes);
        for (const size of this.productsizes) {
          if (size[0].id == this.productsizeid ){
            this.highestBid = size.highest_bid;
            this.lowestask = size.lowest_ask;
            this.productsizeselected = size;
          }
        }
      });
    }



  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  adjustaskamount(value: string) {
    if (this.askamount === '0') {
      this.askamount = '' + value;
    } else {
      this.askamount = '' + this.askamount + value;
    }
    this.askamountnumber = Number(this.askamount);
    this.updateprice();
    this.askmessagecheck();
  }

  clearaskamount() {
    if (this.askamount.length > 0) {
      this.askamount = this.askamount.slice(0, -1);
    }
    if (this.askamount === ''){
      this.askamount = '0';
    }
    this.askamountnumber = Number(this.askamount);
    this.updateprice();
    this.askmessagecheck();
  }

  updateprice() {
    let amount: number;
    amount = this.askamountnumber;

    let lowestaskvalue = 0;
    let highestbidvalue = 0;
    if (this.lowestask) {
      lowestaskvalue = this.lowestask.lowest_ask_price;
    }

    if (this.highestBid) {
      highestbidvalue = this.highestBid.highest_bid_price;
    }


    if (amount > 0 && amount < highestbidvalue) {
      amount = highestbidvalue;
    } else {
      if (amount < 25) {
        amount = 0;
      }
    }


    let transfeeamount: number;
    transfeeamount = Number((+amount) * (this.transfee / 100));
    this.processFee = ((+amount) * 0.035);
    this.ordertotal = +amount - +this.processFee.toFixed(2) - +this.sellerfee - +transfeeamount;
    if (amount === 0) {
      this.ordertotal = 0;
    }
  }

  askmessagecheck() {
    let amount: number;
    amount = this.askamountnumber;

    let lowestaskvalue = 0;
    let highestbidvalue = 0;
    if (this.lowestask) {
      lowestaskvalue = this.lowestask.lowest_ask_price;
    }

    if (this.highestBid) {
      highestbidvalue = this.highestBid.highest_bid_price;
    }

    console.log(amount);

    this.askmessage = '';
    if (amount == 0) {} else if (amount < 25) {
      this.askmessage = 'The minimum price you can list at is  $25';
    } else if (( lowestaskvalue == 0 ) || (amount < lowestaskvalue && amount != 0)){
      if (amount <= highestbidvalue) {
        this.askmessage = 'You\'re about to sell at the highest offer $' + this.highestBid.highest_bid_price;
      } else {
        this.askmessage = 'You\'re about to list at the lowest price';
      }
    } else   if (amount > lowestaskvalue) {
      this.askmessage = 'You do not have the lowest asking price';
    }  else if (amount == lowestaskvalue) {
      this.askmessage = 'You\'re about to match the lowest price. Their listing will be accepted first';
    } else {}
  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  openselllistpopup(){
    this.closeModal({
      openmodal: 'SellPriceModalPage',
      data: {
        productid: this.productid,
        product: this.product
      }
    });
  }
 async loadlistpricesuccessmodal() {
  let amount: number;
  amount = this.askamountnumber;

  let lowestaskvalue = 0;
  let highestbidvalue = 0;
  if (this.lowestask) {
    lowestaskvalue = this.lowestask.lowest_ask_price;
  }

  if (this.highestBid) {
    highestbidvalue = this.highestBid.highest_bid_price;
  }

  
  if (amount == 0) {} else if (amount < 25) {
    this.askmessage = 'You\'re about to be the Lowest Ask';
  } else if (( lowestaskvalue == 0 ) || (amount < lowestaskvalue && amount != 0)){
    if (amount <= highestbidvalue) {
      this.reviewsellnowmodal(
        this.highestBid.highest_bid_price,
        this.productsizeid,
        this.highestBid.id,
        this.productsizeselected[0].value);
    } else {
      this.reviewlistmodal(
        amount,
        this.productsizeid,
        this.productsizeselected[0].value);
    }
  } else   if (amount > lowestaskvalue) {
    //this.askmessage = 'You\'re not the Lowest Ask';
    this.reviewlistmodal(
      amount,
      this.productsizeid,
      this.productsizeselected[0].value);
  }  else if (amount == lowestaskvalue) {
    // this.askmessage = 'You\'re about to match the lowest Ask. Their Ask will be accepted before yours';
    this.reviewlistmodal(
      amount,
      this.productsizeid,
      this.productsizeselected[0].value);
  } else {
  }

  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async reviewsellnowmodal(price: string, productSizeId: number, highestBidId: number, sizeValue: number ) {
  
    this.closeModal();
    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') !== null ) {
      const modal = await this.modalController.create({
        component: SellReviewModalPage,
        cssClass: 'sell-review-modal',
        componentProps: {
          productid: this.productid,
          product: this.product,
          bidPrice: price,
          productsizeid: productSizeId,
          highestbidid: highestBidId,
          size: sizeValue,
          offerid: this.id
        }
      });

      modal.onDidDismiss().then((response) => {
        if (response !== null) {
          if (response.data.openmodal == 'SellPriceModalPage'){
            this.loadmymodal(response.data.data,productSizeId);
          }
        }
      });

      return await modal.present();
    } else {
    //this.showLoader();
      let navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          bidPrice: price,
          productsizeid: productSizeId,
          highestbidid: highestBidId,
          size: sizeValue,
          modal: 'sellPriceModal',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
     }

  }

  

  async loadmymodal(data = {}, productsizeid) {

    const modal = await this.modalController.create({
      component: SellListPriceModalPage,
      cssClass: 'sell-list-price-modal',
      componentProps: {
        productid: this.productid,
        productSize: productsizeid,
        product: this.product,
        productsizes: this.productsizes,
        highestBid: this.highestBid,
        lowestAsk: this.lowestask,
        productsizeselected: this.productsizeselected
      }
    });
    return await modal.present();
  }
  async reviewlistmodal(Price: number, productSizeId: number, sizeValue: number ) {
    this.closeModal();
    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') !== null ) {
      const modal = await this.modalController.create({
        component: SellListReviewModalPage,
        cssClass: 'sell-list-review-modal',
        componentProps: {
          productid: this.productid,
          product: this.product,
          price: Price,
          productsizeid: productSizeId,
          size: sizeValue,
          id: this.id
        }
      });

      modal.onDidDismiss().then((response) => {
        if (response !== null) {
          if (response.data.openmodal == 'SellListPriceModalPage'){
            this.loadmymodal(response.data.data, productSizeId);
          }
        }
      });

      return await modal.present();
    } else {
    // //this.showLoader();
    //   let navigationExtras: NavigationExtras = {
    //     queryParams: {
    //       productid: this.productid,
    //       bidPrice: price,
    //       productsizeid: productSizeId,
    //       size: sizeValue,
    //       modal: 'sellPriceModal',
    //       redirectPath: 'productdetail/' + this.productid
    //     }
    //   };
    //   this.router.navigate(['login'], navigationExtras);
     }

  }
  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sell List Price Screen');
  }

}
