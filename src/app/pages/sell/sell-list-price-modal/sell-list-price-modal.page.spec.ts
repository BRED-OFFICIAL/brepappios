import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellListPriceModalPage } from './sell-list-price-modal.page';

describe('SellListPriceModalPage', () => {
  let component: SellListPriceModalPage;
  let fixture: ComponentFixture<SellListPriceModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellListPriceModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellListPriceModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
