import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams} from '@ionic/angular';
import { Router } from '@angular/router';
import { ProfileService} from '../../../services/profile.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-sell-success-modal',
  templateUrl: './sell-success-modal.page.html',
  styleUrls: ['./sell-success-modal.page.scss'],
})
export class SellSuccessModalPage implements OnInit {
  productid: number;
  order: any;
  amount: number;
  product: any;
  productsize: any;
  loaderToShow: any;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private router: Router,
    private profileService: ProfileService,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.order = this.navParams.data.order;
    this.productsize = this.navParams.data.productsize;
    this.profileService.loadselling(localStorage.getItem('userID'), '', '');
  }
  async closeModal() {
    const data = {closed: true};
    data.closed = true;
    await this.modalController.dismiss(data);

  }
  redirecttohome(){
    //this.showLoader();
     this.closeModal();

    //this.navCtrl.navigateForward(["tabs/home"]);
    
    //this.router.navigate(['tabs/home']);
    //window.location.reload();
    

  }
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        //clearTimeout(this.loadervisiblecheck);
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  redirectoorder(){
    this.closeModal();
    this.router.navigate(['/selling/order-detail-sell-view',this.order.id]);

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sale Success Screen');
  }

}
