import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellSuccessModalPage } from './sell-success-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SellSuccessModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellSuccessModalPage]
})
export class SellSuccessModalPageModule {}
