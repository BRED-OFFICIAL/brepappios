import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {ModalController,NavController, NavParams, LoadingController, AlertController,  Platform, ToastController} from '@ionic/angular';
import {ProductService} from '../../../services/product.service';
import {OrderService} from '../../../services/order.service';
import {ProfileService} from '../../../services/profile.service';
import {SellListSuccessModalPage} from '../sell-list-success-modal/sell-list-success-modal.page';
import {PaymentEditModalPage} from '../../payment-edit-modal/payment-edit-modal.page';
import {PayoutEditModalPage} from '../../payout-edit-modal/payout-edit-modal.page';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-sell-list-review-modal',
  templateUrl: './sell-list-review-modal.page.html',
  styleUrls: ['./sell-list-review-modal.page.scss'],
})
export class SellListReviewModalPage implements OnInit {
  productid: number;
  sellerid: number;
  bidId: number;
  prodSizeId: number;
  size: number;
  discountCoupon: number;
  discountCouponId: number;
  discountCouponType: number;
  
  credit: number;
  tokenId: any;
  product: any;
  sellerpaymentinformation: any;
  sellershippinginformation: any;
  sellerbillinginformation: any;
  subtotal: number;
  shippingamount: number;
  paymentprocamount: number;
  authenticationfee: number;
  transfee: number;
  transfeeamount: number;
  price: number;
  totalamount: number;
  sellerfee: number;
  loadervisiblecheck: any;
  termsaccepted: boolean;
  productsizeselected: any;
  loaderToShow: any;
  payoutinfo: any;

  isauthenticchecked: boolean;
  isshippedchecked: boolean;
  id: number;
  fingerPrintOptions: FingerprintOptions;

  constructor(
    private modalController: ModalController,
    private productService: ProductService,
    private orderService: OrderService,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private alertController: AlertController,
    private profileService: ProfileService,
    private toastController: ToastController,
    private fingerPrint: FingerprintAIO,
    private platform: Platform,
    private gaAnalyticsService: GoogleAnalyticsService 
      ) {
    this.fingerPrintOptions = {
      title:'Confirm Listing',
      // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      //clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      disableBackup:true,  //Only for Android(optional)
      // localizedFallbackTitle: 'Use Pin', //Only for iOS
      //  localizedReason: 'Please authenticate' //Only for iOS
    };

  }

  ngOnInit() {
    //console.log(this.navParams.data);
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.price = Number(this.navParams.data.price);
    this.shippingamount = Number(this.navParams.data.buyershippingfee);
    this.size = Number(this.navParams.data.size);
    //this.bidId = Number(this.navParams.data.highestbidid);
    this.paymentprocamount = 0;
    this.authenticationfee = 0;
    this.totalamount = 0;
    this.isauthenticchecked = false;
    this.isshippedchecked = false;
    this.prodSizeId = Number(this.navParams.data.productsizeid);

    if(this.navParams.data.id !== undefined){
      this.id = this.navParams.data.id;
    }

    


    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') != null ) {
      this.sellerid = Number(localStorage.getItem('userID'));
    }

    if (this.sellerid) {
      this.subtotal = 0.00;
      this.shippingamount = 0.00;
      this.paymentprocamount = 0.00;
      this.authenticationfee = 0.00;
      this.totalamount = 0.00;
    }
    this.presentLoader();
    this.productService.getsellerallinformation(this.sellerid)
    .subscribe( (buyeralldata: any) => {
      console.log('api called');
      this.sellerpaymentinformation = buyeralldata['payment'];
      this.sellershippinginformation = buyeralldata['shipping'];
      this.sellerbillinginformation = buyeralldata['billing'];
      this.sellerfee = buyeralldata['sellerfee'];
      this.transfee = Number(buyeralldata['trans_fee']);
      this.transfeeamount =  Number(this.price * (this.transfee / 100));
      this.shippingamount = Number(buyeralldata['sellerfee']);
      this.payoutinfo = buyeralldata['payoutinfo'];
      this.transfeeamount = Number(this.transfeeamount.toFixed(2));
      this.subtotal = this.price - this.shippingamount;
      this.paymentprocamount = (this.price * 0.035);
      this.totalamount = +this.subtotal + +this.totalamount;
      this.totalamount = +this.totalamount - +this.paymentprocamount.toFixed(2);
      this.totalamount = +this.totalamount - this.transfeeamount;
      // this.loadingController.dismiss();
      setTimeout( () => {
          this.loadingController.dismiss();
        }, 2000);
      
      // this.loadervisiblecheck = setTimeout( () => {
      //   //this.loadingController.dismiss();
      // }, 3000);
    });
  }

  async presentLoader() {
    const loading = await this.loadingController.create({
      message: ''
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }


  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  async listing() {
    try{
      await this.platform.ready();
      const available = await this.fingerPrint.isAvailable();
      this.fingerPrint.show(this.fingerPrintOptions)
      .then((result: any) => {
        if(result == 'biometric_success'){
          this.processListing();
        }
      })
      .catch((error: any) => {
        console.log(error);
        this.presentToast('Finger touch lock doesn\'t match.');
      });
    } catch(e) {
      this.processListing();
    }
    
  }
  
  processListing() {

    this.presentLoader();
    if (this.isauthenticchecked && this.isshippedchecked) {
      if(this.id){
        this.profileService.deleteOffer(this.id).subscribe( (response1: any) => {
          this.orderService.listing(
            this.productid,
            localStorage.getItem('userID'),
            this.prodSizeId,
            this.size,
            this.price,
            this.shippingamount,
            this.paymentprocamount.toFixed(2),
            this.transfeeamount.toFixed(2),
            this.totalamount.toFixed(2),
            null,
            null,
            null)
          .subscribe( (response: any) => {
    
            this.loadervisiblecheck = setTimeout( () => {
              this.loadingController.dismiss();
            }, 1000);
            if (response.status) {
                this.loadsuccessordermodal(response.listing);
            } else {
              this.alert('Something went wrong. Try again');
            }
          });
          
        });


      } else {
        this.orderService.listing(
          this.productid,
          localStorage.getItem('userID'),
          this.prodSizeId,
          this.size,
          this.price,
          this.shippingamount,
          this.paymentprocamount.toFixed(2),
          this.transfeeamount.toFixed(2),
          this.totalamount.toFixed(2),
          null,
          null,
          null)
        .subscribe( (response: any) => {
  
          this.loadervisiblecheck = setTimeout( () => {
            this.loadingController.dismiss();
          }, 1000);
          if (response.status) {
              this.loadsuccessordermodal(response.listing);
          } else {
            this.alert('Something went wrong. Try again');
          }
        });

      }


      
    }

  }

  opensellpricepopup() {
    this.closeModal({
      openmodal: 'SellListPriceModalPage',
      data: {
        productid: this.productid,
        product: this.product,
        price: this.price
      }
    });


  }

  authenticCheckboxTrigger(e: any) {
    this.isauthenticchecked = e.detail.checked;
  }

  shipCheckboxTrigger(e: any) {
    this.isshippedchecked = e.detail.checked;
  }
  Checkboxfocus(){

  }

  async alert(messageText: any) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: messageText,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();



  }

  async loadsuccessordermodal(listingInfo: any){

    this.closeModal();
    const modal = await this.modalController.create({
      component: SellListSuccessModalPage,
      cssClass: 'sell-list-success-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        productsize: this.size,
        price: this.price,
        listing: listingInfo
      }
    });
    return await modal.present();
  }

  async paymentedit() {

    this.closeModal();
    console.log(this.sellerbillinginformation);
    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.sellerpaymentinformation,
        billing: this.sellerbillinginformation,
        shipping: this.sellershippinginformation,
        paymentIdentifer: 'seller_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      console.log(response);
          

      if (response !== null) {
        if (response.data.openModal === 'SellListReview'){
          console.log(response.data);
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          this.openself();
        }
      } 

    });
    return await modal.present();
  }

  async openself() {

    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.price = Number(this.navParams.data.price);
    this.shippingamount = Number(this.navParams.data.buyershippingfee);
    this.size = Number(this.navParams.data.size);

    const modal = await this.modalController.create({
      component: SellListReviewModalPage,
       cssClass: 'sell-list-review-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        price: this.price,
        buyershippingfee: this.shippingamount,
        size: this.size,
      }
    });

    modal.onDidDismiss().then((response: any) => {
      // console.log(response.data.modal);
      if (response !== null) {
        if (response.data.openmodal === 'SellPriceModalPage'){
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          //this.sellPriceTab(response.data.data);
        }
      }
    });



    return await modal.present();


  }

  async payoutedit() {

    //this.closeModal();
    console.log(this.sellerbillinginformation);
    const modal = await this.modalController.create({
      component: PayoutEditModalPage,
      cssClass: 'payout-edit-modal',
      componentProps: {
        payoutInfo: this.payoutinfo,
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      console.log(response);
          

      if (response !== null) {
        console.log('Payout Modal Response ',response.data);
        if(response.data.haschanged !== undefined && response.data.haschanged){
          this.ngOnInit();
        }
      } 

    });
    return await modal.present();
  }

  doRefresh(event){
    this.productService.getsellerallinformation(this.sellerid)
    .subscribe( (buyeralldata: any) => {
      this.sellerpaymentinformation = buyeralldata['payment'];
      this.sellershippinginformation = buyeralldata['shipping'];
      this.sellerbillinginformation = buyeralldata['billing'];
      this.sellerfee = buyeralldata['sellerfee'];
      this.transfee = Number(buyeralldata['trans_fee']);
      this.transfeeamount =  Number(this.price * (this.transfee / 100));
      this.shippingamount = Number(buyeralldata['sellerfee']);
      this.payoutinfo = buyeralldata['payoutinfo'];
      this.transfeeamount = Number(this.transfeeamount.toFixed(2));
      this.subtotal = this.price - this.shippingamount;
      this.paymentprocamount = (this.price * 0.035);
      this.totalamount = +this.subtotal + +this.totalamount;
      this.totalamount = +this.totalamount - +this.paymentprocamount.toFixed(2);
      this.totalamount = +this.totalamount - this.transfeeamount;
      setTimeout(() => {
        console.log('Async operation has ended');
        event.target.complete();
      }, 1000);
      
      // this.loadervisiblecheck = setTimeout( () => {
      //   //this.loadingController.dismiss();
      // }, 3000);
    });
  }

  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sell List Review Screen');
  }


}
