import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellListReviewModalPage } from './sell-list-review-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SellListReviewModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellListReviewModalPage]
})
export class SellListReviewModalPageModule {}
