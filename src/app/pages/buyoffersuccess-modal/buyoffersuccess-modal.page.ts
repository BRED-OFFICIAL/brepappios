import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams,LoadingController} from '@ionic/angular';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'app-buyoffersuccess-modal',
  templateUrl: './buyoffersuccess-modal.page.html',
  styleUrls: ['./buyoffersuccess-modal.page.scss'],
})
export class BuyoffersuccessModalPage implements OnInit {
  productid: number;
  amount: number;
  product: any;
  loaderToShow: any;
  expireon: string;
  order: any;
  totalamount: number;
  productsizeselected: any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private loadingController: LoadingController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService 
    ) {


    }

  ngOnInit() {
    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.order = this.navParams.data.order;
    this.totalamount = this.navParams.data.totalamount;
    this.productsizeselected = this.navParams.data.productsizeselected;

    console.log(this.totalamount);
    console.log('On Success Page');
    console.log(this.order);


  }
  async closeModal() {
    const data = {closed: true};
    data.closed = true;
    await this.modalController.dismiss(data);

  }
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        //clearTimeout(this.loadervisiblecheck);
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  redirecttoOrder(){
    this.closeModal();
    this.router.navigate(['/buying'],{queryParams:{tab:'offer',id: this.order.confirm_bid}})
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Offer Success Screen');
  }

}
