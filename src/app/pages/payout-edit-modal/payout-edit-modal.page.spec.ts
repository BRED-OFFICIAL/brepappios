import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutEditModalPage } from './payout-edit-modal.page';

describe('PayoutEditModalPage', () => {
  let component: PayoutEditModalPage;
  let fixture: ComponentFixture<PayoutEditModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutEditModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutEditModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
