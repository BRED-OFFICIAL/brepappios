import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators,FormGroup} from '@angular/forms';
import {NavParams, LoadingController, ModalController} from '@ionic/angular';
import {ProfileService} from '../../services/profile.service';
import {EmailValidator, EmailMatch} from '../../validators/email';

@Component({
  selector: 'app-payout-edit-modal',
  templateUrl: './payout-edit-modal.page.html',
  styleUrls: ['./payout-edit-modal.page.scss'],
})
export class PayoutEditModalPage implements OnInit {

  model = {
    email: ''
  };
  editpayout: FormGroup;
  submitAttempt: boolean;
  payoutInfo: any;

  constructor(
    public formBuilder: FormBuilder,
    private navParams: NavParams,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private profileService: ProfileService,
    public emailValidator: EmailValidator,
    ) { 
      this.submitAttempt = false;

      this.editpayout = formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        confirmemail:['', [Validators.required, Validators.email]]
      },{
        validator: EmailMatch('email', 'confirmemail')
      });


    }

  ngOnInit() {
    this.payoutInfo = this.navParams.data.payoutInfo;
    this.editpayout.setValue({
      email: this.payoutInfo.payout,
      confirmemail: ''
    });
  }
  back(){
    this.closeModal({
      openModal: 'SellListReview',
      haschanged: false
    });

  }
  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  async presentLoader() {
    const loading = await this.loadingController.create({
      message: ''
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }
  save() {
    this.submitAttempt = true;
    const userID = localStorage.getItem('userID');
    if (this.editpayout.valid) {
      this.presentLoader();
      const paypalemail = this.editpayout.controls.email.value;
      this.profileService.updatePayout(paypalemail, userID).subscribe((response: any) => {
        if(response.status){
          this.loadingController.dismiss();
          this.closeModal({
            openModal: 'SellListReview',
            haschanged: true
          });

        }
      });
    }
  }


}
