import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PayoutEditModalPage } from './payout-edit-modal.page';

const routes: Routes = [
  {
    path: '',
    component: PayoutEditModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PayoutEditModalPage]
})
export class PayoutEditModalPageModule {}
