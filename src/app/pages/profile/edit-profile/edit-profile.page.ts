import { Component, OnInit } from '@angular/core';
import {AlertController, NavController, LoadingController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {UsernameValidator} from '../../../validators/username';
import {EmailValidator} from '../../../validators/email';
import {LoginService} from '../../../services/login.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  model = {
    username: '',
    email: '',
    firstname: '',
    lastname: '',
    phone: ''
  };
  submitAttempt: boolean;
  editprofile: FormGroup;
  loadervisiblecheck: any;
  loaderToShow: any;

  constructor(
    public formBuilder: FormBuilder,
    public usernameValidator: UsernameValidator,
    public emailValidator: EmailValidator,
    private loginService: LoginService,
    private router: Router,
    private alertController: AlertController,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private gaAnalyticsService: GoogleAnalyticsService 
    ) {
    this.submitAttempt = false;

    this.editprofile = formBuilder.group({
      username: ['', Validators.required, usernameValidator.checkusername.bind(usernameValidator)],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      emailId: ['', Validators.compose([Validators.required, Validators.email]), emailValidator.checkEmail.bind(emailValidator)],
      phone: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]+$')])],
  });



  }

  ngOnInit() {
    this.showLoader();
    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          redirectPath: 'edit-profile'
        }
      };
      this.router.navigate(['login'], navigationExtras);
    } else {
      this.loginService.getprofile(localStorage.getItem('userID')).subscribe( (response: any) => {
        this.editprofile.setValue({
          username: response.username,
          firstname: response.firstname,
          lastname: response.lastname,
          emailId: response.email,
          phone: response.phone
        }, {onlySelf: true});

        this.loadervisiblecheck = setInterval(() => {
          this.loadingController.dismiss();
        }, 1000);
      });
    }
  }
  save() {
    this.submitAttempt = true;
    const userID = localStorage.getItem('userID');
    if (this.editprofile.valid) {
     // this.showLoader();
      const data: any = {
        username: this.editprofile.controls.username.value,
        firstname: this.editprofile.controls.firstname.value,
        lastname: this.editprofile.controls.lastname.value,
        email: this.editprofile.controls.emailId.value,
        phone: this.editprofile.controls.phone.value
      };
      this.loginService.updateprofile(data, userID).subscribe((response: any) => {
        
        if (response.status) {
         // this.loadingController.dismiss();
          this.alertMessage();
        }


      });
      console.log(this.editprofile.controls.username.value);
    }


    console.log(this.editprofile.controls);
  }

  async alertMessage() {
    const alert = await this.alertController.create({
      header: 'Success',
      subHeader: '',
      message: 'Profile is updated successfully.',
      buttons: ['OK']
    });

    await alert.present();
  }

  back() {
    //this.navCtrl.navigateForward(['/settings']);
    this.navCtrl.pop();
  }

  ionViewDidEnter () {
    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      this.ngOnInit();
    }
    
  }
  showLoader() {
    if (!this.loaderToShow) {
      this.loaderToShow = this.loadingController.create({
        message: ''
      }).then((res) => {
        res.present();
  
        res.onDidDismiss().then((dis) => {
          clearInterval(this.loadervisiblecheck);
          console.log('Loading dismissed! after 2 Seconds');
        });
      });

    }
    
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Edit Profile Screen');
  }


}
