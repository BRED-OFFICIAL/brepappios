import { Component, OnInit } from '@angular/core';
import {LoadingController, ToastController, AlertController, ModalController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProfileService} from '../../../services/profile.service';
import {DeleteModalPage} from '../../../popups/delete-modal/delete-modal.page';
import {BuyingOfferDetailModalPage} from '../../../popups/buying-offer-detail-modal/buying-offer-detail-modal.page';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-buying',
  templateUrl: './buying.page.html',
  styleUrls: ['./buying.page.scss'],
})
export class BuyingPage implements OnInit {

  buyingdata: any;
  purchases: any;
  offers: any;
  loadervisiblecheck: any;
  loaderToShow: any;
  isPurchaseTabOpen: boolean;
  isOfferTabOpen: boolean;
  searchpurchasevalue: string;
  searchoffervalue: string;
  searchvalue: string;
  iscalling: any;
  constructor(
    private profileService: ProfileService,
    private loadingController: LoadingController,
    private router: Router,
    private toastController: ToastController,
    private alertController: AlertController,
    private modalController: ModalController,
    private route: ActivatedRoute,
    private gaAnalyticsService: GoogleAnalyticsService
    ) { }

  ngOnInit() {
    this.isPurchaseTabOpen = true;
    this.isOfferTabOpen = false;
    this.searchpurchasevalue = '';
    this.searchoffervalue = '';

    this.route.queryParams.subscribe((params)=>{
      const tab = params.tab;
      // const id = params.id;
      console.log(tab);
      if(tab === 'offer'){
        this.showOfferSection();
      }
    });


    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          redirectPath: 'buying'
        }
      };
      this.router.navigate(['login'], navigationExtras);
    } else {
      this.loaddata();
      this.updatedata();
    }
  }

  updatedata(){
    this.profileService.buyingdata.subscribe((response:any)=>{
      if(response.data != undefined){
        this.buyingdata = response.data;
        this.purchases = response.data.purchases;
        this.offers = response.data.offers;
      }
    });
  }

  showLoader() {
    if (!this.loaderToShow) {
      this.loaderToShow = this.loadingController.create({
        message: ''
      }).then((res) => {
        res.present();
        res.onDidDismiss().then((dis) => {
          clearInterval(this.loadervisiblecheck);
          console.log('Loading dismissed! after 2 Seconds');
        });
      });
    }
  }
  showPurchasedSection() {
    this.isPurchaseTabOpen = true;
    this.isOfferTabOpen = false;
    this.searchvalue = this.searchpurchasevalue;
    this.gaAnalyticsService.setTrackScreen('Buying Screen: Purchased');
  }
  showOfferSection() {
    this.isPurchaseTabOpen = false;
    this.isOfferTabOpen = true;
    this.searchvalue = this.searchoffervalue;
    this.gaAnalyticsService.setTrackScreen('Buying Screen: Offer');
  }
  async delete(item: any) {
    const modal = await this.modalController.create({
      component: DeleteModalPage,
      cssClass: 'delete-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        title: 'Delete Offer',
        text: 'Are you sure you want to delete your offer?',
        actionbutton: 'Delete Offer'
      }
    });

    modal.onDidDismiss().then((response1: any) => {
      if(response1.data.delete != undefined && response1.data.delete){
        this.profileService.deleteOffer(item.id).subscribe( (response: any) => {
          if (response.status) {
            const index = this.offers.indexOf(item);
            this.offers.splice(index, 1);
            this.presentToast('Your offer has been deleted successfully.');
          }
        });

      }


    });
    return await modal.present();



    // const alert = await this.alertController.create({
    //   header: 'Confirm!',
    //   message: 'Are you want to delete this offer?',
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       cssClass: 'secondary',
    //       handler: (blah) => {
    //         console.log('Confirm Cancel: blah');
    //       }
    //     }, {
    //       text: 'Okay',
    //       handler: () => {
    //         this.profileService.deleteOffer(item.id).subscribe( (response: any) => {
    //           if (response.status) {
    //             const index = this.offers.indexOf(item);
    //             this.offers.splice(index, 1);
    //             this.presentToast('Your offer has been deleted successfully.');
    //           }
    //         });
    //       }
    //     }
    //   ]
    // });

    // await alert.present();



  }

  async presentToast(m: any) {
    const toast = await this.toastController.create({
      message: m,
      duration: 2000
    });
    toast.present();
  }

  loaddata() {

    this.showLoader();
    this.profileService.buying(localStorage.getItem('userID'), this.searchpurchasevalue, this.searchoffervalue)
    .subscribe( (response: any) => {
      console.log(response.data);
      this.buyingdata = response.data;
      this.purchases = response.data.purchases;
      this.offers = response.data.offers;
      this.loadervisiblecheck = setInterval(() => {
         this.loadingController.dismiss();
       }, 1000);
     });

  }

  search() {
    console.log(this.searchvalue);
    if (this.isPurchaseTabOpen) {
      this.searchpurchasevalue = this.searchvalue;
    }
    if (this.isOfferTabOpen){
      this.searchoffervalue = this.searchvalue;
    }
    clearTimeout(this.iscalling);

    if (this.searchvalue !== undefined) {
      this.iscalling = setTimeout(() => {
        this.loaddata();
      }, 1000);
    }


  }

  refresh(event: any){
    this.profileService.buying(localStorage.getItem('userID'), this.searchpurchasevalue, this.searchoffervalue)
    .subscribe( (response: any) => {
      console.log(response.data);
      this.buyingdata = response.data;
      this.purchases = response.data.purchases;
      this.offers = response.data.offers;
      this.loadervisiblecheck = setInterval(() => {
        event.target.complete();
       }, 1000);
     });

  }

  async openoption(data: any){
    const modal = await this.modalController.create({
      component: BuyingOfferDetailModalPage,
      cssClass: 'buying-offer-detail-modal',
     
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        d:data,
        obj: this
      }
    });

    return await modal.present();

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Buying Screen');
  }


}
