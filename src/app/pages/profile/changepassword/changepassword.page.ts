import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {FormBuilder,Validators, FormGroup} from '@angular/forms';
import {ProfileService} from '../../../services/profile.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {
  changePasswordForm: FormGroup;
  submitAttempt: false;
  model = {
    oldpassword: '',
    newpassword: '',
    confirmpassword: '',
  };


  constructor(
    public formBuilder: FormBuilder,
    private profileService: ProfileService,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private gaAnalyticsService: GoogleAnalyticsService

  ) {
    this.changePasswordForm = formBuilder.group({
      oldpassword: ['', Validators.required],
      newpassword: ['', Validators.required],
      confirmpassword: ['', Validators.required]
    });


  }

  ngOnInit() {
  }

  save() {
    const userID = localStorage.getItem('userID');
    const userEmail = localStorage.getItem('userEmail');
    if (this.changePasswordForm.valid) {
      const data: any = {
        oldpassword: this.changePasswordForm.controls.oldpassword.value,
        newpassword: this.changePasswordForm.controls.newpassword.value,
        confirmpassword: this.changePasswordForm.controls.confirmpassword.value,
      };

      this.presentLoader();
      this.profileService.changePassword(data, userID, userEmail).subscribe((response: any) => {
        this.loadingController.dismiss();
        this.presentToast(response.message);
        this.changePasswordForm.reset()

      });



    }

  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loadertoshow = await this.loadingController.create({
      message: ''
    });
    await loadertoshow.present();
  
    const { role, data } = await loadertoshow.onDidDismiss();
  
    console.log('Loading dismissed!');
  }
  async presentToast(m: any) {
    const toast = await this.toastController.create({
      message: m,
      duration: 2000
    });
    toast.present();
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Change Password Screen');
  }

}
