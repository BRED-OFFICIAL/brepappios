import { Component, OnInit } from '@angular/core';
import {LoadingController, ModalController} from '@ionic/angular';
import {ProductService} from '../../../services/product.service';
import {PaymentEditModalPage} from '../../payment-edit-modal/payment-edit-modal.page';
import {PayoutEditModalPage} from '../../payout-edit-modal/payout-edit-modal.page';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-payment-settings',
  templateUrl: './payment-settings.page.html',
  styleUrls: ['./payment-settings.page.scss'],
})
export class PaymentSettingsPage implements OnInit {

  buyinginformation: any;
  sellinginformation: any;

  constructor(
    private loadingController: LoadingController,
    private productService: ProductService,
    private modalController: ModalController,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.presentLoader();
    this.productService.getuserallinformation(localStorage.getItem('userID'))
        .subscribe( (response: any) => {
          console.log(response);
          this.loadingController.dismiss();
          if (response.buying){
            this.buyinginformation = response.buying;
          }
          if (response.selling){
            this.sellinginformation = response.selling;
          }
          this.loadingController.dismiss();
        });
  }
  async buyingedit(){
    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.buyinginformation.payment,
        billing: this.buyinginformation.billing,
        shipping: this.buyinginformation.shipping,
        paymentIdentifer: 'buyer_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      console.log(response);
      this.ngOnInit();
    });
    return await modal.present();

  }
  async selleredit(){

    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.sellinginformation.payment,
        billing: this.sellinginformation.billing,
        shipping: this.sellinginformation.shipping,
        paymentIdentifer: 'seller_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      console.log('on sell list review modal');
      this.ngOnInit();
    });
    return await modal.present();
    
  }

  async presentLoader() {
    const loading = await this.loadingController.create({
      message: ''
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  doRefresh(event: any){
    this.productService.getuserallinformation(localStorage.getItem('userID'))
        .subscribe( (response: any) => {
          if(event){
            event.target.complete();
          }
          if (response.buying){
            this.buyinginformation = response.buying;
          }
          if (response.selling){
            this.sellinginformation = response.selling;
          }
          this.loadingController.dismiss();
        });
  }

  async payoutedit(){

    const modal = await this.modalController.create({
      component: PayoutEditModalPage,
      cssClass: 'payout-edit-modal',
      componentProps: {
        payoutInfo: this.sellinginformation.payoutinfo,
      }
    });
    modal.onDidDismiss().then((response: any) => {
      this.ngOnInit();
    });
    return await modal.present();
  }
  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Payment Settings Screen');
  }

}
