import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AlertController, LoadingController} from '@ionic/angular';
import {OrderService} from '../../../services/order.service';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {FileTransfer,FileTransferObject,FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {File} from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

@Component({
  selector: 'app-shippinglabel',
  templateUrl: './shippinglabel.page.html',
  styleUrls: ['./shippinglabel.page.scss'],
})
export class ShippinglabelPage implements OnInit {
  orderId: any;
  file: string;

  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private socialSharing: SocialSharing,
    private base64: Base64,
    private fileTransfer: FileTransfer,
    private fileM: File
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.orderId = params.get('orderId');
      if (this.orderId) {
        this.orderService.orderShippingLabel(this.orderId, +localStorage.getItem('userID')).subscribe((response: any) => {
          if (response.status === true) {
            this.file = response.data.file;
          }else{
            this.presentAlert(response.message);

          }
          



        });

      }
    });


  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }
  async presentAlert(text = '') {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: text,
      buttons: ['OK']
    });

    await alert.present();
  }

  async share() {
    console.log(this.fileM.cacheDirectory);

    const fileTransfer: FileTransferObject = this.fileTransfer.create();
    fileTransfer.download(this.file, this.fileM.cacheDirectory + `${ new Date().getTime()  }.jpg`).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      console.log(entry.nativeURL);
      this.socialSharing.share('This is test','test subject',  entry.nativeURL, this.file).then(() => {
        console.log('success');

      }).catch(() => {

      });

    }, (error) => {
      // handle error
    });

    // this.base64.encodeFile(this.file).then((base64File: string) => {
    //   console.log(base64File);
    // }, (err) => {
    //   console.log(err);
    // });

    // this.socialSharing.share('This is test','test subject', this.file,null).then(() => {
    //   console.log('success');

    // }).catch(() => {

    // });
  }

  async download(){


  }

}
