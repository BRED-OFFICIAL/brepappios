import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippinglabelPage } from './shippinglabel.page';

describe('ShippinglabelPage', () => {
  let component: ShippinglabelPage;
  let fixture: ComponentFixture<ShippinglabelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippinglabelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippinglabelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
