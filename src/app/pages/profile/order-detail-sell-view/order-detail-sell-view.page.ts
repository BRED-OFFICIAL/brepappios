import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {OrderService} from '../../../services/order.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-order-detail-sell-view',
  templateUrl: './order-detail-sell-view.page.html',
  styleUrls: ['./order-detail-sell-view.page.scss'],
})
export class OrderDetailSellViewPage implements OnInit {
  segment: string;
  isSaleStatusOpen: boolean;
  isSaleDetailOpen: boolean;
  title: string;
  product: any;
  order: any;
  // product = {
  //   title: '',
  //   sku: '',
  //   image_large: ''
  // };
  // order = {
  //   value: '',
  //   total: 0,
  //   paymentdate: '',
  //   paymenttime: '',
  //   status: '',
  //   activity: [],
  //   order_id: '',
  //   seller_information: {
  //     address: []
  //   },
  //   transaction_fee: 0,
  //   transaction_fee_amount: 0,
  //   proc_amount: 0,
  //   seller_shipping_fee: 0,
  //   seller_total: 0,
    
  // };
  orderId: number;
  sellerId: number;
  trackingInformation = {
    occurrence:[]
  };
  loaderToShow: any;
  sellingPayment: any;
  //buyerAddress: [];

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute,
    private loadingController: LoadingController,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    this.presentLoader();
    this.activeSaleStatusTab();
    this.route.paramMap.subscribe(params => {
      this.orderId = +params.get('orderId');
      this.sellerId = +localStorage.getItem('userID');
      this.getDetail().then(()=>{
          setTimeout(()=>{
            this.loadingController.dismiss();
          },2000);
      });
      
    });

  }
  refresh($event){
    this.getDetail().then(()=>{
      setTimeout(()=>{
        $event.target.complete();
      },1000);
  });

  }
  async getDetail(){
   await this.orderService.detail(this.orderId, null, this.sellerId).subscribe( (response: any) => {
      if (response.status === true) {
        this.product = response.data.product;
        this.order = response.data.order;
        this.sellingPayment = response.data.seller_payment;
        //this.buyerAddress =  this.order.seller_information.address;


        this.order.total = +Number(this.order.total).toFixed(2);
        this.order.transaction_fee_amount = +Number(this.order.transaction_fee_amount).toFixed(2);
        this.order.seller_shipping_fee = +Number(this.order.seller_shipping_fee).toFixed(2);
        this.order.seller_total = +Number(this.order.seller_total).toFixed(2);
        this.trackingInformation = response.data.tracking_information;
      }
      // setTimeout(()=>{
      //   this.loadingController.dismiss();
      // },2000);
    });

  }
  activeSaleStatusTab() {
    this.isSaleStatusOpen = true;
    this.title = 'Sale Status';
    this.isSaleDetailOpen = false;
    this.segment = 'salestatus';
    this.gaAnalyticsService.setTrackScreen('Sale Detail Screen: Sale Status');
  }
  activeSaleDetailTab() {
    this.isSaleStatusOpen = false;
    this.title = 'Sale Details';
    this.isSaleDetailOpen = true;
    this.segment = 'saledetail';
    this.gaAnalyticsService.setTrackScreen('Sale Detail Screen: Sale Detail');
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Sale Detail Screen');
  }

}
