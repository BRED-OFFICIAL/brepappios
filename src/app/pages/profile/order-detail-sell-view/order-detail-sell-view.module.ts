import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailSellViewPage } from './order-detail-sell-view.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailSellViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailSellViewPage]
})
export class OrderDetailSellViewPageModule {}
