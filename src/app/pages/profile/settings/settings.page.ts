import { Component, OnInit } from '@angular/core';
import {AlertController,LoadingController, NavController, ModalController, Platform} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {ShippingAddressModalPage} from '../../../popups/shipping-address-modal/shipping-address-modal.page';
import { ThemeableBrowser, ThemeableBrowserObject, ThemeableBrowserOptions } from '@ionic-native/themeable-browser/ngx';
import {ThemeableBrowserOptionConfig} from '../../../../config/ThemeableBrowserOptions';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  subscription: any;

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private themeableBrowser: ThemeableBrowser,
    private navCtrl: NavController,
    private platform: Platform,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          redirectPath: 'settings'
        },
        // skipLocationChange: true,
      };
       this.router.navigate(['login'], navigationExtras);
      //  this.navCtrl.navigateForward(['login'], navigationExtras);
    }
  }

  


  async presentLoader() {
    console.log('prsent laoder at product page');
    const loadertoshow = await this.loadingController.create({
      message: ''
    });
    await loadertoshow.present();
    const { role, data } = await loadertoshow.onDidDismiss();
    console.log('Loading dismissed!');
  }
  
  // ionViewWillEnter() {
  //   this.presentLoader();
  //   if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
  //     //this.router.navigate(['tabs/home']);
  //   }
  //   setTimeout(() => {
  //     console.log('dismiss after time out');
  //     this.loadingController.dismiss();
  //   }, 1000);

  // }

  async loadShippingAddressModal() {
    const modal = await this.modalController.create({
      component: ShippingAddressModalPage,
      cssClass: 'shipping-address-modal',
      
    });
    modal.onDidDismiss().then((response: any) => {
     
    });
    return await modal.present();

  }

  logout(){
    localStorage.removeItem('userEmail');
    localStorage.removeItem('userID');
    this.navCtrl.navigateRoot(['/landing']);
  }

  openbrowser(url:string,title: string) {
    this.gaAnalyticsService.setTrackScreen(title+ ' Screen');
    const options: ThemeableBrowserOptions = ThemeableBrowserOptionConfig;
    const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
   
    
  }
  // ionViewWillEnter(){
  //   //this.presentLoader();
  //   if(+localStorage.getItem('userID')){
  //     //this.loadingController.dismiss();
      
  //   }else{
  //     this.navCtrl.pop();
  //   }
  // }

    // ionViewDidEnter(){
    //   this.subscription = this.platform.backButton.subscribe(() => {
    //     console.log( this.router.url);
    //     this.navCtrl.back();
  
    //     if (this.router.isActive('/tabs/browse', true) && this.router.url === '/tabs/browse') {
    //       // this.router.navigate(['/tabs/home']);
    //       this.navCtrl.back();
    //     }
    //     else if(this.router.isActive('/tabs/profile', true) && this.router.url === '/tabs/profile'){
    //       this.navCtrl.back();
    //     }
    //     else{
    //       if(+localStorage.getItem('userID')){
    //         navigator['app'].exitApp();
    //       }else{
    //         this.navCtrl.back();
    //       }
          
    //     }
    //   });
    // }
  
    // ionViewWillLeave(){
    //   this.subscription.unsubscribe();
    // }

    ionViewWillEnter(){
      this.gaAnalyticsService.setTrackScreen('Settings Screen');
    }





}
