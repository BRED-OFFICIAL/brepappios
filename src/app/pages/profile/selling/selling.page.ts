import { Component, OnInit } from '@angular/core';
import {LoadingController, ToastController, AlertController, ModalController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProfileService} from '../../../services/profile.service';
import {SellingListingDetailModalPage} from '../../../popups/selling-listing-detail-modal/selling-listing-detail-modal.page';
import {DeleteModalPage} from '../../../popups/delete-modal/delete-modal.page';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-selling',
  templateUrl: './selling.page.html',
  styleUrls: ['./selling.page.scss'],
})
export class SellingPage implements OnInit {
  sellingdata: any;
  solditems: any;
  listings: any;
  loadervisiblecheck: any;
  loaderToShow: any;
  isSoldTabOpen: boolean;
  isListingTabOpen: boolean;
  searchpurchasevalue: string;
  searchoffervalue: string;
  searchvalue: string;
  iscalling: any;
  isdisabletoggleshow: boolean;
  disablelisting: boolean;

  constructor(
    private profileService: ProfileService,
    private loadingController: LoadingController,
    private router: Router,
    private toastController: ToastController,
    private alertController: AlertController,
    private modalController: ModalController,
    private route: ActivatedRoute,
    private gaAnalyticsService: GoogleAnalyticsService 
  ) {

    
   }

  ngOnInit() {
    this.isSoldTabOpen = true;
    this.isListingTabOpen = false;
    this.searchpurchasevalue = '';
    this.searchoffervalue = '';
    this.disablelisting = false;

    

    
    this.route.queryParams.subscribe((params)=>{
      const tab = params.tab;
      // const id = params.id;
      console.log(tab);
      if(tab === 'listing'){
        this.showListingSection();
      }
    });


    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          redirectPath: 'buying'
        }
      };
      this.router.navigate(['login'], navigationExtras);
    } else {
      this.loaddata();
      this.updatedata();
    }
  }

  updatedata(){
    this.profileService.sellingdata.subscribe((response:any)=>{
      if(response.data != undefined){
        this.sellingdata = response.data;
        this.solditems = response.data.solditems;
        this.listings = response.data.listings;
      }
    });
  }

  showLoader() {
    if (!this.loaderToShow) {
      this.loaderToShow = this.loadingController.create({
        message: ''
      }).then((res) => {
        res.present();
        res.onDidDismiss().then((dis) => {
          clearInterval(this.loadervisiblecheck);
          console.log('Loading dismissed! after 2 Seconds');
        });
      });
    }
  }
  showSoldSection() {
    this.isSoldTabOpen = true;
    this.isListingTabOpen = false;
    this.searchvalue = this.searchpurchasevalue;
    this.gaAnalyticsService.setTrackScreen('Selling Screen: Sold Tab');
  }
  showListingSection() {
    this.isSoldTabOpen = false;
    this.isListingTabOpen = true;
    this.searchvalue = this.searchoffervalue;
    this.gaAnalyticsService.setTrackScreen('Selling Screen: Listings Tab');
  }
  async delete(item: any) {
    const modal = await this.modalController.create({
      component: DeleteModalPage,
      cssClass: 'delete-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        title: 'Delete Listing',
        text: 'Are you sure you want to delete your listing?',
        actionbutton: 'Delete Listing'
      }
    });
    modal.onDidDismiss().then((response1: any) => {
      if(response1.data.delete != undefined && response1.data.delete){
        this.profileService.deleteOffer(item.id).subscribe( (response: any) => {
          if (response.status) {
            const index = this.listings.indexOf(item);
            this.listings.splice(index, 1);
            this.presentToast('Your listing has been deleted successfully.');
          }
        });
      }
    });
    return await modal.present();
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async presentToast(m: any) {
    const toast = await this.toastController.create({
      message: m,
      duration: 2000
    });
    toast.present();
  }

  loaddata() {

    this.presentLoader();
    this.profileService.selling(localStorage.getItem('userID'), this.searchpurchasevalue, this.searchoffervalue)
    .subscribe( (response: any) => {
      console.log(response.data);
      this.sellingdata = response.data;
      this.solditems = response.data.solditems;
      this.listings = response.data.listings;
      this.loadingController.dismiss();
    }, error => {
      console.error(error);
      this.loadingController.dismiss();
      if (error.status === 0) {
        this.presentToast('Couldn\'t able to connect server/ ');
      } else {
        this.presentToast(error.statusText);
      }
    });

  }

  search() {
    console.log(this.searchvalue);
    if (this.isSoldTabOpen) {
      this.searchpurchasevalue = this.searchvalue;
    }
    if (this.isListingTabOpen){
      this.searchoffervalue = this.searchvalue;
    }
    clearTimeout(this.iscalling);

    if (this.searchvalue !== undefined && this.searchvalue !== '') {
      this.iscalling = setTimeout(() => {
        this.loaddata();
      }, 1000);
    }


  }
  doRefresh(event: any) {
    event.target.complete();
    this.loaddata();

  }
  showListingWithDisableSection(){
    this.isdisabletoggleshow = true;
    this.showListingSection();

  }
  hideListingWithDisableSection(){
    this.isdisabletoggleshow = false;
    this.showListingSection();
  }

  async openoption(data: any) {
    console.log(data);
    const modal = await this.modalController.create({
      component: SellingListingDetailModalPage,
      cssClass: 'selling-listing-detail-modal',
     
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        d: data,
        obj: this
      }
    });

    return await modal.present();

  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Selling Screen');
  }



}
