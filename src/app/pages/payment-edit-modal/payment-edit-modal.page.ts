import { Component, OnInit, NgZone, HostBinding } from '@angular/core';
import {AlertController, NavController, NavParams, LoadingController, ModalController, AngularDelegate} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {PaymentService} from '../../services/payment.service';
import {ProfileService} from '../../services/profile.service';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

// import { AngularFrameworkDelegate } from '@ionic/angular/dist/providers/angular-delegate';
import { ProductService } from 'src/app/services/product.service';
declare var jQuery: any;
declare var braintree: any;
declare var window: any;
@Component({
  selector: 'app-payment-edit-modal',
  templateUrl: './payment-edit-modal.page.html',
  styleUrls: ['./payment-edit-modal.page.scss']})
export class PaymentEditModalPage implements OnInit {

  model = {
    billingname: '',
    billingaddres1: '',
   // billingaddres2: '',
    billingcountry: '',
    billingregion: '',
    billingcity: '',
    billingpostalcode: '',
    billingphone: '',
    shiptobilling: '',
  };
  submitAttempt: boolean;
  editpayment: FormGroup;
  loadervisiblecheck: any;
  loaderToShow: any;
  paymentInformation: any;
  billingInformation: any;
  shippingInformation: any;
  paypal: boolean;
  paymentresponse: any;
  paymentIdentifier: any;
  billingId: number;
  shippingId: number;
  shipLabel: string;


  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private alertController: AlertController,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private paymentService: PaymentService,
    private ngZone: NgZone,
    private profileService: ProfileService,
    private modalController: ModalController,
    private gaAnalyticsService: GoogleAnalyticsService
  ) {

    this.submitAttempt = false;

    this.editpayment = formBuilder.group({
      billingname: ['', Validators.required],
      billingaddres1: ['', Validators.required],
    //  billingaddres2: [''],
      billingcountry: ['', Validators.required],
      billingregion: ['', Validators.required],
      billingcity: ['', Validators.required],
      billingpostalcode: ['', [Validators.required]],
      billingphone: ['', Validators.required],
      shiptobilling: [{disabled: true}, Validators.required],
    });



   }

  ngOnInit() {
    this.paymentInformation = this.navParams.data.payment;
    this.billingInformation = this.navParams.data.billing;
    this.shippingInformation = this.navParams.data.shipping;
    this.paypal = this.navParams.data.paypal;
    this.paymentIdentifier = this.navParams.data.paymentIdentifer;
    console.log(this.navParams.data);
    if(this.paymentIdentifier === 'seller_payment') {
      this.shipLabel = 'shipping';
    }

    if(this.paymentIdentifier === 'buyer_payment') {
      this.shipLabel = 'buying';
    }

    this.presentLoader();

    if (this.billingInformation) {
      if(this.billingInformation.id && this.billingInformation.id !== undefined){
        this.billingId = this.billingInformation.id;
      }
      this.editpayment.setValue({
        billingname: this.billingInformation.firstname + ' ' + this.billingInformation.lastname,
        billingaddres1: this.billingInformation.address,
      //  billingaddres2: '',
        billingcountry: this.billingInformation.country,
        billingregion: this.billingInformation.state,
        billingcity: this.billingInformation.city,
        billingpostalcode: this.billingInformation.zipcode,
        billingphone: this.billingInformation.phone,
        shiptobilling: true
      }, {onlySelf: true});
    }else{
      this.billingId = 0;
    }

    this.editpayment.get('shiptobilling').disable();

    if (this.shippingInformation) {
      if(this.shippingInformation.id && this.shippingInformation.id !== undefined){
        this.shippingId = this.shippingInformation.id;
      }
    }else{
      this.shippingId = 0;
    }

    window ['angularComponentReference'] = {
      component: this,
      zone: this.ngZone,
      loadAngularFunction: (data: any) => this.setPaymentResponse(data),
    };

    window ['angularHideLoader'] = {
      component: this,
      zone: this.ngZone,
      loadAngularFunction: () => this.loadingController.dismiss(),
    };

    window ['angularShowLoader'] = {
      component: this,
      zone: this.ngZone,
      loadAngularFunction: () => this.showLoader(),
    };

    window ['angularPaymentError'] = {
      component: this,
      zone: this.ngZone,
      loadAngularFunction: (message: string) => this.PaymentError(message),
    };

    window ['paymentIdentifier'] = this.paymentIdentifier;




    this.paymentService.getToken()
      .subscribe( (response: any) => {
        console.log(response);

        (function($) {
          $(document).ready(function() {
                var button = document.querySelector('#paymentsavebutton');
                braintree.dropin.create({
                  authorization: response.authorization_token,
                  container: '#dropin-container',
                  dataCollector: {
                    // kount: true, // Required if Kount fraud data collection is enabled
                    paypal: true// Required if PayPal fraud data collection is enabled
                  },
                  card: {
                    cardholderName: true,
                    overrides:{
                      fields:{
                        number:{
                          supportedCardBrands: {
                            visa: true, // prevents Visas from showing up as valid even when the Braintree control panel is configured to allow them
                            mastercard: true,
                            discover: false,
                            jcb: false,
                            'union-pay': false,
                            'diners-club': false, // allow Diners Club cards to be valid (processed as Discover cards on the Braintree backend)
                            'american-express':false
                          }
                        },
                        cvv: {
                          maskInput: true
                        }

                      }
                      
                    }
                  },
                  paypal: {
                    flow: 'vault',
                    buttonStyle: {
                      color: 'blue',
                      shape: 'rect',
                      size: 'medium'
                    }
                  }
                }, function (err, instance) {
                  console.log(instance);

                  instance.on('cardTypeChange', function (event) {
                    if (event.cards.length === 1) {
                      console.log(event.cards[0].type);
                    } else {
                      console.log('Type of card not yet known');
                    }
                  });


                      $(".braintree-sheet__card-icon[data-braintree-id=\"american-express-card-icon\"], .braintree-sheet__card-icon[data-braintree-id=\"jcb-card-icon\"] , .braintree-sheet__card-icon[data-braintree-id=\"discover-card-icon\"]").hide();
                      if(window.paymentIdentifier == 'buyer_payment'){
                        $('.braintree-option.braintree-option__paypal').show();
                      }else{
                        $('.braintree-option.braintree-option__paypal').hide();
                      }
                     
                      window.angularHideLoader.zone.run(() => {
                    window.angularHideLoader.loadAngularFunction(); });

                    instance.on('paymentMethodRequestable', function (event) {
                      /* NOTE: This instance call for to hide previous payment info section. \
                      This is call when user chosse paypal account
                      because paypal account event doesn't comes inside instance.requestPaymentMethod().
                      */
                    
                      if(event.paymentMethodIsSelected && event.type == 'PayPalAccount'){
                        $('.showCard').hide();
                      }
                      //console.log(event.type); // The type of Payment Method, e.g 'CreditCard', 'PayPalAccount'.
                      //console.log(event.paymentMethodIsSelected); // True if a customer has selected a payment method when paymentMethodRequestable fires.
                    
                      // submitButton.removeAttribute('disabled');
                    });
                    instance.on('noPaymentMethodRequestable', function () {
                     // $('.showCard').show();
                    });
                  button.addEventListener('click', function () {
                    instance.requestPaymentMethod(function (err, payload: any) {
                      console.log(err);
                      console.log('payload', payload);
                    


                      // Submit payload.nonce to your server
                      if(payload !== undefined){
                        window.angularShowLoader.zone.run(() => {
                          window.angularShowLoader.loadAngularFunction(); });


                        var deviceDataPayout = payload.deviceData;
                        // delete deviceData
                        console.log(payload);
                        delete payload.deviceData;
                        if(payload.type == 'CreditCard'){
                          if(
                             (payload.binData.prepaid == 'No' && payload.binData.debit == 'No')
                            || (payload.binData.prepaid == 'Unknown' && payload.binData.debit == 'Unknown') 
                             ) {
                              // console.log(JSON.stringify(payload));
                            // this.paymentresponse = payload;
                            window.angularComponentReference.zone.run(() => {
                              window.angularComponentReference.loadAngularFunction(payload); });

                              
                          }else{
                            $(".braintree-method.braintree-method--active").hide();
                            window.angularComponentReference.zone.run(() => {
                              window.angularPaymentError.loadAngularFunction('Debit cards not accepted.'); });
                              //should show debit card not accepted message
                          }
                        }else{

                          //paypal conditions




                          window.angularComponentReference.zone.run(() => {
                            window.angularComponentReference.loadAngularFunction(payload); });
                        }
                      }else {
                        console.log('else conditon');
                        if(!$('.braintree-dropin').hasClass('braintree-sheet--has-error')){
                          window.angularShowLoader.zone.run(() => {
                            window.angularShowLoader.loadAngularFunction(); }); 

                          window.angularComponentReference.zone.run(() => {
                            window.angularComponentReference.loadAngularFunction({}); });
                        }else{
                          // console.log('sdfsdfjsldkfjsd');
                          // window.angularHideLoader.zone.run(() => {
                          //   window.angularHideLoader.loadAngularFunction(); });

                        }


                      }

                    });
                  })
                });
              });
            })(jQuery);

      });








  }
  back(){
    this.closeModal({
      openModal: 'SellListReview'
    });

  }

  PaymentError(message = ''){
    setTimeout(() => {
      this.hideLoader();
    this.presentAlert(message);


    },3000);


  }

  async presentAlert(msg = '') {
    const alert = await this.alertController.create({
      header: 'Error',
      subHeader: '',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  save() {
    const userID = localStorage.getItem('userID');
    if(this.paymentresponse){
      let ispaymentresponseokay = false;
      if(this.paymentresponse.type == 'CreditCard'){
        if((this.paymentresponse.binData.prepaid == 'No' && this.paymentresponse.binData.debit == 'No') ||
        (this.paymentresponse.binData.prepaid == 'Unknown' && this.paymentresponse.binData.debit == 'Unknown') ){
          //save data
          ispaymentresponseokay = true;

        } else {
          ispaymentresponseokay = false;
        }

      } else {
        ispaymentresponseokay = true;
      }

      if(ispaymentresponseokay && this.editpayment.valid) {
        const data: any = {
          billingname: this.editpayment.controls.billingname.value,
          billingaddres1: this.editpayment.controls.billingaddres1.value,
          billingaddres2: '',
          billingcountry: this.editpayment.controls.billingcountry.value,
          billingregion: this.editpayment.controls.billingregion.value,
          billingcity: this.editpayment.controls.billingcity.value,
          billingpostalcode: this.editpayment.controls.billingpostalcode.value,
          billingphone: this.editpayment.controls.billingphone.value,
          shiptobilling: this.editpayment.controls.shiptobilling.value,
          payment: this.paymentresponse,
          payment_identifer: this.paymentIdentifier,
          billing_id: this.billingId,
          shipping_id: this.shippingId,
          shipLabel: this.shipLabel,
          ship_name: this.editpayment.controls.billingname.value,
          ship_address1: this.editpayment.controls.billingaddres1.value,
          ship_address2: '',
          ship_country: this.editpayment.controls.billingcountry.value,
          ship_region: this.editpayment.controls.billingregion.value,
          ship_city: this.editpayment.controls.billingcity.value,
          ship_postalcode: this.editpayment.controls.billingpostalcode.value,
          ship_phone: this.editpayment.controls.billingphone.value,
          form_action: 'billing_form',
          userid: userID
        };
        this.profileService.updateAddress(data).subscribe((response: any) => {
          this.loadingController.dismiss();

          if (response.status) {
            if(response.payment_information_decline != undefined){
              this.presentAlert('Your payment information could not be verified. Please check your details and try again. If the problem persists, please contact your card issuer.');

            }else{
              this.closeModal({
                openModal: 'SellListReview',
                data: response
              });
            }
            
            // if(response.payment_information_decline){
            //   this.presentAlert('Error: Your are not authorized, please check card detail you entered.');
            // }else{
            //   this.closeModal({
            //     openModal: 'SellListReview',
            //     data: response
            //   });
            // }


            

          } else {
           this.presentAlert(response.error);
          }



        });
      }else{
       // this.loadingController.dismiss();
       
      }

    }


  }

  doRefresh(event){

  }

  doPayment(){
    console.log('do payment');

  }
  setPaymentResponse(d){
    console.log('payment response');
    this.paymentresponse = d;
    //console.log(data);
    this.save();

  }
  togglePaymentOption(){
    console.log('Toogle payment optiion');

    (function($) {
      $(document).ready(function() {
        console.log('Toogle payment optiion2');
      });

      $('#dropin-container').find('.braintree-option.braintree-option__paypal').hide();
    });

  }
  async presentLoader() {
    const loading = await this.loadingController.create({
      message: ''
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  hideLoader(){
    this.loadingController.dismiss();
  }
  showLoader(){
    if(this.editpayment.valid){
      this.presentLoader();
    }
   

  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }

  postalCode_Valid(e){
    // const pattern = new RegExp(/^[A-Za-z]\d[A-Za-z][ ]\d[A-Za-z]\d$/);
    let postalcode = this.editpayment.controls.billingpostalcode.value;

    postalcode = postalcode.toUpperCase();

    
    if(e.key!="Backspace" && e.keyCode!=8){
      if(postalcode.length==3){
        postalcode = postalcode.replace(/^(.{3})(.*)$/, "$1 $2");
      }else{
        if(postalcode.length > 3){
          if(postalcode[3]!=" "){
            postalcode = postalcode.replace(/^(.{3})(.*)$/, "$1 $2");
          }
  
        }
      }

    }

    
    

    this.editpayment.controls.billingpostalcode.setValue(postalcode);
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Payment Screen');
  }


}
