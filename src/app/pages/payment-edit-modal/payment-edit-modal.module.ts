import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentEditModalPage } from './payment-edit-modal.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentEditModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PaymentEditModalPage]
})
export class PaymentEditModalPageModule {}
