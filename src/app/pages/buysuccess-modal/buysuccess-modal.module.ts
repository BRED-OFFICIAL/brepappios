import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuysuccessModalPage } from './buysuccess-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BuysuccessModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuysuccessModalPage]
})
export class BuysuccessModalPageModule {}
