import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams,LoadingController} from '@ionic/angular';
import { ProductService } from '../../services/product.service';
import {ProfileService} from '../../services/profile.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
@Component({
  selector: 'app-buysuccess-modal',
  templateUrl: './buysuccess-modal.page.html',
  styleUrls: ['./buysuccess-modal.page.scss'],
})
export class BuysuccessModalPage implements OnInit {
  productid: number;
  order: any;
  amount: number;
  product: any;
  productsize: any;
  loaderToShow: any;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private navCtrl: NavController,
    private router: Router,
    private productservice: ProductService,
    private profileService: ProfileService,
  ) { }

  ngOnInit() {
    this.productid = this.navParams.data.productid;
    this.order = this.navParams.data.order;
    this.product = this.navParams.data.product;
    this.productsize = this.navParams.data.productsize;
    console.log(this.productid);
    console.log(this.order);
    console.log(this.product);
    this.profileService.loadbuying(localStorage.getItem('userID'), '', '');


  }
  async closeModal() {
    const data = {closed: true};
    data.closed = true;
    await this.modalController.dismiss(data);

  }
  redirecttohome(){
    //this.showLoader();
     this.closeModal();

    //this.navCtrl.navigateForward(["tabs/home"]);
    
    //this.router.navigate(['tabs/home']);
    //window.location.reload();
    

  }
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        //clearTimeout(this.loadervisiblecheck);
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }
  redirectToOrderDetail(){
    this.closeModal();
    this.navCtrl.navigateForward(['/buying/order-detail/',this.order.id]);
  }


}
