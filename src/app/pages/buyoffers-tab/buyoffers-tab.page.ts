import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams, LoadingController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {ProductService} from '../../services/product.service';
import { map } from 'rxjs/operators';
import {BuyoffersreviewModalPage} from '../buyoffersreview-modal/buyoffersreview-modal.page';
import { BuyreviewpurchaseModalPage } from '../../pages/buyreviewpurchase-modal/buyreviewpurchase-modal.page';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'app-buyoffers-tab',
  templateUrl: './buyoffers-tab.page.html',
  styleUrls: ['./buyoffers-tab.page.scss'],
})
export class BuyoffersTabPage implements OnInit {

  product: any;
  productid: number;
  productsizes: any;
  productsizeselected: any;
  productsizeid: number;
  loaderToShow: any;
  bidamount: string;
  bidamountnumber: number;
  buyershippingfee: number;
  processFee: number;
  transfee: number;
  ordertotal: number;
  bidmessage: string;

  highestBid: any;
  lowestask: any;
  prodSizeValue: number;


  id: number;
  previousamount: number;

  constructor(
    private navParams: NavParams,
    private route: ActivatedRoute,
    private modalController: ModalController,
    private productservice: ProductService,
    private loadingController: LoadingController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService 
  ) { }

  ngOnInit() {
    console.log(this.navParams.data);
    this.product = this.navParams.data.product;
    this.productid = this.navParams.data.productid;
    this.productsizeid = this.navParams.data.productSize;
    this.highestBid = this.navParams.data.highestBid;
    this.lowestask = this.navParams.data.lowestAsk;
    this.prodSizeValue = Number(this.navParams.data.sizeValue);
    this.bidamount = '0';
    this.bidamountnumber = 0;
    this.ordertotal = 0;

    // console.log('Highest ',this.highestBid);
    // console.log('Lowest ',this.lowestask);
    console.log('data ',this.navParams.data);

    if (this.navParams.data.id !== undefined){
      this.id = this.navParams.data.id;
    }
    if (this.navParams.data.amount !== undefined){
      this.previousamount = this.bidamount = this.bidamountnumber = this.navParams.data.amount;
    }



    //this.productsizeselected = this.navParams.data.productsizeselected;
    this.showLoader();
    
    if (this.productid) {
      this.productservice.getproductsize(this.productid)
    .subscribe( (productpagedata) => {
      this.productsizes = productpagedata['size'];
      for(let data of this.productsizes) {
        if (this.productsizeid == data[0].id ) {
            this.productsizeselected = data;
        }
      }
      let buyerid: string;
      if(localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
        buyerid = localStorage.getItem('userID');
      }


      this.productservice.getbuyerfees(buyerid).subscribe((data: any) => {
        this.buyershippingfee = Number(data.buyerfee);
        this.transfee = data.trans_fee;
        this.loadingController.dismiss();
      });
    });

    }

  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }
  adjustbidamount(value: string) {
    if (this.bidamount === '0') {
      this.bidamount = '' + value;
    } else {
      this.bidamount = '' + this.bidamount + value;
    }
    this.bidamountnumber = Number(this.bidamount);
    this.updateprice();
    this.bidmessagecheck();
  }

  clearbidamount(){
    if (this.bidamount.length > 0) {
      this.bidamount = this.bidamount.slice(0, -1);
    }
    if (this.bidamount === ''){
      this.bidamount = '0';
    }
    this.bidamountnumber = Number(this.bidamount);
    this.updateprice();
    this.bidmessagecheck();
  }

  updateprice() {
    let amount: number;
    amount = this.bidamountnumber;
    this.processFee = ((+amount + +this.buyershippingfee) * 0.035);
    this.ordertotal = amount + +this.processFee.toFixed(2) + +this.buyershippingfee;
    if (amount === 0) {
      this.ordertotal = 0;
    }
  }

  bidmessagecheck() {
    let amount: number;
    amount = this.bidamountnumber;

    let lowestaskvalue = 0;
    let highestbidvalue = 0;
    if (this.lowestask) {
      lowestaskvalue = this.lowestask.lowest_ask_price;
    }

    if (this.highestBid) {
      highestbidvalue = this.highestBid.highest_bid_price;
    }

    if (amount < 25) {
      this.bidmessage = 'The minimum amount you can offer is $25';
    }else if ((lowestaskvalue <= amount && lowestaskvalue != 0)){
      this.bidmessage = 'You\'re about to purchase this product at the lowest price $' + lowestaskvalue;
    }
    else if(amount < highestbidvalue){
      this.bidmessage = 'You do not have the highest offer';
    }
    else if(amount == highestbidvalue){
      this.bidmessage = 'You\'re about to match the highest offer. Their offer will be accepted first';
    }else if (highestbidvalue < amount){
      this.bidmessage = 'You\'re about to place the highest offer';
    }else{
      this.bidmessage="";
    }
  }
  
  async loadbuyoffersconfirmmodal() {
    this.closeModal();
    const modal = await this.modalController.create({
      component: BuyoffersreviewModalPage,
      cssClass: 'buyoffersreview-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        bidamount: this.bidamountnumber,
        processFee: this.processFee,
        ordertotal: this.ordertotal,
        buyershippingfee: this.buyershippingfee,
        productsizeid: this.productsizeid,
        productsizeselected: this.productsizeselected,
        id: this.id
      }
    });

    modal.onDidDismiss().then ((dataReturned) => {
      console.log(dataReturned);
      if (dataReturned !== null) {
        if (dataReturned.data.openmodel == 'BuypopupTabsPage'){
          console.log('dfsfsdfsf');
          this.buypopup(dataReturned.data.data, 'offers');


        }
       // this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });

    return await modal.present();



    if (this.bidamountnumber >= 25 ) {
      console.log(this.bidamountnumber);
      console.log(this.ordertotal);
      console.log(this.processFee);

    }

  }

  async buypopup(data: any, tab: string) {
     
    const modal = await this.modalController.create({
      component: BuyoffersTabPage,
      cssClass: 'buyoffers-tabs',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        productSize: this.productsizeid,
        product: this.product,
        productsizes: this.productsizes,
        highestBid: this.highestBid,
        lowestAsk: this.lowestask,
        sizeValue: this.prodSizeValue
        }
    });

    return await modal.present();

  }

  async reviewpurchasemodal(lowestPrice,size, askid, sizevalue ){
    this.closeModal();
    console.log('offer id', this.id);
    if(localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      const modal = await this.modalController.create({
        component: BuyreviewpurchaseModalPage,
        // cssClass: 'buypopup-tabs',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.productid,
          product: this.product,
          lowestPrice: lowestPrice,
          size: size,
          askid: askid,
          sizeValue: sizevalue,
          offerid: this.id
        }
      });

      modal.onDidDismiss().then((response) => {
        if (response !== null) {
          if (response.data.openmodal == 'BuypopupTabsPage'){
            //response.data.data.defaulttab = 'offers';
            this.buypopup(response.data.data, 'offers');
          }
        }
      });

      return await modal.present();
    } else {
      this.showLoader();
      let navigationExtras: NavigationExtras = {
        queryParams: {
          productid: this.productid,
          lowestPrice: lowestPrice,
          size: size,
          askid: askid,
          modal: 'BuyreviewpurchaseModalPage',
          redirectPath: 'productdetail/' + this.productid
        }
      };
      this.router.navigate(['login'], navigationExtras);
      


    }
  }

  async loadmymodal() {

    const modal = await this.modalController.create({
      component: BuyoffersTabPage,
      cssClass: 'buyoffers-tab.page',
      componentProps: {
        productid: this.productid,
        productSize: this.productsizeid,
        product: this.product,
        productsizes: this.productsizes,
        highestBid: this.highestBid,
        lowestAsk: this.lowestask,
        sizeValue: this.prodSizeValue
      }
    });
    return await modal.present();
  }

  loadreviewmodal(){
    let amount: number;
    amount = this.bidamountnumber;

    let lowestaskvalue = 0;
    let highestbidvalue = 0;
    if (this.lowestask) {
      lowestaskvalue = this.lowestask.lowest_ask_price;
    }

    if (this.highestBid) {
      highestbidvalue = this.highestBid.highest_bid_price;
    }

    if (amount < 25) {
      this.bidmessage = 'You must meet the minimum Bid of $25';
    }else if ((lowestaskvalue <= amount && lowestaskvalue != 0)){
      this.reviewpurchasemodal(lowestaskvalue, this.productsizeid, this.lowestask.id, this.prodSizeValue);
    }
    else if(amount < highestbidvalue){
      this.loadbuyoffersconfirmmodal();
    }
    else if(amount == highestbidvalue){
      this.loadbuyoffersconfirmmodal();
    }else if (highestbidvalue < amount){
      this.loadbuyoffersconfirmmodal();
    }else{
      this.loadbuyoffersconfirmmodal();
    }
  }

  goback() {
    this.closeModal(
      {
        openmodel: 'BuypopupTabsPage',
        tab: 'offer',
        data: {
          productid: this.productid,
          product: this.product
        }
      }
    );
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Offer Price Screen');
  }
}
