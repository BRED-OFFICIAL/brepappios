import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyoffersTabPage } from './buyoffers-tab.page';

describe('BuyoffersTabPage', () => {
  let component: BuyoffersTabPage;
  let fixture: ComponentFixture<BuyoffersTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyoffersTabPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyoffersTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
