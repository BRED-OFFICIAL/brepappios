import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BuyoffersTabPage } from './buyoffers-tab.page';

const routes: Routes = [
  {
    path: '',
    component: BuyoffersTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BuyoffersTabPage]
})
export class BuyoffersTabPageModule {}
