import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams,Platform,LoadingController, ToastController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { OrderService } from '../../services/order.service';
import {ProfileService} from '../../services/profile.service';
import {BuyoffersuccessModalPage} from '../buyoffersuccess-modal/buyoffersuccess-modal.page';
import {PaymentEditModalPage} from '../../pages/payment-edit-modal/payment-edit-modal.page';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'app-buyoffersreview-modal',
  templateUrl: './buyoffersreview-modal.page.html',
  styleUrls: ['./buyoffersreview-modal.page.scss'],
})
export class BuyoffersreviewModalPage implements OnInit {

  productid: number;
  buyid: any;
  askid: number;
  lowestAskVal: number;
  discountPrice: number;
  prodSize: number;
  discountCoupn: number;
  discountCoupnId: number;
  discountCoupnType: number;
  bidorbuy: any;
  credit: number;
  tokenId: any;
  product: any;
  loadertoshow: any;
  buyerpaymentinformation: any;
  buyershippinginformation: any;
  buyerbillinginformation: any;
  subtotal: number;
  shippingamount: number;
  paymentprocamount: number;  //Payment Proc: 3.5%
  authenticationfee: number;
  transfee: number;
  id: number;

  price: number;
  totalamount: number;
  buyerfee: number;
  loadervisiblecheck: any;
  loaderToShow: any;
  termsaccepted: boolean;
  productsizeselected: any;

  fingerPrintOptions: FingerprintOptions;

  constructor(
    private modalController: ModalController,
    private productpageserv: ProductService,
    private orderService: OrderService,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private profileService: ProfileService,
    private toastController: ToastController,
    private fingerPrint: FingerprintAIO,
    private platform: Platform,
    private gaAnalyticsService: GoogleAnalyticsService 
  ) { 

    this.fingerPrintOptions = {
      title:'Confirm Offer',
      // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      //clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      disableBackup:true,  //Only for Android(optional)
      // localizedFallbackTitle: 'Use Pin', //Only for iOS
      //  localizedReason: 'Please authenticate' //Only for iOS
    };


  }

  ngOnInit() {

    this.productid = this.navParams.data.productid;
    this.product = this.navParams.data.product;
    this.price = this.navParams.data.bidamount;
    this.shippingamount = this.navParams.data.buyershippingfee;
    this.paymentprocamount = Number(this.navParams.data.processFee);
    this.totalamount = 0;
    this.termsaccepted = false;
    this.prodSize = this.navParams.data.productsizeid;
    this.productsizeselected = this.navParams.data.productsizeselected;

    if(this.navParams.data.id !== undefined){
      this.id = this.navParams.data.id;
    }
    // this.lowestAskVal = parseInt(this.navParams.data.lowestPrice);
    // this.askid = this.navParams.data.askid;




    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      this.buyid = localStorage.getItem('userID');
    }

    if (this.buyid !== '') {

      // this.subtotal = 0.00;
      // this.shippingamount = 0.00;
      // this.paymentprocamount = 0.00;
      this.authenticationfee = 0.00;
      // this.totalamount = 0.00;
      this.showLoader();






      this.productpageserv.getbuyerallinformation(this.buyid)
        .subscribe( (buyeralldata: any) => {
          console.log(buyeralldata);
          this.buyerpaymentinformation = buyeralldata['payment'];
          this.buyerbillinginformation = buyeralldata['billing'];
          this.buyershippinginformation = buyeralldata['shipping'];
          this.buyerfee = buyeralldata['buyerfee'];
          this.transfee = buyeralldata['trans_fee'];
          // this.shippingamount = parseInt(buyeralldata['buyerfee']);
          
          //this.price = +this.lowestAskVal + +this.shippingamount;
          //this.paymentprocamount = (this.price * 0.035);
          this.totalamount = this.price + this.paymentprocamount;
          this.totalamount = this.shippingamount + this.totalamount;
          // this.totalamount = Number(this.paymentprocamount.toFixed(2) + this.totalamount.toFixed(2));
          
          this.loadingController.dismiss();
        });


    }



  }

  async closeModal(data = {}) {
    console.log('closing modal with data');
    console.log(data);
    await this.modalController.dismiss(data);
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        clearTimeout(this.loadervisiblecheck);
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  async confirmorder() {
    
    console.log('Buyerid:' + this.buyid);
    console.log('Productid:' + this.productid);
    console.log('ProductSize:' + this.prodSize);
    console.log('ProductPrice:' + this.price);
    console.log('ShippingFee:' + this.shippingamount);
    console.log('ProcessFee:' + this.paymentprocamount);
    console.log('Total:' + this.totalamount);

    try{
      await this.platform.ready();
      const available = await this.fingerPrint.isAvailable();
      this.fingerPrint.show(this.fingerPrintOptions)
      .then((result: any) => {
        if(result == 'biometric_success'){
          this.processConfirmOffer();
        }
      })
      .catch((error: any) => {
        console.log(error);
        this.presentToast('Finger touch lock doesn\'t match.');

      });

    } catch(e) {
      this.processConfirmOffer();
    }

  }

  processConfirmOffer(){
    this.showLoader();
    if (this.id){
      this.profileService.deleteOffer(this.id).subscribe((response: any) => {

        this.orderService.confirm(
          this.productid,
          this.buyid,
          this.prodSize,
          this.price,
          this.shippingamount,
          this.paymentprocamount.toFixed(2),
          this.totalamount.toFixed(2),
          null,
          null,
          null)
          .subscribe( (response1: any) => {
            if (response1.status) {
              // this.loadsuccessordermodal(response.order);
              this.loadingController.dismiss();
              console.log('response');
              console.log(response1);
              this.loadoffersuccess(response1.data);
            } else {
            }
          });


      });

    } else {
      this.orderService.confirm(
        this.productid,
        this.buyid,
        this.prodSize,
        this.price,
        this.shippingamount,
        this.paymentprocamount.toFixed(2),
        this.totalamount.toFixed(2),
        null,
        null,
        null)
        .subscribe( (response1: any) => {
          if (response1.status) {
            // this.loadsuccessordermodal(response.order);
            this.loadingController.dismiss();
            console.log('response');
            console.log(response1);
            this.loadoffersuccess(response1.data);
          } else {
          }
        });


   

    }
  }
  openbuynowpopup(){

    this.closeModal({
          openmodel: 'BuypopupTabsPage',
          data: {
            productid: this.productid,
            product: this.product,
            productSize: this.prodSize
          }
        });
    

  }

  async loadoffersuccess(response: any) {
    console.log('Load offer resposne');
    console.log(response);
    this.closeModal();
    const modal = await this.modalController.create({
      component: BuyoffersuccessModalPage,
      cssClass: 'buyoffersuccess-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        order: response,
        totalamount: this.totalamount,
        productsizeselected: this.productsizeselected
      }
    });

    return await modal.present();



  }

  async paymentedit() {
    this.closeModal();
    const modal = await this.modalController.create({
      component: PaymentEditModalPage,
      cssClass: 'payment-edit-modal',
      componentProps: {
        payment: this.buyerpaymentinformation,
        billing: this.buyerbillinginformation,
        shipping: this.buyershippinginformation,
        paymentIdentifer: 'buyer_payment',
        paypal: false
      }
    });
    modal.onDidDismiss().then((response: any) => {
      this.openself();

    });
    return await modal.present();
  }

  async openself() {
    const modal = await this.modalController.create({
      component: BuyoffersreviewModalPage,
       cssClass: 'buyoffersreview-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.productid,
        product: this.product,
        bidamount: this.price,
        buyershippingfee: this.shippingamount,
        processFee: this.paymentprocamount,
        productsizeid: this.prodSize,
        productsizeselected: this.productsizeselected
      }
    });

    modal.onDidDismiss().then((response: any) => {
      // console.log(response.data.modal);
      if (response !== null) {
        if (response.data.openmodal === 'BuypopupTabsPage'){
          // response.data.data.defaulttab = 'offers';
         // this.buypopuptab(response.data.data);
          //this.buypopuptab(response.data.data);
        }
      }
    });



    return await modal.present();


  }

  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
  

ionViewWillEnter(){
  this.gaAnalyticsService.setTrackScreen('Offer Review Screen');
}


}
