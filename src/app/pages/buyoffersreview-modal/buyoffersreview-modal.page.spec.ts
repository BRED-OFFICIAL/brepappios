import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyoffersreviewModalPage } from './buyoffersreview-modal.page';

describe('BuyoffersreviewModalPage', () => {
  let component: BuyoffersreviewModalPage;
  let fixture: ComponentFixture<BuyoffersreviewModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyoffersreviewModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyoffersreviewModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
