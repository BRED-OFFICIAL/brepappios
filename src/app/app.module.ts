import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthenticationInterceptor} from '../interceptors/authentication.interceptor';
import { AuthModule } from './auth/auth.module';
import { IonicStorageModule } from '@ionic/storage';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { AuthenticGuaranteedPopupPageModule } from './popups/authentic-guaranteed-popup/authentic-guaranteed-popup.module';
import { ProductHowToBuyPopupPageModule } from './popups/product-how-to-buy-popup/product-how-to-buy-popup.module';
import { ProductHowToSellPopupPageModule } from './popups/product-how-to-sell-popup/product-how-to-sell-popup.module';

import { BuyPopupPageModule } from './popups/buy-popup/buy-popup.module';
import {HomePageModule} from './home/home.module';
import { BuypopupTabsPageModule } from './pages/buypopup-tabs/buypopup-tabs.module';
import { SellOnBoardModalPageModule} from './pages/sell/sell-on-board-modal/sell-on-board-modal.module';
import {SellPriceModalPageModule} from './pages/sell/sell-price-modal/sell-price-modal.module';
import {SellReviewModalPageModule} from './pages/sell/sell-review-modal/sell-review-modal.module';
import {SellSuccessModalPageModule} from './pages/sell/sell-success-modal/sell-success-modal.module';
import {PaymentEditModalPageModule} from './pages/payment-edit-modal/payment-edit-modal.module';
import {PayoutEditModalPageModule} from './pages/payout-edit-modal/payout-edit-modal.module';
import {SellListPriceModalPageModule} from './pages/sell/sell-list-price-modal/sell-list-price-modal.module';
import {SellListSuccessModalPageModule} from './pages/sell/sell-list-success-modal/sell-list-success-modal.module';
import {SellListReviewModalPageModule} from './pages/sell/sell-list-review-modal/sell-list-review-modal.module';
import {DeleteModalPageModule} from './popups/delete-modal/delete-modal.module';
import {BuyingOfferDetailModalPageModule} from './popups/buying-offer-detail-modal/buying-offer-detail-modal.module';
import {SellingListingDetailModalPageModule} from './popups/selling-listing-detail-modal/selling-listing-detail-modal.module';
import {ShippingAddressModalPageModule} from './popups/shipping-address-modal/shipping-address-modal.module';
import {SearchfilterSizePageModule} from './popups/searchfilter-size/searchfilter-size.module';
import {SearchfilterBrandPageModule} from './popups/searchfilter-brand/searchfilter-brand.module';
import {FiltersPageModule} from './filters/filters.module';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {Base64} from '@ionic-native/base64/ngx';
import {FileTransfer} from '@ionic-native/file-transfer/ngx';
import {File} from '@ionic-native/file/ngx';
import {Facebook} from '@ionic-native/facebook/ngx';
import {ThemeableBrowser} from '@ionic-native/themeable-browser/ngx';
import {Deeplinks} from '@ionic-native/deeplinks/ngx';
//import {SplashPage} from './splash/splash.page';
import {SplashPageModule} from './splash/splash.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FingerprintAIO} from '@ionic-native/fingerprint-aio/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
//import { Firebase } from '@ionic-native/firebase/ngx';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [AppComponent],
  imports: [BrowserModule, SlickCarouselModule, IonicModule.forRoot(), AppRoutingModule ,
    HttpClientModule, AuthModule, IonicStorageModule.forRoot(), AuthenticGuaranteedPopupPageModule,
    BuyPopupPageModule, BuypopupTabsPageModule, ProductHowToBuyPopupPageModule,
    ProductHowToSellPopupPageModule, SellOnBoardModalPageModule, SellPriceModalPageModule, SellReviewModalPageModule,
    SellSuccessModalPageModule, PaymentEditModalPageModule, SellListPriceModalPageModule, SellListSuccessModalPageModule,
    SellListReviewModalPageModule,PayoutEditModalPageModule,DeleteModalPageModule,BuyingOfferDetailModalPageModule,
    SellingListingDetailModalPageModule,ShippingAddressModalPageModule,FiltersPageModule,SearchfilterSizePageModule,
    SearchfilterBrandPageModule,SplashPageModule,BrowserAnimationsModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SocialSharing,
    Base64,
    FileTransfer,
    File,
    Facebook,
    ThemeableBrowser,
    Deeplinks,
    FingerprintAIO,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    GoogleAnalytics,
 //    Firebase
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
