import {Injectable} from '@angular/core';
import {FormControl, AbstractControl, FormGroup} from '@angular/forms';
import {LoginService} from '../services/login.service';
@Injectable({providedIn: 'root'})
export class EmailValidator {

    iscalling: any;

    constructor(public loginService: LoginService) {

    }
    checkEmail(control: FormControl): any {
        clearTimeout(this.iscalling);
        return new Promise(resolve => {
            this.iscalling = setTimeout(() => {
                this.loginService.isemailunqiue(control.value, localStorage.getItem('userID')).subscribe((response: any) => {
                    if (response.status) {
                        resolve(null);
                    } else {
                        resolve({emailexists: true});
                    }
                });
            }, 1000);
        });
    }
    confirmEmail(c: AbstractControl ): { invalid: boolean } {
        if (c.get('email').value !== c.get('confirmemail').value) {
            return {invalid: true};
        }

    }
}

export function EmailMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }

}