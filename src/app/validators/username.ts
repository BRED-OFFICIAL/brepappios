import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';
import {LoginService} from '../services/login.service';
@Injectable({providedIn: 'root'})
export class UsernameValidator {

    iscalling: any;

    constructor(public loginService: LoginService) {

    }
    checkusername(control: FormControl): any {
        clearTimeout(this.iscalling);

        return new Promise(resolve => {
            let userID = null;

            if (+localStorage.getItem('NewuserID')){
                userID = +localStorage.getItem('NewuserID');
            }

            if (+localStorage.getItem('userID')){
                userID = +localStorage.getItem('userID');
            }

            this.iscalling = setTimeout(() => {
                this.loginService.isusernameunqiue(control.value, userID).subscribe((response: any) => {
                    if (response.status) {
                        resolve(null);
                    } else {
                        resolve({usernameexists: true});
                    }
                });
            }, 1000);
            


        });
    }
}