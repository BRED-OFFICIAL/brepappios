import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, NavController, IonTabs,  } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  @ViewChild('myTabs',{read: IonTabs}) tabs: IonTabs;
  constructor(
    private platform: Platform,
    private navCtrl: NavController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService
  ) { }
  isUserLogin: boolean;
  activetab: any;
  subscription: any;

  ngOnInit() {
   this.SelectTab();
   this.checkUserLogin();
   //this.tabs.select('browse');
    
  }
  SelectTab(str = 'home'){
    this.activetab = str;
    if(!+localStorage.getItem('userID')){
      if(this.activetab=='profile'){
        const navigationExtras: NavigationExtras = {
          queryParams: {
            redirectPath: 'settings'
          },
          // skipLocationChange: true,
        };
        console.log('tab rediect to login');
        // this.router.navigate(['/login'], navigationExtras);
        this.navCtrl.navigateForward(['/login']);
      }
    }
  }

  ionTabsDidChange(){
    
    //this.tabs.
    console.log('calling event');
  }
  ionTabsWillChange(){
    console.log('calling event2');
  }
  checkUserLogin(){
    if (localStorage.getItem('userID') === '' || localStorage.getItem('userID') === null ) {
      this.isUserLogin  = false;
    } else{
      this.isUserLogin  = true;
    }

  }


  ionViewDidEnter(){
    this.checkUserLogin();
    this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
      console.log( this.router.url);
      if (this.router.url.split('?')[0] === '/tabs/browse') {
        console.log('browse');
        // this.router.navigate(['/tabs/home']);
        // if(+localStorage.getItem('userID')){
        //   this.navCtrl.navigateRoot(['/tabs/home']);
        // }else{
        //   // this.navCtrl.back();
        //   this.navCtrl.navigateBack(['/tabs/home']);
        // }

        this.tabs.select('home');

        //this.navCtrl.pop();
        
      }
      else if(this.router.url === '/tabs/profile'){
        console.log('profile');
        this.tabs.select('home');
      }
      else{
        console.log('home');
        
        if(+localStorage.getItem('userID')){
          navigator['app'].exitApp();
        }else{
          this.navCtrl.pop();
        }
        
      }
    });
  }

  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

  redirecttologin(){
    this.gaAnalyticsService.setTrackScreen('Profile redirecting to login');
    const navigationExtras: NavigationExtras = {
      queryParams: {
        redirectPath: 'settings'
      },
     //  skipLocationChange: true,
    };
    console.log('tab rediect to login');
    // this.router.navigate(['/login'], navigationExtras);
     this.navCtrl.navigateForward(['/login']);
  }

  redirecttobrowse(){
    const navigationExtras: NavigationExtras = {
      queryParams: {
        sort: 'null'
      },
     //  skipLocationChange: true,
    };
   
     this.navCtrl.navigateForward(['/tabs/browse']);

  }

  

}
