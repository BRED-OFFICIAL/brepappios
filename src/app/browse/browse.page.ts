import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController} from '@ionic/angular';
import { HomepageService } from './../services/homepage.service';
import { BrowseService } from './../services/browse.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { FiltersPage } from '../filters/filters.page';
import { GoogleAnalyticsService } from '../services/google-analytics.service';


@Component({
  selector: 'app-browse',
  templateUrl: './browse.page.html',
  styleUrls: ['./browse.css']
})
export class BrowsePage implements OnInit {
  constructor(
    private router: Router,
    private homepageserv: HomepageService,
    private browseserv: BrowseService,
    private dataService: DataService,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private route: ActivatedRoute,
     private gaAnalyticsService: GoogleAnalyticsService
   
  ) {}
  items: any;
  product_list: [];
  data = { gridview: true, listview: false };
  MyStrings = ['one', 'two', 'three'];
  NewMyStrings = 'four';
  fromlimit = 0;
  fetchproducts = 8;
  product_list_appended: [];
  filters: any;
  selectProductId: number;
  search = '';
  totalproduct = 0;
  issearch = false;
  isloading = false;

  searchServiceObj: any;
  filtercount = 0;

  ngOnInit() {
    
  }

  clearSearchData() {
    this.search = '';
    this.searchProduct();
  }

  clearSearchDataOnBlank(ev) {
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() == '') {
      this.clearSearchData();
    }
  }

  getItems(ev) {
    if(ev.target.value == "" && this.search == ""){
      return;
    }

    
    this.search = ev.target.value;
    //this.presentLoader();
    this.searchProduct();
   
  }

  searchProduct(){
    this.product_list = [];
    this.issearch = false;
    this.isloading = true;
    if(this.searchServiceObj){
      this.searchServiceObj.unsubscribe();
    }
    this.reset();
    this.getProducts(this.fromlimit,this.fetchproducts).then(()=>{
      setTimeout(() => {
         //this.loadingController.dismiss();
      }, 2000);
    });

  }

  async getProducts(fromlimit, fetchproducts) {
    let sortby = '';
    let gender = '';
    let category = '';
    let size = '';
    
    if (this.filters && this.filters.SortBy !== undefined){
      sortby = this.filters.SortBy;
    }

    if (this.filters && this.filters.Gender !== undefined){
      gender = this.filters.Gender;
    }

    if (this.filters && this.filters.Category !== undefined &&  this.filters.Category.id !== undefined){
      category = this.filters.Category.id;
    }

    if (this.filters && this.filters.Size !== undefined && this.filters.Size.id !== undefined){
      size = this.filters.Size.id;
    }

    this.searchServiceObj  = await this.browseserv
    .getproducts(fromlimit, fetchproducts, gender, category, size, sortby, this.search)
      .subscribe((product_list: any) => {
        this.product_list = product_list['products'];
        this.totalproduct = product_list.totalproduct;
        this.fromlimit = this.fromlimit + 8;
        this.isloading = false;
        if(this.search != ''){
          this.issearch = true;
        }else{
          this.issearch = false;
        }


      });
  }

  changeView(type) {
    if (type == 'showgrid') {
      this.data.gridview = true;
      this.data.listview = false;
    } else {
      this.data.gridview = false;
      this.data.listview = true;
    }
  }

  async showfilterpage() {
    console.log('brwose', this.filters);

    const modal = await this.modalController.create({
      component: FiltersPage,
      cssClass: 'filters',
      componentProps: this.filters
    });

    modal.onDidDismiss().then((response) => {
      const oldfilter = this.filters;
      if (response !== null) {
        this.filters = response.data;
      }

      if(oldfilter !== this.filters){
        this.filtercount = 0;
        console.log(this.filters);
        if(this.filters.Size){
          this.filtercount++;
        }
        if(this.filters.Category){
          this.filtercount++;
        }


        this.reset();
        this.getProducts(this.fromlimit, this.fetchproducts);
      }


    });

    return await modal.present();
  }

  test() {
    this.MyStrings.push(this.NewMyStrings);
  }

  loadMoreProducts(event) {
    // this.product_list_appended = [];
    let sortby = '';
    let gender = '';
    let category = '';
    let size = '';
    if (this.filters && this.filters.SortBy !== undefined){
      sortby = this.filters.SortBy;
    }

    if (this.filters && this.filters.Gender !== undefined){
      gender = this.filters.Gender;
    }

    if (this.filters && this.filters.Category !== undefined &&  this.filters.Category.id !== undefined){
      category = this.filters.Category.id;
    }

    if (this.filters && this.filters.Size !== undefined && this.filters.Size.id !== undefined){
      size = this.filters.Size.id;
    }

    setTimeout(() => {
      this.browseserv
        .getproducts(this.fromlimit, this.fetchproducts, gender, category, size, sortby, this.search)
        .subscribe(product_list_appended => {
          this.product_list_appended = product_list_appended['products'];
          this.fromlimit = this.fromlimit + 8;
          for (let i = 0; i < this.product_list_appended.length; i++) {
            this.product_list.push(this.product_list_appended[i]);
          }
          this.product_list_appended = [];
          //console.log(this.product_list_appended);
          //console.log(this.product_list);
          event.target.complete();
        });

      

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
    }, 500);
  }

  gotoproductdetails(product_id) {
    this.selectProductId = product_id;

    const navigationExtras: NavigationExtras = {
      state: {
        product_id
      }
    };
    setTimeout(()=>{
      this.router.navigate(['productdetail', product_id]);
    },500);
    
  }

  reset(){
    this.fromlimit = 0;
    // this.product_list = [];
  }
 

  async presentLoader() {
    console.log('prsent laoder at product page');
    let loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  doRefresh($event){

    this.getProducts(0, this.fetchproducts).then(()=>{
      setTimeout(() => {
        console.log('Async operation has ended');
        $event.target.complete();
      }, 2000);
    });


    // this.loaddata().then(()=>{
    //   setTimeout(() => {
    //     console.log('Async operation has ended');
    //     $event.target.complete();
    //   }, 2000);
    //   //$event.target.complete();
    // });
  }

  ionViewWillEnter(){
    this.filters = {};
    this.filtercount = 0;
    this.gaAnalyticsService.setTrackScreen('Browse Screen');
    this.route.queryParamMap.subscribe((data:any)=>{
      console.log('calling ionviewenter');
    
      const sort = data.get('sort');
      let isfilterexists = false;
      let filter = {
        SortBy:''
      };
      if(sort !== ''){
        isfilterexists = true;
        filter.SortBy = sort;
      }
      if(filter != this.filters){
        this.filters = filter;

      }
      
    });



    this.presentLoader();

    if( this.searchServiceObj){
      this.searchServiceObj.unsubscribe();

    }

    this.reset();
    this.getProducts(this.fromlimit, this.fetchproducts).then(()=>{
      setTimeout(() => {
        this.loadingController.dismiss();
      }, 2000);
    });
    
  
  }


}
