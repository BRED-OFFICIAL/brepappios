import { Component, OnInit } from '@angular/core';
import {ModalController, LoadingController, NavParams, Platform} from '@ionic/angular';
import {BrowseService} from '../services/browse.service';
import {SearchfilterSizePage} from '../popups/searchfilter-size/searchfilter-size.page';
import {SearchfilterBrandPage} from '../popups/searchfilter-brand/searchfilter-brand.page';
import { Router } from '@angular/router';
@Component({
  selector: 'app-filters',
  templateUrl: './filters.page.html',
  styleUrls: ['./filters.css'],
})
export class FiltersPage implements OnInit {

  sortby: any;
  gender: any;
  selectcategory: any;
  selectsize: any;
  filter: any;
  gendertext: any;
  productCount: number;
  isproductcountloading = false;
  subscription: any;

  constructor(
    private modalController: ModalController,
    private loadingController: LoadingController,
    private navParams: NavParams,
    private browseService: BrowseService,
    private platform: Platform,
    private router: Router
  ) { }

  ngOnInit() {
    this.gender = 'men';
    this.sortby = 'featured';
    this.gendertext = 'All US Mens Sizes';

    if(this.navParams.data !== undefined){
      this.filter = this.navParams.data;
    }
    console.log('filter nginit',this.filter);
    if(this.filter.SortBy !== undefined && this.filter.SortBy){
      this.sortby = this.filter.SortBy;
    }

    if(this.filter.Gender !== undefined && this.filter.Gender){
      this.gender = this.filter.Gender;
    }

    if(this.filter.Size !== undefined && this.filter.Size){
      this.selectsize = this.filter.Size;
    }

    if(this.filter.Category !== undefined && this.filter.Category){
      this.selectcategory = this.filter.Category;
    }
    this.countResult();
  
  }

  back(){
    this.closeModal();
  }

  setgender(val= '',text = ''){
    if(this.gender !== val){
      this.gender = val;
      this.countResult();
    }
    this.gender = val;
    this.gendertext = text;
  }

  async loadSize() {
    const modal = await this.modalController.create({
      component: SearchfilterSizePage,
      cssClass: 'searchfilter-size',
      componentProps: {
        selectedsize: this.selectsize,
        gender: this.gender
      }
    });

    modal.onDidDismiss().then((response) => {
      if (response !== null) {
        if (response.data !== undefined && response.data.Size !== undefined){
          this.selectsize = response.data.Size;
        }
        this.countResult();
      }
    });

    return await modal.present();

  }

  async loadBrand() {
    const modal = await this.modalController.create({
      component: SearchfilterBrandPage,
      cssClass: 'searchfilter-brand',
      componentProps: {
        selectcategory: this.selectcategory,
      }
    });

    modal.onDidDismiss().then((response) => {
      console.log('onclosed', response);
      if (response !== null) {
        if (response.data !== undefined && response.data.Category !== undefined){
          this.selectcategory = response.data.Category;
        }
        this.countResult();
      }
    });

    return await modal.present();

  }


  async presentLoader() {
    console.log('prsent laoder at product page');
    let loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  updateFilters(){
    console.log(this.gender, this.sortby,  this.selectcategory, this.selectsize);
    this.closeModal({
      Gender: this.gender,
      SortBy: this.sortby,
      Category: this.selectcategory,
      Size: this.selectsize
    });
  }

  async closeModal(data = {}) {
    await this.modalController.dismiss(data);
  }
  reset(){
    this.gender = 'men';
    this.gendertext = 'All US Mens Sizes';
    this.sortby = 'featured';
    this.selectcategory = {};
    this.selectsize = {};
    this.countResult();

  }

  countResult(){
    let sortby = '';
    let gender = '';
    let category = '';
    let size = '';
    //this.presentLoader();
    this.isproductcountloading = true;
    if (this.sortby !== undefined){
      sortby = this.sortby;
    }

    if (this.gender !== undefined){
      gender = this.gender;
    }

    if (this.selectcategory !== undefined &&  this.selectcategory.id !== undefined){
      category = this.selectcategory.id;
    }

    if (this.selectsize !== undefined && this.selectsize.id !== undefined){
      size = this.selectsize.id;
    }

    this.browseService
      .getproductCount(gender, category, size, sortby)
      .subscribe((product: any) => {
        this.productCount = product['productscount'];
       // this.loadingController.dismiss();
       this.isproductcountloading = false;
    
      });

  }

  ionViewDidEnter(){
    
    this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
      console.log( this.router.url);
      if (this.router.url.split('?')[0] === '/tabs/browse') {
       this.modalController.dismiss();

       

        //this.navCtrl.pop();
        
      }
      
    });
  }

  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

}
