import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, LoadingController   } from '@ionic/angular';
import { AuthenticGuaranteedPopupPage } from '../popups/authentic-guaranteed-popup/authentic-guaranteed-popup.page';
import { ProductHowToBuyPopupPage } from '../popups/product-how-to-buy-popup/product-how-to-buy-popup.page'
import { ProductHowToSellPopupPage } from '../popups/product-how-to-sell-popup/product-how-to-sell-popup.page'
import { BuyPopupPage } from '../popups/buy-popup/buy-popup.page';
import { BuypopupTabsPage } from '../pages/buypopup-tabs/buypopup-tabs.page';
import { BuyreviewpurchaseModalPage } from '../pages/buyreviewpurchase-modal/buyreviewpurchase-modal.page';
import {BuyoffersTabPage} from '../pages/buyoffers-tab/buyoffers-tab.page';
import {SellOnBoardModalPage} from '../pages/sell/sell-on-board-modal/sell-on-board-modal.page';
import {SellPriceModalPage} from '../pages/sell/sell-price-modal/sell-price-modal.page';
import {SellReviewModalPage} from '../pages/sell/sell-review-modal/sell-review-modal.page';
import {SellListPriceModalPage} from '../pages/sell/sell-list-price-modal/sell-list-price-modal.page';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../services/product.service';
import { DataService } from '../services/data.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.css'],
})
export class ProductPage implements OnInit {
  constructor(
    public modalController: ModalController,
    private route: ActivatedRoute,
    private router: Router,
    private productpageserv: ProductService,
    private dataService: DataService,
    private navCtrl: NavController,
    public loadingController: LoadingController,
    private gaAnalyticsService: GoogleAnalyticsService) {
  }

  data = {showmore: false, showless: true, product_id: 1109};
  dataReturned: any;
  productpagedata: any;
  productdata: any;
  relatedproducts = [];
  product_id: any;
  loadervisiblecheck: any;
  loaderToShow: any;
  productsizes: any;

  slideOpts = {
    slidesPerView: 1.5,
    speed: 800,
    slidesPerColumn: 1,
    slidesPerGroup:2,
    freeMode: true,
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true,
  }
  };


  ngOnInit() {
   

    this.productpagedata = {
      brand_name: '',
    };

   
    //this.presentLoader();
    
    
    this.route.queryParams.subscribe(params => {
      if (this.route.snapshot.params.productId || params.productId ) {
        if(this.route.snapshot.params.productId !== undefined){
          this.data.product_id = this.route.snapshot.params.productId;
        }else{
          this.data.product_id = params.productId;
        }
        console.log('productID',this.data.product_id);

        this.productpageserv.getproductpagepagedata(this.data.product_id)
      .subscribe( (productpagedata: any) => {
        console.log(productpagedata);
        

        


        this.productpageserv.update(productpagedata);
        this.productpagedata = productpagedata;
        this.gaAnalyticsService.setTrackScreen('Product Screen: '+this.productpagedata.product.title);
        this.productdata = this.productpagedata['product'];
       // this.productdata.description = this.productdata.description+"";
        this.relatedproducts = this.productpagedata['reletedProduct'];
        //this.loadingController.dismiss();
        

        if(params.modal == "BuyreviewpurchaseModalPage" ){
          this.reviewpurchasemodal(params.lowestPrice,params.size,params.askid);
        }
        
        if(params.modal == "BuyoffersTabPage" ){
          this.buyoffermodal(params.productSize);
        }

        if(params.modal == "sellPriceModal" ){
          this.sellReviewModal(params.bidPrice,params.size,params.highestbidid,params.size);
        }
        if (params.modal == "SellListPrice" ) {
          this.sellListPrice(params.productSize);
        }


        
      });

      }
    });
  }

  async reviewpurchasemodal(lowestPrice,size, askid ) {
   // this.closeModal();
    console.log(localStorage.getItem('userID'));

    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      const modal = await this.modalController.create({
        component: BuyreviewpurchaseModalPage,
        // cssClass: 'buypopup-tabs',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.data.product_id,
          product: this.productdata,
          lowestPrice: lowestPrice,
          size: size,
          askid: askid
        }
      });

      modal.onDidDismiss().then((response: any) => {
        console.log(response.data.modal);
        if (response !== null) {
          if (response.data.openmodal === 'BuypopupTabsPage'){
            // response.data.data.defaulttab = 'offers';
            this.buypopuptab(response.data.data);
          }
        }
      });



      return await modal.present();
    } else {   }
  }

  async buypopuptab(data: any) {
    const modal = await this.modalController.create({
      component: BuypopupTabsPage,
      // cssClass: 'buypopup-tabs',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: data
    });
    return await modal.present();
  }

  async sellPriceTab(data: any){
    const modal = await this.modalController.create({
      component: SellPriceModalPage,
       cssClass: 'sell-price-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: data
    });
    return await modal.present();

  }

  async buyoffermodal(size) {
    //this.showLoader();
    
    this.loadingController.dismiss();
    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
     
      // this.productpageserv.getproductsize(this.data.product_id)
      //   .subscribe( (productpagedata: any) => {
      //     this.productsizes = productpagedata['size'];

      //     this.loadingController.dismiss();
    
          


          
          
          
      // });

      const modal = await this.modalController.create({
        component: BuyoffersTabPage,
         cssClass: 'buyoffers-tab',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.data.product_id,
          product: this.productdata,
          productSize: size,
          productsizes: this.productsizes,
        }
      });
      
      modal.onDidDismiss().then ((dataReturned) => {
        console.log(dataReturned);
        if (dataReturned !== null) {
          if (dataReturned.data.openmodel == 'BuypopupTabsPage'){
            console.log('dfsfsdfsf');
            this.buypopup(dataReturned.data.data.productid, 'offers');


          }
         // this.dataReturned = dataReturned.data;
          //alert('Modal Sent Data :'+ dataReturned);
        }
      });
      return await modal.present();





       
     } else {   }
   }

   async sellReviewModal(bidprice,sizeId, bidid, sizenumber) {
    if (localStorage.getItem('userID') !== '' && localStorage.getItem('userID') != null ) {
      const modal = await this.modalController.create({
        component: SellReviewModalPage,
         cssClass: 'sell-review-modal',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.data.product_id,
          product: this.productdata,
          bidPrice: bidprice,
          productsizeid: sizeId,
          highestbidid: bidid,
          size: sizenumber
        }
      });

      modal.onDidDismiss().then((response: any) => {
        // console.log(response.data.modal);
        if (response !== null) {
          if (response.data.openmodal === 'SellPriceModalPage'){
            // response.data.data.defaulttab = 'offers';
           // this.buypopuptab(response.data.data);
            this.sellPriceTab(response.data.data);
          }
        }
      });



      return await modal.present();
    } else {   }
   }

  async buypopup(id: number, tab: string) {
    const modal = await this.modalController.create({
      component: BuypopupTabsPage,
      cssClass: 'buypopup-tabs',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: id,
        product: this.productdata,
        defaulttab: tab
      }
    });
    return await modal.present();
  }

  async sellpopup() {

    if (localStorage.getItem('sellinstructionmessagepopup') == 'true'){
      const modal = await this.modalController.create({
        component: SellPriceModalPage,
        cssClass: 'sell-price-modal',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: this.data.product_id,
          product: this.productdata
        }
      });
      return await modal.present();
    } else {
      const modal = await this.modalController.create({
        component: SellOnBoardModalPage,
        cssClass: 'sell-on-board-modal-page',
        componentProps: {
          productid: this.data.product_id,
          product: this.productdata
        }
      });
      return await modal.present();

    }
  }

  async sellListPrice(productsize: number) {
    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      const modal = await this.modalController.create({
        component: SellListPriceModalPage,
        cssClass: 'sell-list-price-modal',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productSize: productsize,
          productid: this.data.product_id,
          product: this.productdata,
          productsizes: [],
          highestBid: null,
          lowestAsk: null
        }
      });

      modal.onDidDismiss().then((response: any) => {
        if (response !== null) {
          if (response.data.openmodal === 'SellPriceModalPage'){
            // response.data.data.defaulttab = 'offers';
            this.selllistpricetab(response.data.data, 'listprice');
          }
        }
      });



      return await modal.present();
    } else {   }



  }

  async selllistpricetab(data: any, tab = '') {
    const modal = await this.modalController.create({
      component: SellPriceModalPage,
      cssClass: 'sell-price-modal',
      // enterAnimation:halfSlideUpAnimation,
      componentProps: {
        productid: this.data.product_id,
        product: this.productdata,
        defaulttab: tab
      }
    });
    return await modal.present();

  }

 

  showlessmore(){
    if(this.data.showmore){
      this.data.showmore = false;
      this.data.showless = true;
    }else{
      this.data.showmore = true;
      this.data.showless = false;
    }
  }

  

  slideConfigRelatedProducts = {
    "slidesToShow": 1.5, 
    "slidesToScroll": 1,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":false,
    "infinite": false
  };


  


  async openGuaranteePopup() {
    const modal = await this.modalController.create({
      component: AuthenticGuaranteedPopupPage,
      cssClass: 'guaranteed-popup',
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }

  async openHowToSellPopup() {
    const modal = await this.modalController.create({
      component: ProductHowToSellPopupPage,
      cssClass: 'how-to-sell',
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }


  async openHowToBuyPopup() {
    const modal = await this.modalController.create({
      component: ProductHowToBuyPopupPage,
      cssClass: 'how-to-buy',
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }

  gotoproductdetails(product_id) {
    //this.showLoader();
    let navigationExtras: NavigationExtras = {
      state: {
        product_id: product_id
      }
    };
    //this.router.navigate(['productdetail',product_id]);
    // this.navCtrl.push(['productdetail',product_id],{
    //   state: {
    //     product_id: product_id
    //   }
    // });
    this.navCtrl.navigateForward(['productdetail',product_id]);
  }

  goback(){
    //this.showLoader();
    this.navCtrl.pop();
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async showBuyPopup(id){
    if (localStorage.getItem('buyinstructionmessagepopup') == 'true'){
      const modal = await this.modalController.create({
        component: BuypopupTabsPage,
        cssClass: 'buypopup-tabs',
        // enterAnimation:halfSlideUpAnimation,
        componentProps: {
          productid: id,
          product: this.productdata
        }
      });
      return await modal.present();
    } else {
      const modal = await this.modalController.create({
        component: BuyPopupPage,
        cssClass: 'buy-popup',
        componentProps: {
          productid: id,
          product: this.productdata
        }
      });
      return await modal.present();

    }
  }
  ionViewWillLeave() {
    //this.loadingController.dismiss();
  }

  doRefresh(event){
   // this.presentLoader();

    this.route.queryParams.subscribe(params => {
      if (this.route.snapshot.params.productId) {
        this.data.product_id = this.route.snapshot.params.productId;
      }

      this.productpageserv.getproductpagepagedata(this.data.product_id)
      .subscribe( (productpagedata) => {
        this.productpageserv.update(productpagedata);
        this.productpagedata = productpagedata;
        this.productdata = this.productpagedata['product'];
        this.relatedproducts = this.productpagedata['reletedProduct'];
        setTimeout(() => {
          console.log('Async operation has ended');
          event.target.complete();
        }, 1000);
      //  this.loadingController.dismiss();
        

        if(params.modal == "BuyreviewpurchaseModalPage" ){
          this.reviewpurchasemodal(params.lowestPrice,params.size,params.askid);
        }
        
        if(params.modal == "BuyoffersTabPage" ){
          this.buyoffermodal(params.productSize);
        }
        

        if(params.modal == "sellPriceModal" ){
          this.sellReviewModal(params.bidPrice,params.size,params.highestbidid,params.size);
        }
        if (params.modal == "SellListPrice" ) {
          this.sellListPrice(params.productSize);
        }


        
      });


    });

    

  }


}
