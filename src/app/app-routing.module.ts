import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'landing',
    loadChildren: './landing/landing.module#LandingPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  {
    path: 'resetpassword',
    loadChildren: './resetpassword/resetpassword.module#ResetpasswordPageModule'
  },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  {
    path: 'accountverify',
    loadChildren: './accountverify/accountverify.module#AccountverifyPageModule'
  },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'browse', loadChildren: './browse/browse.module#BrowsePageModule' },
  { path: 'sell', loadChildren: './sell/sell.module#SellPageModule' },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfilePageModule'
  },
  {
    path: 'filters',
    loadChildren: './filters/filters.module#FiltersPageModule'
  },
  {
    path: 'product',
    loadChildren: './product/product.module#ProductPageModule'
  },
  {
    path: 'productdetail/:productId',
    loadChildren: './product/product.module#ProductPageModule'
  },
  {
    path: 'authentic-guaranteed-popup',
    loadChildren:
      './popups/authentic-guaranteed-popup/authentic-guaranteed-popup.module#AuthenticGuaranteedPopupPageModule'
  },
  {
    path: 'product-how-to-buy-popup',
    loadChildren:
      './popups/product-how-to-buy-popup/product-how-to-buy-popup.module#ProductHowToBuyPopupPageModule'
  },
  {
    path: 'product-how-to-sell-popup',
    loadChildren:
      './popups/product-how-to-sell-popup/product-how-to-sell-popup.module#ProductHowToSellPopupPageModule'
  },
  {
    path: 'buy-popup',
    loadChildren: './popups/buy-popup/buy-popup.module#BuyPopupPageModule'
  },
  {
    path: 'settings',
    loadChildren: './pages/profile/settings/settings.module#SettingsPageModule'
  },
  {
    path: 'edit-profile',
    loadChildren:
      './pages/profile/edit-profile/edit-profile.module#EditProfilePageModule'
  },
  {
    path: 'buying',
    loadChildren: './pages/profile/buying/buying.module#BuyingPageModule'
  },
  {
    path: 'selling',
    loadChildren: './pages/profile/selling/selling.module#SellingPageModule'
  },
  {
    path: 'selling/order-detail-sell-view/:orderId',
    loadChildren:
      './pages/profile/order-detail-sell-view/order-detail-sell-view.module#OrderDetailSellViewPageModule'
  },
  {
    path: 'selling/shippinglabel/:orderId',
    loadChildren:
      './pages/profile/shippinglabel/shippinglabel.module#ShippinglabelPageModule'
  },
  {
    path: 'buying/order-detail/:orderId',
    loadChildren:
      './pages/profile/order-detail/order-detail.module#OrderDetailPageModule'
  },
  {
    path: 'changepassword',
    loadChildren:
      './pages/profile/changepassword/changepassword.module#ChangepasswordPageModule'
  },
  {
    path: 'payment-settings',
    loadChildren:
      './pages/profile/payment-settings/payment-settings.module#PaymentSettingsPageModule'
  },
  {
    path: 'searchfilter-brand',
    loadChildren:
      './popups/searchfilter-brand/searchfilter-brand.module#SearchfilterBrandPageModule'
  },
  // { path: 'splash', loadChildren: './splash/splash.module#SplashPageModule' }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
