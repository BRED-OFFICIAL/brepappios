import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountverifyPage } from './accountverify.page';

describe('AccountverifyPage', () => {
  let component: AccountverifyPage;
  let fixture: ComponentFixture<AccountverifyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountverifyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountverifyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
