import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupService }  from '../services/signup.service';
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-accountverify',
  templateUrl: './accountverify.page.html',
  styleUrls: ['./accountverify.page.scss'],
})
export class AccountverifyPage implements OnInit {

  constructor(
    private router: Router,
    private signupserv: SignupService,
    public toastController: ToastController,
    private loadingController: LoadingController
    ) { }

  ngOnInit() {
  }

  UserData:any;

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  redirectHome(){
    this.router.navigate(['/tabs/home']);
  }
  resendVerificationEmail(){
    this.presentLoader();
    this.signupserv.resendmail(localStorage.getItem('userid_to_verify'))
      .subscribe( (userdata) => {
          this.loadingController.dismiss();
          this.UserData = userdata;
          this.presentToast(this.UserData.message);
      });
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loaderToShow = await this.loadingController.create({
      message: ''
    });
    await loaderToShow.present();

    const { role, data } = await loaderToShow.onDidDismiss();

    console.log('Loading dismissed!');
  }

}
