import { createAnimation } from '@ionic/core';

export function halfSlideUpAnimation(AnimationC: String, baseEl: HTMLElement) {
    'use strict';


    const baseAnimation = createAnimation();

    const backdropAnimation = createAnimation();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));

    const wrapperAnimation = createAnimation();
    wrapperAnimation.addElement(baseEl.querySelector('.modal-wrapper'));

    wrapperAnimation
        .fromTo('transform', 'translate3d(0,90%,0)', 'translate3d(0,0,0)');

    backdropAnimation.fromTo('opacity', 0.01, 0.4);

    // return Promise.resolve(baseAnimation
    //     // .addElement(baseEl)
    //     // .easing('cubic-bezier(0.36,0.66,0.04,1)')
    //     // .duration(1000)
    //     // .beforeAddClass('show-modal')
    //     // .add(backdropAnimation)
    //     // .add(wrapperAnimation)
    //     );

    baseAnimation.play();


}