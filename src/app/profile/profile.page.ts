import { Component, OnInit } from '@angular/core';
import {ModalController,NavController,NavParams, LoadingController} from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private router: Router,
    private gaAnalyticsService: GoogleAnalyticsService
    ) { }

  ngOnInit() {
    if (localStorage.getItem('userID') != '' && localStorage.getItem('userID') != null ) {
      this.router.navigate(['settings']);
    } else {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          redirectPath: 'settings'
        }
      };
      this.router.navigate(['login'], navigationExtras);

    }


  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Profile Screen');
  }

}
