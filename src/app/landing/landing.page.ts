import { Component, OnInit } from '@angular/core';
import {LoadingController, NavController, Platform} from '@ionic/angular';
import { Router } from '@angular/router';
import { ThemeableBrowser, ThemeableBrowserObject, ThemeableBrowserOptions } from '@ionic-native/themeable-browser/ngx';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import {trigger,state,style,animate,transition} from '@angular/animations';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

@Component({
  selector: 'app-landing[class="landing-page"]',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
  // animations: [
  //   trigger('openClose', [
  //     // ...
  //     state('open', style({
  //       height: '200px',
  //       opacity: 1,
  //       backgroundColor: 'yellow'
  //     })),
  //     state('closed', style({
  //       height: '100px',
  //       opacity: 0.5,
  //       backgroundColor: 'green'
  //     })),
  //     transition('open => closed', [
  //       animate('1s')
  //     ]),
  //     transition('closed => open', [
  //       animate('0.5s')
  //     ]),
  //   ]),
  // ],
})
export class LandingPage implements OnInit {
  subscription: any;
  fingerPrintOptions: FingerprintOptions;
  isOpen = true;
  constructor(
    private router: Router,
    private loadingController: LoadingController,
    private themeableBrowser: ThemeableBrowser,
    private navCtrl: NavController,
    private platform: Platform,
    private fingerprint: FingerprintAIO,
    private gaAnalyticsService: GoogleAnalyticsService
    ) { 

      this.fingerPrintOptions = {
        // clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
         //clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
         disableBackup:true,  //Only for Android(optional)
        // localizedFallbackTitle: 'Use Pin', //Only for iOS
       //  localizedReason: 'Please authenticate' //Only for iOS
     };
    }

  ngOnInit() {
    //this.presentLoader();
    if(+localStorage.getItem('userID')){
      //this.loadingController.dismiss();
      this.navCtrl.navigateRoot(['/tabs/home']);
    }
    //this.loadingController.dismiss();
  }

  async presentLoader() {
    console.log('prsent laoder at product page');
    const loadertoshow = await this.loadingController.create({
      message: ''
    });
    await loadertoshow.present();
  
    const { role, data } = await loadertoshow.onDidDismiss();
  
    console.log('Loading dismissed!');
  }
  openbrowser(){
    const options: ThemeableBrowserOptions = {
      statusbar: {
          color: '#ffffffff'
      },
      toolbar: {
          height: 44,
          color: '#222428'
      },
      title: {
          color: '#003264ff',
          showPageTitle: true
      },
      backButton: {
        wwwImage: 'assets/icon/paypal-icon.png',
        align: 'left',
        event: 'backPressed'
          // image: 'back',
          // imagePressed: 'back_pressed',
          // align: 'left',
          // event: 'backPressed'
      },
      forwardButton: {
          image: 'forward',
          imagePressed: 'forward_pressed',
          align: 'left',
          event: 'forwardPressed'
      },
      closeButton: {
          wwwImage: 'assets/icon/paypal-icon.png',
          //imagePressed: 'close_pressed',
          align: 'left',
          event: 'closePressed'
      },
      menu: {
          image: 'menu',
          imagePressed: 'menu_pressed',
          title: 'Test',
          cancel: 'Cancel',
          align: 'right',
          items: [
              {
                  event: 'helloPressed',
                  label: 'Hello World!'
              },
              {
                  event: 'testPressed',
                  label: 'Test!'
              }
          ]
      },
      backButtonCanClose: true
 }
 
    const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://ionic.io', '_blank', options);
    
  }

  loginRedirect(){
    this.router.navigate(['/login']);

  }

  ionViewDidEnter(){
    
    this.subscription = this.platform.backButton.subscribe(() => {
      console.log(this.router.url);
      if (this.router.isActive('/landing', true) && this.router.url === '/landing') {
        navigator['app'].exitApp();
      }
    });
  }
  
  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

  ionViewWillEnter(){
    //this.presentLoader();
    if(+localStorage.getItem('userID')){
      //this.loadingController.dismiss();
      this.gaAnalyticsService.setTrackScreen('Landing Screen: Redirecting to home screen');
      this.navCtrl.navigateRoot(['/tabs/home']);
    }else{
      this.gaAnalyticsService.setTrackScreen('Landing Screen');
    }

 }
  toggle() {
    this.isOpen = !this.isOpen;
  }

  async checkFingerPrintAIO(){
    try{
      await this.platform.ready();

      const available = await this.fingerprint.isAvailable();
      console.log(available);
      // if(available == "biometric") {
        
        
      // }
      this.fingerprint.show(this.fingerPrintOptions)
        .then((result: any) => {
          console.log(result)
        //biometric_success

        })
        .catch((error: any) => console.log(error));

    } catch(e) {
      console.log('Fingure tyr errr:', e);

    }
  }

}
