import { Component } from "@angular/core";

import { Platform, ModalController, NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Deeplinks } from "@ionic-native/deeplinks/ngx";
import { HomePage } from "./home/home.page";
import { BrowsePage } from "./browse/browse.page";
import { ProductPage } from "./product/product.page";
import { SplashPage } from "./splash/splash.page";
import { GoogleAnalytics } from "@ionic-native/google-analytics/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private modalController: ModalController,
    private deeplinks: Deeplinks,
    private navController: NavController,
    private router: Router,
    private ga: GoogleAnalytics
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString("#1d1c1e");
      this.statusBar.styleLightContent();
      // this.callSplash();
      this.splashScreen.hide();
      this.deeplinks
        .route({
          "/tabs/home": HomePage,
          "/browse": BrowsePage,
          "/productdetail/:productId": ProductPage,
          // '/universal-links-test': AboutPage,
          // '/products/:productId': ProductPage
        })
        .subscribe(
          (match: any) => {
            // match.$route - the route we matched, which is the matched entry from the arguments to route()
            // match.$args - the args passed in the link
            // match.$link - the full link data
            console.log("Successfully matched route", match);
            console.log("Successfully matched link", match.$link.path);
            this.router.navigate([match.$link.path]);
          },
          (nomatch) => {
            // nomatch.$link - the full link data
            console.error("Got a deeplink that didn't match", nomatch);
          }
        );

      this.ga
        .startTrackerWithId("UA-134674041-1")
        .then(() => {
          console.log("Google analytics is ready now");
          //   this.ga.trackView('Mobile App Screen');
          // Tracker is ready
          // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch((e) => console.log("Error starting GoogleAnalytics", e));
    });
  }

  async callSplash() {
    const splash = await this.modalController.create({
      component: SplashPage,
    });
    return await splash.present();
  }
}
