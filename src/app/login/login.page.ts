import { Component, OnInit } from '@angular/core';
import { LoginService }  from '../services/login.service';
import { SignupService }  from '../services/signup.service';
import { ActivatedRoute,Router,NavigationExtras } from '@angular/router';
import { ToastController, LoadingController, NavController, Platform } from '@ionic/angular';
import {Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  islogincheck = false;
  UserData: any;
  redirectPath: any;
  params: any;
  loaderToShow: any;
  constructor(private serv: LoginService,
              private signupserv: SignupService,
              private route: ActivatedRoute,
              private router: Router,
              public toastController: ToastController,
              private loadingController: LoadingController,
              private navCtrl: NavController,
              private fb: Facebook,
              private platform: Platform,
              private gaAnalyticsService: GoogleAnalyticsService
              ) {}
  model = {email: '', password: ''};
  subscription: any;
  errorCode: number;
  ngOnInit() {
    // if(+localStorage.getItem('userID')) {
    //   this.router.navigate(['/tabs/home']);
    // }

    this.route.queryParams.subscribe(params => {
      this.params = params;
      if(params.redirectPath != undefined) {
        this.redirectPath = params.redirectPath;

      }
    });
  }
  async presentToast(text = '') {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
  login(data: any) {
   
    
    if (data.valid) {
      this.islogincheck = true;
      this.presentLoader();
      this.serv.getLogin(this.model.email, this.model.password)
      .subscribe( (userdata: any) => {
        this.UserData = userdata;
        this.loadingController.dismiss(); 
        if (this.UserData.auth === true) {
          localStorage.setItem('userID', this.UserData.userid);
          localStorage.setItem('userEmail', this.UserData.email);
          if(this.redirectPath !== undefined && this.redirectPath !== '') {

            if(this.redirectPath == 'settings'){
               this.navCtrl.navigateForward(['/tabs/profile']);
              //window.location.reload();
            } else {
              let navigationExtras: NavigationExtras = {
                queryParams: this.params
              };
              this.router.navigate([this.redirectPath],navigationExtras);

            }


            
          } else {
             this.router.navigate(['/tabs/home']);
            // this.navCtrl.navigateBack(['/landing']);
           // this.navCtrl.pop();

          }
        } else {
          this.islogincheck = false;
         // this.presentToast('<a (click)="resendEmail()">test</a>');
         if(userdata.code == 1001){
           this.errorCode = userdata.code;
           localStorage.setItem('userid_to_verify', userdata.data.id);


         }else{
          this.presentToast(userdata.message);
          this.errorCode = 0;

         }
         
        }
      }, error => {
        console.error(error);
        this.islogincheck = false;
        this.loadingController.dismiss();
        if (error.status === 0) {
          this.presentToast('Couldn\'t able to connect server/ ');
        } else {
          this.presentToast(error.statusText);
        }
      });
    } else {
      console.log('all false');
    }
  }

  async presentLoader() {
    this.loaderToShow = await this.loadingController.create({
      message: ''
    });
    await this.loaderToShow.present();

    const { role, data } = await this.loaderToShow.onDidDismiss();
  }

  
  back(){
    console.log('login back');
    this.navCtrl.back();
    // this.navCtrl.pop();
  }
  goToForgotPassword(){
    this.navCtrl.navigateForward(["/resetpassword"]);
  }

  fbLogin(){
    this.presentLoader();
    this.fb.getLoginStatus().then((res) => {
      if (res.status === 'connected') {
        console.log(res);
        this.fb.api('me?fields=id,name,email,gender,locale,picture',[]).then((userData) => {
          // console.log(userData);
          this.call_facebook_login(userData);


        });
      
          // Already logged in to FB so pass credentials to provider (in my case firebase)
          // let provider = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
          // firebase.auth().signInWithCredential(provider).then((authToken) => {
          //    this.authToken = authToken;
          // });
      } else {
          // Not already logged in to FB so sign in
          this.fb.login(['public_profile','email']).then((userData1) => {
            console.log(userData1);
            this.fb.api('me?fields=id,name,email,gender,locale,picture',[]).then((userData)=> {
              console.log(userData);
              this.call_facebook_login(userData);
            });
              // FB Log in success
          }).catch((error) => {
            console.log('Error logging into Facebook', error);
              // FB Log in error
          });
      }
  });
  //   this.fb.login(['public_profile', 'user_friends', 'email'])
  // .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
  // .catch(e => console.log('Error logging into Facebook', e));


    this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);

 }

 call_facebook_login(data: any){
  this.loadingController.dismiss();
  this.serv.facebooklogin(data).subscribe((response: any) => {
    console.log(response);
    if (response.status) {
      localStorage.setItem('userID', response.userID);
      localStorage.setItem('userEmail', response.userEmail);
      this.router.navigate(['/tabs/home']);
    }else{
      if(response.code == 'step2'){
        let navigationExtras: NavigationExtras = {
          queryParams: {
            userID: response.userID,
            userEmail: response.userEmail,
            firstName: response.userData.first_name,
            lastName:  response.userData.last_name,
            userName: response.userData.user_name,
            step: 'step2'
          }
        };
        this.router.navigate(['/signup'],navigationExtras);
      } else if (response.code == 'confirm_email'){
        localStorage.setItem('userid_to_verify', response.userID);
        this.router.navigate(['/accountverify']);
      }

      
    }


  });

 }

 resendEmail(){
   console.log('Resend email');
   this.presentLoader();
   this.signupserv.resendmail(localStorage.getItem('userid_to_verify'))
    .subscribe( (userdata) => {
        this.loadingController.dismiss();
        this.UserData = userdata;
        this.presentToast(this.UserData.message);
    });
 }

 ionViewWillEnter(){
    //this.presentLoader();
    if(+localStorage.getItem('userID')){
      //this.loadingController.dismiss();
      this.navCtrl.navigateBack(['/landing']);
      this.gaAnalyticsService.setTrackScreen('Login: Auto Redirecting to home screen');
    }else{
      this.gaAnalyticsService.setTrackScreen('Login screen');
    }

 }
 ionViewDidEnter(){
  
  this.subscription = this.platform.backButton.subscribe(() => {
    console.log( this.router.url);
    this.navCtrl.pop();
    // if(+localStorage.getItem('userID')){
    //   this.navCtrl.navigateRoot(['/tabs/home']);
    // }
    // else{
    //   this.navCtrl.navigateRoot(['/landing']);
    // }
   

    // if (this.router.isActive('/tabs/browse', true) && this.router.url === '/tabs/browse') {
    //   // this.router.nav igate(['/tabs/home']);
     
    // }
    // else if(this.router.isActive('/tabs/profile', true) && this.router.url === '/tabs/profile'){
    //   this.navCtrl.back();
    // }
    // else{
    //   if(+localStorage.getItem('userID')){
    //     navigator['app'].exitApp();
    //   }else{
    //     this.navCtrl.back();
    //   }
      
    // }
  });
}

ionViewWillLeave(){
  this.subscription.unsubscribe();
}
}
