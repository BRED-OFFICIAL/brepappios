import { Component, OnInit } from '@angular/core';
import { ResetpasswordService }  from '../services/resetpassword.service';
import { Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';
import { GoogleAnalyticsService } from '../services/google-analytics.service';


@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
  
  constructor(
    private resetserv: ResetpasswordService,
    private router: Router,
    public toastController: ToastController,
    private navCtrl: NavController,
    private gaAnalyticsService: GoogleAnalyticsService
    ) { }
  model = {email: '', success: false};
  UserData: any;
  err_response: any;
  succ_response: any;

  ngOnInit() {
    
  }

  async presentToastSuccess() {
    const toast = await this.toastController.create({
      message: 'Email sent successfully!!',
      duration: 2000
    });
    toast.present();
  }
  
  resetpassword(data : any) {
    this.resetserv.resetpassword(this.model.email)
      .subscribe( (userdata) => {
          this.UserData = userdata;
          localStorage.setItem('resetpass_email', this.model.email);
          if (this.UserData.resp == true) {
            // localStorage.setItem('userID', this.UserData.email);
            // this.router.navigate(['/home']);
            this.model.success = true ;
            this.err_response = '';
            //this.presentToastSuccess();
          } else {
            //this.presentToastFail();
            this.err_response = 'Email not found!!!';
            this.model.success = false ;
          }
      });
  }

  back() {
    this.navCtrl.pop();
  }

  resendResetEmail(){
    this.resetserv.resetpassword(localStorage.getItem('resetpass_email'))
      .subscribe( (userdata) => {
          this.UserData = userdata;
        
          if (this.UserData.resp == true) {
            this.model.success = true ;
            this.err_response = '';
            this.presentToastSuccess();
          } else {
            //this.presentToastFail();
            this.err_response = 'Email not found!!!';
            this.model.success = false ;
          }
      });
  }

  ionViewWillEnter(){
    this.gaAnalyticsService.setTrackScreen('Reset Password Screen');
  }

}
