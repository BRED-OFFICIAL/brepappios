import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { HomepageService } from "./../services/homepage.service";
import { Router, NavigationExtras } from "@angular/router";
import {
  ToastController,
  LoadingController,
  Platform,
  NavController,
} from "@ionic/angular";
import { DataService } from "../services/data.service";
import { GoogleAnalyticsService } from "../services/google-analytics.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.css"],
})
export class HomePage implements OnInit {
  constructor(
    private router: Router,
    public toastController: ToastController,
    private homepageserv: HomepageService,
    private dataService: DataService,
    private platform: Platform,
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private gaAnalyticsService: GoogleAnalyticsService
  ) {}

  images = [1, 2, 3].map(
    () => "https://picsum.photos/900/500?random&t=${Math.random()}"
  );
  homepageData: any;
  homeslides: any;
  featured_products: any;
  brands: any;
  items: any;
  newListings: any;
  newHighestListing: any;
  upcomingReleaseListing: any;
  subscription: any;
  screenWidth: any;

    
  slideOpts = {
    slidesPerView: 1.8,
    spaceBetween: 15,
    speed: 800,
    slidesPerColumn: 1,
    slidesPerGroup: 2,
    freeMode: true,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    on: {
      beforeInit: function() {
        console.log('slider init');
        console.log(window.innerWidth);
        
        switch (window.innerWidth){
          case 414:
            const overwriteParams = {
              slidesPerView: 2.5,
              spaceBetween: 70,
            };
            this.params = Object.assign(this.params, overwriteParams);
            this.originalParams = Object.assign(this.originalParams, overwriteParams);
            break;
        }

        
      }
    }
  };

  

  gotoproductdetails(product_id) {
    let navigationExtras: NavigationExtras = {
      state: {
        product_id: product_id,
      },
    };

    this.router.navigate(["/productdetail/", product_id], navigationExtras);
    // this.router.navigate(['/landing']);
  }

  ngOnInit() {
    // this.presentLoader();
    this.loaddata();
  }

  async loaddata() {
    await this.homepageserv.gethomepagedata().subscribe(
      (homepagedata) => {
        this.homepageData = homepagedata;
        this.homeslides = this.homepageData.home_slider;
        this.featured_products = this.homepageData.featured_products;
        this.brands = this.homepageData.brands;
        this.newListings = this.homepageData.new_listings;
        this.newHighestListing = this.homepageData.newhighest_listings;
        this.upcomingReleaseListing = this.homepageData.upcoming_listings;
      },
      (error) => console.log(error)
    );
  }

  clearSearchData() {
    this.items = [];
  }

  clearSearchDataOnBlank(ev) {
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() == "") {
      this.clearSearchData();
    }
  }

  getItems(ev) {
    // Reset items back to all of the items

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.homepageserv.getsearcheddata(val).subscribe((search_data) => {
        this.items = search_data;
      });
    } else {
      this.items = [];
      this.clearSearchData();
      console.log("I am no");
    }
  }
  navigateToDetails() {
    console.log("Here I will navigate");
  }

  slides = [
    { img: "https://picsum.photos/900/500?random&t=${Math.random()}" },
    { img: "https://picsum.photos/900/500?random&t=${Math.random()}" },
    { img: "https://picsum.photos/900/500?random&t=${Math.random()}" },
    { img: "https://picsum.photos/900/500?random&t=${Math.random()}" },
    { img: "https://picsum.photos/900/500?random&t=${Math.random()}" },
  ];

  slideConfigFeatured = {
    slidesToShow: 1.6,
    slidesToScroll: 3,
    nextArrow: "",
    prevArrow: "",
    dots: false,
    infinite: false,
  };

  slideConfigBrands = {
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: "<div class='nav-btn next-slide'></div>",
    prevArrow: "<div class='nav-btn prev-slide'></div>",
    dots: false,
    infinite: false,
  };

  slideConfigHomeTop = {
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: "<div class='nav-btn next-slide'></div>",
    prevArrow: "<div class='nav-btn prev-slide'></div>",
    dots: true,
    infinite: false,
  };

  async presentLoader() {
    console.log("prsent laoder at product page");
    const loadertoshow = await this.loadingController.create({
      message: "",
    });
    await loadertoshow.present();

    const { role, data } = await loadertoshow.onDidDismiss();

    console.log("Loading dismissed!");
  }

  slickInit(e) {
    console.log("slick", e);
  }

  breakpoint(e) {
    console.log("breakpoint", e);
  }

  doRefresh($event) {
    this.loaddata().then(() => {
      setTimeout(() => {
        console.log("Async operation has ended");
        $event.target.complete();
      }, 2000);
      //$event.target.complete();
    });
  }

  ionViewWillEnter() {
    console.log('fffff');
    //this.slideOpts.slidesPerView=2.5;
    this.gaAnalyticsService.setTrackScreen("Home Screen");
    
  }
}
