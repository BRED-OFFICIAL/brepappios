import {Injectable} from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
  })
  export class AuthenticationInterceptor implements HttpInterceptor {
    url = environment.API.URL;
    apiKey = environment.API.KEY; // <-- Enter your own key here!

    intercept( request: HttpRequest<any>, next: HttpHandler): Observable <HttpEvent <any>>{
        console.log('Interceptor calling');
        // request = request.clone({
        //     setHeaders:{
        //         'Content-Type':'application/x-www-form-urlencoded',
        //        'X-API-KEY': '1e957ebc35631ab22d5bd6526bd14ea2'
        //     }

        // });
       
        return next.handle(request);

    }

  }
