export const ThemeableBrowserOptionConfig = {
  statusbar: {
    color: '#ffffffff'
  },
  toolbar: {
    height: 44,
    color: '#ffffff'
  },
  title: {
    color: '#003264ff',
    showPageTitle: true
  },
  backButton: {
    // wwwImage: 'assets/icon/back.png',
    // imagePressed: 'assets/icon/back_pressed.png',
    // align: 'left',
    // event: 'backPressed'
    image: 'back',
    imagePressed: 'back_pressed',
    align: 'right',
    event: 'backPressed'
  },
  forwardButton: {
    image: 'forward',
    imagePressed: 'forward_pressed.png',
    align: 'right',
    event: 'forwardPressed'
  },
  closeButton: {
    image: 'close',
    imagePressed: 'close_pressed',
    align: 'left',
    event: 'closePressed'
  },
  backButtonCanClose: true
};
